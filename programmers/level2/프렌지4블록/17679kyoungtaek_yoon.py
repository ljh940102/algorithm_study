
def drop(curm, curn, check):
    f = check[curm][curn]
    while check[curm][curn] == "-1" and curm >=0:
        curm -= 1
    return curm
def solution(m, n, board):
    answer = 0
    conti = True
    copyboard = [[0] * n for _ in range(m)]

    for i in range(m):
        for j in range(n):
            copyboard[i][j] = board[i][j]
    while conti:
        conti = False
        check = [[0] * n for _ in range(m)]
        for i in range(m-1):
            for j in range(n-1):
                if copyboard[i][j] == copyboard[i+1][j] == copyboard[i][j+1] == copyboard[i+1][j+1] != "-1":
                    check[i][j] = -1
                    check[i+1][j] = -1
                    check[i][j+1] = -1
                    check[i+1][j+1] = -1
                    conti = True

        for i in range(m):
            for j in range(n):
                if check[i][j] == -1:
                    copyboard[i][j] = "-1"
        for i in range(m-1,-1,-1):
            for j in range(n):
                curi = drop(i, j, copyboard)
                copyboard[i][j] = copyboard[curi][j]
                if i != curi:
                    copyboard[curi][j] = "-1"
        #check = [[0] * n for _ in range(m)]

    for i in range(m):
        for j in range(n):
            if copyboard[i][j] == "-1":
                answer += 1
    return answer

##답찾은거
def friends(m, n, board, visited):
    for i in range(m - 1):
        for j in range(1, n):
            if board[i][j] != ' ' and board[i][j] == board[i][j - 1] and board[i][j - 1:j + 1] == board[i + 1][
                                                                                                  j - 1:j + 1]:
                visited[i][j] = True
                visited[i][j - 1] = True
                visited[i + 1][j] = True
                visited[i + 1][j - 1] = True


def deleteBlock(m, n, board, visited):
    cnt = 0
    for i in range(m):
        for j in range(n):
            if visited[i][j] == True:
                cnt += 1
                board[i] = board[i][:j] + ' ' + board[i][j + 1:]

    rotation_board = []
    for i in range(n):
        tmp = ''
        for j in range(m):
            tmp += board[j][i]
        rotation_board.append(tmp)

    for i in range(n):
        for j in range(1, m):
            if rotation_board[i][j] == ' ':
                rotation_board[i] = ' ' + rotation_board[i][:j] + rotation_board[i][j + 1:]

    tmp_board = []
    for i in range(m):
        tmp = ''
        for j in range(n):
            tmp += rotation_board[j][i]
        tmp_board.append(tmp)

    board = tmp_board

    return board, cnt


def solution(m, n, board):
    answer = 0
    result = 1
    while result:
        visited = [[False] * n for _ in range(m)]
        friends(m, n, board, visited)
        board, result = deleteBlock(m, n, board, visited)
        answer += result
    return answer