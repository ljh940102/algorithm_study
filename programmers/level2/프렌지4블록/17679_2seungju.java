import java.util.*;

class Solution {
    private static final int[] X = {1, 0, 1, 0};
    private static final int[] Y = {0, 1, 1, 0};
    
    private int m = 0;
    private int n = 0;
    private int count = 0;
    
    public int solution(int x, int y, String[] board) {
        m = x;
        n = y;
        
        String[][] array = new String[m][n];

        for (int i = 0; i < m; i++) {
            String[] strings = board[i].split("");
            for (int j = 0; j < n; j++) {
                array[i][j] = strings[j];
            }
        }

        deleteBlock(array);
        
        return count;
    }
    
    private String[][] deleteBlock(String[][] array) {
        boolean[][] booleans = new boolean[m][n];
        boolean isRecursive = false;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] == null || isInvalid(i, j, array)) {
                    continue;
                }
                
                for (int k = 0; k < X.length; k++) {
                    if (!booleans[i + X[k]][j + Y[k]]) {
                        booleans[i + X[k]][j + Y[k]] = true;
                        count++;
                    }
                }
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (booleans[i][j]) {
                    isRecursive = true;
                    array[i][j] = null;
                }
            }
        }

        return isRecursive ? deleteBlock(filledBlock(array)) : array;
    }
    
    private boolean isInvalid(int x, int y, String[][] array) {
        boolean isInvalid = false;
        for (int i = 0; i < X.length; i++) {
            if (x + X[i] >= m || y + Y[i] >= n) {
                isInvalid = true;
                break;
            }

            if (!array[x][y].equals(array[x + X[i]][y + Y[i]])) {
                isInvalid = true;
            }
        }
        
        return isInvalid;
    }

    private String[][] filledBlock(String[][] array) {
        Stack<String> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (array[j][i] != null) {
                    stack.push(array[j][i]);
                }

                array[j][i] = null;
            }

            int index = m - 1;

            while (!stack.isEmpty()) {
                String str = stack.pop();

                if (array[index][i] == null) {
                    array[index--][i] = str;
                }
            }
        }

        return array;
    }
}