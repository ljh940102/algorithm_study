"""
검사는 해당칸과 좌측칸관 아래 두칸만 같은 블록인지 확인하면 됨(4방향 X)
board는 튜플로 구성(캐릭터이름,삭제여부)로 하고 safe sort하면 아래로 떨어트릴수 있음.

위 2단계를 함수로 만들고, 제거된 블록의 수를 리턴하도록 하벼 += 연산
이후 0이 리턴되면 answer 리턴

단, 숫자가 큰 인덱스일수록 아래임을 유의
"""

row_len = 0
col_len = 0
rotate_r = [0, 0, -1, -1]
rotate_c = [0, 1, 0, 1]

def is_safe(r, c):
    if r>=0 and r<row_len and c>=0 and c<col_len:
        return True
    else:
        return False


def inspect(r, c, main_board):
    """ 붙어있는 4칸 블록 찾고 1로 표시 """
    safe = True
    for idx in range(4):
        safe &= (is_safe(r+rotate_r[idx], c+rotate_c[idx]) and main_board[r+rotate_r[idx]][c+rotate_c[idx]][1] != '2')
    if safe:  # 검사할 4칸이 모두 safe인 경우
        check = set()
        for idx in range(4):
            check.add(main_board[r + rotate_r[idx]][c + rotate_c[idx]][0])
        if len(check) == 1:  # 4개가 모두 같다면 1로 표시 => 점수계산시에 캐릭터 D로 변경
            for idx in range(4):
                main_board[r + rotate_r[idx]][c + rotate_c[idx]][1] = 1


def postprocssing(main_board):
    """ 터트린블록 계산 개수 계산 및 해당 블록 빈칸처리 """
    rtn = 0
    for idx_r in range(row_len):
        for idx_c in range(col_len):
            target = main_board[idx_r][idx_c]
            if target[0] != 'D' and target[1] == 1:
                rtn += 1
                target[0] = 'D'
                target[1] = 2
    return rtn


def make_board_clean(main_board):
    """ 열을 기준으로 list를 정렬하여 덮어씌워 떨어트리는 효과를 만든다 """
    for col in range(col_len):
        col_target = []
        for row in range(row_len):
            col_target.append(main_board[row][col])
        col_target = sorted(col_target, key=lambda x: x[1], reverse=True)
        for row in range(row_len):
            main_board[row][col] = col_target[row]


def do_one_turn(main_board):
    # 같은블록 표시하기
    for idx_r in range(row_len):
        for idx_c in range(col_len):
            inspect(idx_r, idx_c, main_board)
    # 점수계산 및 터진블록 처리
    point = postprocssing(main_board)
    # 떨어트리기
    make_board_clean(main_board)
    return point


def solution(m, n, board):
    answer = 0
    global row_len
    global col_len
    row_len, col_len = m, n
    main_board = []
    for row in board:
        main_board.append([[character, 0] for character in row])

    while True:
        point = do_one_turn(main_board)  # 한 턴 수행
        if point == 0:
            break
        else:
            answer += point
    return answer

print(solution(4,5,["CCBDE", "AAADE", "AAABF", "CCBBF"]))
print(solution(6,6,["TTTANT", "RRFACC", "RRRFCC", "TRRRAA", "TTMMMF", "TMMTTJ"]))
print(solution(3,4,["AAAA", "AAAA", "AAAA"]))
print(solution(4,4,["acvb","anaa","bjha","basa"]))
