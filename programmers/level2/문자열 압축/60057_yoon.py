def solution(s):
    minNum = float('inf')
    if len(s) == 1:
        return 1

    for index in range(1, len(s) // 2 + 1):  # 모든 자르는 경우수에 대해서 계산
        count = 1
        temp = s[:index]  # 여기까지 잘림
        result = ""
        for index_start in range(index, len(s), index):  # index만큼 잘라서 비교함
            if s[index_start:index_start + index] == temp:
                count += 1
            else:
                if count == 1:
                    count = ""  # 반복이 안 될 경우 그냥 안 더함
                result += str(count) + temp  # count만큼 더하고 문자열ㅇ르 더함
                temp = s[index_start:index_start + index]
                count = 1
        if count == 1:
            count = ""
        result += str(count) + temp
        if minNum > len(result):
            minNum = len(result)
    return minNum