
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
def solution(s):
    rtn = len(s)
    limit = len(s)//2

    for window_size in range(1, limit+1):
        compressed = ""
        window = None
        compressed_num = 1
        idx = 0
        while idx < len(s):
            if not window:  # 윈도우가 초기화된 상태라면
                if idx+window_size > len(s):  # 압축할 문자열이 부족한 경우
                    compressed += s[idx:]
                    window = None
                    break
                else:
                    window = s[idx:idx+window_size]
                    idx += window_size  ######## 여기서 인덱스 에러 날 수 있겠다

            if window == s[idx:idx+window_size]:  # 압축성공
                compressed_num += 1
                idx += window_size

            else:  # 압축실패
                if compressed_num > 1:
                    compressed += str(compressed_num)
                compressed += window

                window = None
                compressed_num = 1

        if window:
            if compressed_num > 1:
                compressed += str(compressed_num)
            compressed += window

        # print(compressed)
        rtn = min(rtn, len(compressed))

    return rtn