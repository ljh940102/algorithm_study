def solution(s):
    rtn = len(s)
    limit = len(s)//2  # 중간값 이상을 검사필요 X

    for window_size in range(1, limit+1):
        compressed, compressed_num = "", 1
        window = s[:window_size]

        idx = window_size
        while idx < len(s):
            if window == s[idx:idx+window_size]:  # 압축가능
                compressed_num += 1
                idx += window_size

            else:                                 # 압축불가
                if compressed_num > 1:
                    compressed += str(compressed_num)
                compressed += window

                # 다음 윈도우 세팅
                window = s[idx:idx + window_size]
                idx += window_size
                compressed_num = 1

            if idx + window_size > len(s):  # 압축할 문자열이 부족한 경우(남은 문자열이 부족한 경우)
                if compressed_num > 1:  # 현재 윈도우 붙여주고
                    compressed += str(compressed_num)
                compressed += window

                compressed += s[idx:]  # 나머지 붙여주기
                break

        rtn = min(rtn, len(compressed))

    return rtn

print(solution("aabbaccc"))
print(solution("ababcdcdababcdcd"))
print(solution("abcabcdede"))
print(solution("abcabcabcabcdededededede"))