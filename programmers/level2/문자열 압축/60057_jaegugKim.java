class Solution {
    public int solution(String s) {
        int answer = 0;
        answer = s.length();
        int count = 1;
        for(int i=1; i<=s.length()/2; i++){
            String tmp = s.substring(0,i);
            String zip = "";
            
            for(int j=i; j<s.length(); j+=i){
                if(tmp.equals(s.substring(j,j+i))){
                    count++;
                }else{
                    if(count == 1){
                        zip += tmp; 
                        tmp = s.substring(j,j+i);
                    }else{
                        zip += count;
                        zip += tmp;
                        tmp = s.substring(j,j+i);
                        count = 1;
                    }
                }
                if(j+i+i > s.length()){
                    if(count == 1){
                        zip += s.substring(j); 
                    }else{
                        zip += count;
                        zip += tmp;
                        zip += s.substring(j+i); 
                        count = 1;
                    }
                    break;
                }
            }
            answer = answer>zip.length()? zip.length() : answer;
        }
        return answer;
    }
}