import java.util.*;
class Solution {
    public String solution(String p) {
        String answer = "";
        String u = "";
        String v = "";
        int open = 0;
        int close = 0;
        
        for(int i=0; i<p.length(); i++){
            if(p.charAt(i)=='('){
                open++;
            }else{
                close++;
            }
            if(open==close){
                u = p.substring(0,i+1);
                v = p.substring(i+1);
                break;
            }
        }
        if(!v.equals("")){
            v = solution(v);    
        }
        
        boolean flag = true;
        int head = 0;
        ArrayList<Character> st = new ArrayList<>();
        for(int i=0; i<u.length(); i++){
            if(head == 0 && u.charAt(i) == ')'){
                flag = false;
                break;
            }else if(u.charAt(i) == '('){
                head++;
            }else if(u.charAt(i) ==')'){
                head--;
            }
        }
        if(!flag){
            answer="("+v+")";
            for(int i=1; i<u.length()-1; i++){
                if(u.charAt(i) =='('){
                    answer += ')';
                }else{
                    answer += '(';
                }
            }
        }else{
            answer = u + v;    
        }
        return answer;
    }
}