def checkIfOkayParenthesis(u):
    numPar = 0
    for char in u:
        if char == "(":
            numPar += 1
        elif char == ")":
            numPar -= 1
        if numPar < 0:
            return False
    return True


def returnBalancedParentheisNum(w):
    u = ""
    numPar = 0
    for i in range(len(w)):
        char = w[i]
        if char == "(":
            numPar += 1
        elif char == ")":
            numPar -= 1
        if numPar == 0:
            return i


def solution(p):
    answer = ''
    if p == "":
        return p
    i = returnBalancedParentheisNum(p)
    u, v = p[:i+1], p[i+1:]
    if checkIfOkayParenthesis(u):
        return u + solution(v)
    else:
        result = '('
        result += solution(v)
        result += ')'
        for i in range(1,len(u)-1):
            char = u[i]
            if char == '(':
                result += ')'
            else:
                result += '('
        return result

#print(solution("(()())()"))

print(solution("()))((()"))


print(solution("(()())()"))

print(solution("()))((()"))
