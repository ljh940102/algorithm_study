import java.util.*;
class Solution {
    public String solution(String m, String[] musicinfos) {
        String answer = "";
        int length = 0;
        ArrayList<Character> search = new ArrayList<>();
        for(int i = 0; i < m.length(); i++){
            if(i+1<m.length() && m.charAt(i+1) == '#'){ //다음 문자가 #이면 소문자로 치환해서 List에 삽입
                search.add(Character.toLowerCase(m.charAt(i)));
                i++;
            }else if(m.charAt(i) != '#'){ // 혹시 모르니까 #이 아닌지 확인하고 list 에 삽입해주자.
                search.add(m.charAt(i));
            }
        }
        for(int i = 0; i < musicinfos.length; i++){
            String tmp[] = musicinfos[i].split(",");
            int playTime = Integer.parseInt(tmp[1].substring(0,2)) * 60 - Integer.parseInt(tmp[0].substring(0,2)) * 60
                            + Integer.parseInt(tmp[1].substring(3)) - Integer.parseInt(tmp[0].substring(3));
            ArrayList<Character> target = new ArrayList<>();
            for(int j = 0; j < tmp[3].length(); j++){
                if(j+1 < tmp[3].length() && tmp[3].charAt(j+1) == '#'){
                    target.add(Character.toLowerCase(tmp[3].charAt(j)));
                    j++;
                }else if(tmp[3].charAt(j) != '#'){
                    target.add(tmp[3].charAt(j));
                }
            }
            int mFlag = 0;
            int infosFlag = 0;
            int matchTime = 0;
            for(int j = 0; j<playTime; j++){
                if(infosFlag >= target.size()){
                    infosFlag = 0;
                }
                if(mFlag >= search.size()){
                    break;
                }
                if(search.get(mFlag) == target.get(infosFlag)){ // 두 값이 일치할 때마다 flag를 이동하고 matchTime을 증가시킨다.
                    mFlag++;
                    matchTime++;
                }else if(matchTime > 0){ // 두 값이 일치하지 않으면 matchTime과 mFlag를 초기화해준다
                    mFlag = 0;
                    matchTime = 0;
                    //ABCD, ABABCD 와 같은 반례로 인해 틀린 값부터 다시 점검하는 로직 추가
                    if(infosFlag != 0){
                        infosFlag--;
                        j--;
                    }
                }
                infosFlag++;
            }
            if(matchTime == search.size()){ //가장 긴 음악을 찾아야하기 때문에 playTime을 기록해두고 매칭될 때마다 비교해준다.
                if(playTime > length){ 
                    answer = tmp[2];
                    length = playTime;
                } 
            }
        }
        if(answer.equals("")){
            answer = "(None)";
        }
        return answer;
    }
}
