"""
1. 먼저 곡을 길이만큼 채운다.(단 #은 길이로 치지 않음을 주의)
   -> 이 부분에서 알파벳을 숫자로 대체하자(dict사용)
2. m을 슬라이딩 시키면서 멜로디에 들어있는지 본다.
3. 들어있으면 max heap에 넣을까?(음악길이,이름길이 가 비교값)
+ heap에 넣는거랑 정렬하는거랑 뭐가 효율이지??..
-> max heap하려면 따로 우선순위를 구해줘야되네? 걍 정렬하자
"""
import datetime

def normalize(sheet):
    """
    #음표는 소문자로 치환
    """
    normalized = []
    for idx, ch in enumerate(sheet):

        if ch == '#':
            target = normalized.pop()
            normalized.append(target.lower())
        else:
            normalized.append(ch)
    return "".join(normalized)

def solution(m, musicinfos):
    insert_seq, answer_list = 1, []
    m = normalize(m)
    musicinfos = [target.split(",") for target in musicinfos]

    for start, end, title, sheet in musicinfos:
        #### 1. 시간구하기
        start_hour, start_min = int(start[0:2]), int(start[3:5])  # 시작시간
        end_hour, end_min = int(end[0:2]), int(end[3:5])  # 종료시간
        played_len = ((end_hour - start_hour)*60) + (end_min - start_min)

        #### 2. 시간만큼 재생된 악보구하기
        total_sheet = ""
        sheet = normalize(sheet)
        for idx in range(played_len):  # 재생시간만큼 악보반복
            i = idx % len(sheet)  # sheet 반복을 위한 인덱스 조정
            total_sheet += sheet[i]

        #### 3. 존재여부 체크
        if total_sheet.find(m) != -1:  # 문자열이 있으면
            answer_list.append([played_len, insert_seq, title])  # (재생시간,삽입순서,제목) -> 튜플을 이용하여 다중조건 정렬 수행
            insert_seq += 1

    if len(answer_list) == 0:
        return "(None)"  # '(None)' 아님
    answer_list = sorted(answer_list, key=lambda x: (-x[0], x[1]))  # 길이는 내림차순, 삽입순서는 오름차순

    return answer_list[0][2]