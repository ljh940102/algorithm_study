import java.util.*;

class MusicInfo {
    private int playTime;
    private String song;
    private List<String> melodyNotes;

    public MusicInfo(int playTime, String song, List<String> melodyNotes) {
        this.playTime = playTime;
        this.song = song;
        this.melodyNotes = melodyNotes;
    }

    public String getSong() {
        return song;
    }

    public List<String> getMelodyNotes() {
        return melodyNotes;
    }

    public int getPlayTime() {
        return playTime;
    }
}

class Solution {
    public String solution(String m, String[] musicInfos) {
        List<MusicInfo> musicInfoList = new ArrayList<>();
        for (String info : musicInfos) {
            String[] musicInfo = info.split(",");

            musicInfoList.add(new MusicInfo(calculatorPlayTime(musicInfo[0], musicInfo[1]), musicInfo[2],  getNotes(musicInfo[3])));
        }

        List<MusicInfo> firstFilterList = new ArrayList<>();
        for (MusicInfo musicInfo : musicInfoList) {
            int index = 0;
            List<String> musicMelodyNotes = new ArrayList<>();
            for (int i = 0; i < musicInfo.getPlayTime(); i++) {
                if (index == musicInfo.getMelodyNotes().size()) {
                    index = 0;
                }

                musicMelodyNotes.add(musicInfo.getMelodyNotes().get(index++));
            }

            List<String> melodyNotes = getNotes(m);
            for (int j = 0; j < musicMelodyNotes.size(); j++) {
                if (j + melodyNotes.size() > musicMelodyNotes.size()) {
                    break;
                }
                String temp = String.join("", musicMelodyNotes.subList(j, j + melodyNotes.size()));
                if (m.equals(temp)) {
                    firstFilterList.add(musicInfo);

                    break;
                }
            }
        }

        String result = "";
        if (firstFilterList.size() == 0) {
            result = "(None)";

        } else if (firstFilterList.size() == 1) {
            result = firstFilterList.get(0).getSong();

        } else {
            int maxTime = 0;
            for (MusicInfo musicInfo : firstFilterList) {
                maxTime = Math.max(maxTime, musicInfo.getPlayTime());
            }

            List<MusicInfo> secondFilterList = new ArrayList<>();
            for (MusicInfo musicInfo : firstFilterList) {
                if (musicInfo.getPlayTime() == maxTime) {
                    secondFilterList.add(musicInfo);
                }
            }

            result = secondFilterList.get(0).getSong();
        }

        return result;
    }

    private int calculatorPlayTime(String start, String end) {
        String[] startTime = start.split(":");
        String[] endTime = end.split(":");

        int time = (Integer.parseInt(endTime[0]) - Integer.parseInt(startTime[0])) * 60;
        time += Integer.parseInt(endTime[1]) - Integer.parseInt(startTime[1]);

        return time;
    }

    private List<String> getNotes(String melody) {
        List<String> notes = new ArrayList<>();
        String[] temp = melody.split("");
        for (String str : temp) {
            if (str.equals("#")) {
                String note = notes.get(notes.size() - 1);
                notes.remove(notes.size() - 1);
                notes.add(note + str);

            } else {
                notes.add(str);
            }
        }

        return notes;
    }
}