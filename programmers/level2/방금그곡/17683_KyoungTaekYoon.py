def solution(m, musicinfos):
    md = changeShop(m)
    title_list = []
    for musicinfo in musicinfos:
        start_time, end_time, title, melody = musicinfo.split(",")
        song = []
        start_hour, start_min = start_time.split(":")
        end_hour, end_min = end_time.split(":")
        total_min = (int(end_hour) - int(start_hour)) * 60 + (int(end_min) - int(start_min))
        melody = changeShop(melody)
        for minute in range(total_min):
            loc = minute % len(melody)
            song.append(melody[loc])
        song_join = ''.join(song)
        if md in song_join:
            title_list.append((title,total_min))
    if len(title_list) == 0:
        return "(None)"
    elif len(title_list) == 1:
        return title_list[0][0]
    else:
        title_list = sorted(title_list, key=lambda x: -x[1])
        return title_list[0][0]

    return answer

def changeShop(melody): #노래 이름 변경
    song = []
    loc = 0
    while loc < len(melody) - 1:
        if melody[loc + 1] == "#":
            if melody[loc] == "C":
                song.append("H")
            elif melody[loc] == "D":
                song.append("I")
            elif melody[loc] == "F":
                song.append("J")
            elif melody[loc] == "G":
                song.append("K")
            elif melody[loc] == "A":
                song.append("l")
            loc += 1
        else:
            song.append(melody[loc])
        loc += 1
    if melody[len(melody) - 1] != "#":
        song.append(melody[len(melody) - 1])
    return "".join(song)