import string
from collections import deque
from itertools import permutations
import copy

def findSpecialLetter(expression: string):
    s = set()
    for char in expression:
        if not char.isnumeric():
            s.add(char)
    return s

def calc(result_deque, perm):
    temp_result = copy.deepcopy(result_deque)
    for oper in perm:
        temp = []
        i = 0
        while temp_result:
            if oper == "+":
                if temp_result[i] == "+":
                    temp.pop()
                    ToAppend = int(temp_result[i-1]) + int(temp_result[i+1])
                    i = i + 2
                    temp_result.popleft()
                    temp_result.popleft()
                    temp_result.popleft()
                    temp.append(ToAppend)
                else:
                    temp.append(temp_result[i])
                    i = i + 1
            elif oper == "-":
                if temp_result[i] == "-":
                    temp.pop()
                    ToAppend = int(temp_result[i-1]) - int(temp_result[i+1])
                    i = i + 2
                    temp_result.popleft()
                    temp_result.popleft()
                    temp_result.popleft()
                    temp.append(ToAppend)
                else:
                    temp.append(temp_result[i])
                    i = i + 1
            elif oper == "*":
                if temp_result[i] == "*":
                    temp.pop()
                    ToAppend = int(temp_result[i-1]) * int(temp_result[i+1])
                    i = i + 2
                    temp_result.popleft()
                    temp_result.popleft()
                    temp_result.popleft()
                    temp.append(ToAppend)
                else:
                    temp.append(temp_result[i])
                    i = i + 1
        temp_result = temp
    answer = abs(temp_result.pop())
    return answer

def solution(expression):
    answer = 0
    special = findSpecialLetter(expression)
    perm = list(permutations(special))
    result = deque()
    temp = ""
    for char in expression:
        if not char.isnumeric():
            result.append(temp)
            temp = ""
            result.append(char)
        else:
            temp += char
    result.append(temp)
    templist = ["-","*"]
    #for operlist in perm:
        #answer = max(answer, calc(result, operlist))
    calc(result,templist)
    return answer
#solution("100-200*300-500+20")
solution("50*6-3*2")