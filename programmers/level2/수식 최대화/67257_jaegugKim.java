import java.util.*;
class Solution {
    public long solution(String expression) {
        long answer = 0;
        int max = 0;
        int flag = 0;
        
        ArrayList<Character> operList = new ArrayList<>();
        LinkedList<String> expList = new LinkedList<>();
        
        for(int i=0; i<expression.length(); i++){
            if(!Character.isDigit(expression.charAt(i))){
                char oper = expression.charAt(i);
                if(!operList.contains(oper)){
                    operList.add(oper);
                }
                expList.add(expression.substring(flag, i));
                expList.add(Character.toString(oper));
                flag = i+1;
            }
        }
        expList.add(expression.substring(flag));
        char[] operArr = new char[operList.size()];
        for (int i = 0; i < operArr.length; i++) {
            operArr[i] = operList.get(i);
        }

        answer = permutation(expList, operArr, 0, answer);
        return answer;
    }
    
    static long permutation(LinkedList<String> expList, char[] operArr, int depth, long answer) {
        if (depth == operArr.length) {
            long ret = calculate(expList, operArr);
            
            answer = Math.max(ret, answer);
            return answer;
        }
        for (int i = depth; i < operArr.length; i++) {
            swap(operArr, i, depth);
            answer = permutation(expList, operArr, depth + 1, answer);
            swap(operArr, i, depth);
        }
        return answer;
    }

    static void swap(char[] arr, int i, int j) {
        char tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
    
    static long calculate(LinkedList<String> list, char[] operArr){
        LinkedList<String> expList = new LinkedList<>(list);
        
        for(int i=0; i<operArr.length; i++){
            if (operArr[i] == '+') {
                int j = 0;
                while(expList.size() >= 2 && expList.size()-1 > j) {
                    if(expList.get(j).equals("+")){
                        long tmp = Long.parseLong(expList.get(j-1)) + Long.parseLong(expList.get(j+1));
                        expList.remove(j+1);
                        expList.remove(j);
                        expList.remove(j-1);
                        if(j-2<=0){
                            expList.addFirst(Long.toString(tmp));
                            j=0;
                        }else{
                            expList.add(j-1, Long.toString(tmp));    
                            j-=1;
                        }
                    }
                    j++;
                }
                
            }
            
            if (operArr[i] == '-') {
                int j = 0;
                while(expList.size() >= 2 && expList.size()-1 > j) {
                    if(expList.get(j).equals("-")){
                        long tmp = Long.parseLong(expList.get(j-1)) - Long.parseLong(expList.get(j+1));
                        expList.remove(j+1);
                        expList.remove(j);
                        expList.remove(j-1);
                        if(j-2<=0){
                            expList.addFirst(Long.toString(tmp));
                            j=0;
                        }else{
                            expList.add(j-1, Long.toString(tmp));  
                            j-=1;
                        }
                    }
                    j++;
                }
            }
            if (operArr[i] == '*') {
                int j = 0;
                while(expList.size() >= 2 && expList.size()-1 > j) {
                    if(expList.get(j).equals("*")){
                        long tmp = Long.parseLong(expList.get(j-1)) * Long.parseLong(expList.get(j+1));
                        expList.remove(j+1);
                        expList.remove(j);
                        expList.remove(j-1);
                        if(j-2<=0){
                            expList.addFirst(Long.toString(tmp));
                            j=0;
                        }else{
                            expList.add(j-1, Long.toString(tmp));    
                            j-=1;
                        }
                    }
                    j++;
                }
            }   
        }
        long answer = Long.parseLong(expList.get(0));
        return Math.abs(answer);
    }
    
}