"""
연산자는 무조건 양쪽에 수는 가진다.
같은 연사자는 먼저나오는게 높다?-> 연산자가 3개이니, 최대 경우의수가 6일 뿐.
==>> 결론적으로 연산자 우선순위가 정해지면 계산해내는 함수만 있으면 된다.

두 개 리스트로?
숫자 리스트
연산 리스트 -> 자기인덱스와 다음 인덱스 -> 값이 합쳐지면? => 숫자 연산 둘다 지워주자.

-> insert가 있으니 linked로 구현 -> linked list가 없다..
-> deque 사용 -> 숫자큐 하나 연산자 큐하나써서
-> 연사자 뽑고 연사잔 별로 한바퀴 돌면서 연산하기
ex.))
1 2 3 4
- * -
=== iter 1 ===
2 3 4 1
* - -

4 1 6
- -

1 6 4 (의도한 연산자가 아니면 무조건 옮겨주기)
- -
=== iter 2 ===
1 6 4
- -

-5 4
-

-9
"""
import collections, re, copy

perms = []
visit = [False, False, False]
operations = []
num_queue = collections.deque()
oper_queue = collections.deque()

def make_perm(target_len, perm):  # -> permutation import로 대체 가능?!
    """ 순열로 우선순위 만든 후 저장"""
    global perms
    if len(perm) == target_len:
        perms.append(copy.deepcopy(perm))
        return

    for idx in range(target_len):
        if not visit[idx]:
            perm.append(idx)
            visit[idx] = True
            make_perm(target_len, perm)
            perm.pop()
            visit[idx] = False


def calculate(oper_list):
    """주어진 연산자 우선순위로 계산해주는 함수"""
    num_que = copy.deepcopy(num_queue)
    oper_que = copy.deepcopy(oper_queue)

    for oper_idx in oper_list:
        for _ in range(len(oper_que)):
            if operations[oper_idx] == oper_que[0]:  # 우선순위에 해당하는 연산자 meet
                express = str(num_que.popleft()) + oper_que.popleft() + str(num_que.popleft())  # 식만들기
                num_que.appendleft(eval(express))
            else:  # 이외의 연산자인 경우, 반시계이동
                oper_que.rotate(-1)
                num_que.rotate(-1)
        num_que.rotate(-1)  # 마지막 숫자는 rotate 필요
    return num_que[0]


def solution(expression):
    global num_queue
    global oper_queue
    global operations
    answer = 0

    for num in re.compile('[0-9]+').findall(expression):  # 숫자 추출
        num_queue.append(int(num))

    for oper in re.compile('[-+*]').findall(expression):  # 연산자 추출
        oper_queue.append(oper)

    # 등장한 연산자 중복제거
    operations = list(set(oper_queue))
    make_perm(len(operations), [])  # 순열만들기
    
    for oper_list in perms:  # 순열로 뽑은 연산자 리스트
        answer = max(answer, abs(calculate(oper_list)))
    return answer