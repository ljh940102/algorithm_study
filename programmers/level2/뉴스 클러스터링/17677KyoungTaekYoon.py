import copy
def solution(str1, str2):
    answer = 0
    set1 = []
    set2 = []

    for i in range(1,len(str1)):
        if str1[i-1].isalpha() and str1[i].isalpha():
            set1Char = str1[i-1] + str1[i]
            set1Char = set1Char.lower()
            set1.append(set1Char)
    for i in range(1, len(str2)):
        if str2[i-1].isalpha() and str2[i].isalpha():
            set2Char = str2[i-1] + str2[i]
            set2Char = set2Char.lower()
            set2.append(set2Char)
    if len(set1) == 0 and len(set2) == 0:
        return 65536
    inter = intersect(set1, set2)
    pl = len(set1) + len(set2)
    #intersect = set1 & set2
    #plus = set1 | set2
    answer = int((len(inter) / (pl - len(inter))) * 65536)
    return answer

def intersect(set1, set2):
    answer = []
    visited = [0] * len(set2)
    for i in range(len(set1)):
        for j in range(len(set2)):
            if visited[j] == 0:
                if set1[i] == set2[j]:
                    answer.append(set1[i])
                    visited[j] = 1
                    break
    return answer

def plus(set1, set2):
    answer = []
    visited = [0] * len(set2)
    for i in range(len(set1)):
        answer.append(set1[i])
    for j in range(len(set2)):
        if visited[j] == 0:
            if set1[i] == set2[j]:
                answer.append(set1[i])
    return answer

#답 찾아본거