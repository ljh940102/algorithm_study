import java.util.*;

class Solution {
    public int solution(String str1, String str2) {
        int answer = 0;
        
        ArrayList<String> str1List = new ArrayList<>();
        String2List(str1, str1List);
        
        ArrayList<String> str2List = new ArrayList<>();
        String2List(str2, str2List);
        
        ArrayList<String> intersection = new ArrayList<>();
        ArrayList<String> union = new ArrayList<>();
        
        for(String tmp : str1List){
            if(str2List.contains(tmp)){
                //삭제하지 않고 합집합을 구할 수 있는 방법은 없을까? Hash를 이용한 합집합 구하는 방법은 중복값이 모두 제거됨.
                str2List.remove(tmp);
                intersection.add(tmp);
            }
            union.add(tmp);
        }
        union.addAll(str2List);
              
        if(union.size() == 0) {
            return 65536;
        } else {
            return (int)((double)intersection.size() / (double)union.size() * 65536);
        }
    }
    
    static void String2List (String str, ArrayList<String> list){
        str = str.toLowerCase();
        for(int i = 0 ; i < str.length() - 1 ; ++i){
            if(97 <= str.charAt(i) && str.charAt(i) <= 122 && 
               97 <= str.charAt(i+1) && str.charAt(i+1) <= 122){
                list.add(str.substring(i, i+2));
            }
        }
        Collections.sort(list);
    }
}