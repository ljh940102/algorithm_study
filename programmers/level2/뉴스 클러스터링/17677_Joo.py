import re
from collections import Counter

def solution(str1, str2):
    answer = 0

    # 소문자 통일 -> 여기서 특수문자와 공백을 제거하면 안됨
    target1 = str1.lower()
    target2 = str2.lower()
    # 2개씩 슬라이딩
    target1_list = [target1[idx]+target1[idx+1] for idx in range(len(target1)) if idx < len(target1)-1]
    target2_list = [target2[idx]+target2[idx+1] for idx in range(len(target2)) if idx < len(target2)-1]
    # 알파벳으로만 이루어진 쌍 추출
    target1_list = [x for x in target1_list if re.compile('[a-z]{2,2}').findall(x)]
    target2_list = [x for x in target2_list if re.compile('[a-z]{2,2}').findall(x)]
    # 집합별 개수구하기
    target1_dict = dict(Counter(target1_list))
    target2_dict = dict(Counter(target2_list))

    # 교집합 점수구하기
    inter_point = 0
    for element in set(target1_list) & set(target2_list):
        inter_point += min(target1_dict.get(element), target2_dict.get(element))
    # 합집합 점수구하기
    union_point = 0
    for element in set(target1_list) | set(target2_list):
        union_point += max(target1_dict.get(element,0), target2_dict.get(element,0))

    if inter_point == 0 and union_point == 0:
        answer = 65536
    else:
        answer = int((inter_point/union_point)*65536)
    return answer

solution("FRANCE", "french")
solution("handshake", "shake hands")
solution("aa1+aa2", "AAAA12")
solution("E=M*C^2", "e=m*c^2")
