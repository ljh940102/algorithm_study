import itertools
from collections import defaultdict
def solution(relation):
    temp = []
    for i in range(len(relation[0])):
        temp.append(i)
    combs = []
    keyList = []
    for i in range(1, len(temp) + 1):
        els = [list(x) for x in itertools.combinations(temp, i)]
        combs.extend(els)
    for combination in combs:
        isDuplicate = False
        for key in keyList:
            findKeyTemp = all(elem in combination for elem in key)
            if findKeyTemp:
                isDuplicate = True
                break
        if not isDuplicate:
            tempDict = defaultdict(int)
            fail = False
            for info in relation:
                tempKey = []
                for pos in combination:
                    tempKey.append(info[pos])
                tempKey = tuple(tempKey)
                if tempDict[tempKey] == 1:
                    fail = True
                    break
                tempDict[tempKey] = 1
            if not fail:
                keyList.append(combination)
    return keyList

relation = [["100","ryan","music","2"],["200","apeach","math","2"],["300","tube","computer","3"],["400","con","computer","4"],["500","muzi","music","3"],["600","apeach","music","2"]]
print(solution(relation))