import copy
"""
유일성은 키를 붙여서 dict로해결
최소성은 조합으로 검사 -> 비트마스크 기법
ex) 1001 과 1101 이라면 -> AND -> 1001이다. AND연산의 결과가 target과 같다면. 최소성 위반!

가장 먼저 할 것은 모든 조합 만들기
짧은 조합부터 검사 시작(최소성을 유효하게 만들기 위하여)
유일성 + 최소성 만족시 정답(rtn_data)에 append 
"""

rtn_data = []
selected_candidates = []  # 선택된 후보키 저장(for 최소성 검사)
col_len, row_len = 0, 0

def check_unique(relation, target):
    """
    유일성을 체크한다. 유일성은 조합으로 뽑힌 컬럼 값을 이어 붙인 뒤, set으로 판변한다.
    """
    check = set()
    for row in range(row_len):
        current_value = [relation[row][idx] for idx in target]

        if "".join(current_value) in check:  # 중복! -> 위반
            return False
        else:
            check.add("".join(current_value))
    return True


def check_minumum(target):
    """
    최소성을 체크한다.
    최소성은 후보키로 뽑힌 대상(selected_candidates)과 비트 AND 연산을 수행하여 판별한다. (비트마스크)
    기본적으로 컬럼 선택여부는 하나의 비트로 표현된다.
    ex) 첫번째, 세번째 컬럼선택 -> 101 -> 5
    ex) 첫번째, 두번째, 새번째, 네번째 컬럼선택 -> 1111 -> 15
    selected_candidates에 5(101)이 들어 있다면 15(1111)은 최소성을 위반한다.
    Why? 1111 & 101은 101이다.
    """
    check = 0
    for position in target:
        check += pow(2, position)

    is_minimum = True
    for compare in selected_candidates:
        if compare == (check & compare):  # AND연산으로 중복체크
            is_minimum = False
    return is_minimum



def make_candidate(relation, target, size, start):
    """
    가능한 컬럼의 모든 조합을 만듦과 동시에 유일성, 최소성을 체크하여 만족하는 경우에만 rtn_data에 append한다.
    (가장 짧은 조합부터 만드므로 최소성 체크 가능)
    """
    global rtn_data, selected_candidates
    if len(target) == size:
        if check_unique(relation, target) and check_minumum(target):  # 두 조건 모두 만족시
            rtn_data.append(copy.deepcopy(target))
            selected_candidates.append(sum([pow(2, idx) for idx in target]))  # 선택한 컬럼 인덱스를 2의 지수로 주어 총합 저장
                                                                              # ex) 101 -> 5 저장

    for idx in range(start, col_len):
        index_list = list(range(col_len))  # input의 각 컬럼의 인덱스를 나타낸다.
        target.append(index_list[idx])  # 선택한 컬럼의 인덱스를 apeend함에 유의
        make_candidate(relation, target, size, idx+1)
        target.pop()


def solution(relation):
    global rtn_data, selected_candidates
    global col_len, row_len

    row_len, col_len = len(relation), len(relation[0])
    # bit_relation = [x for x in range(col_len)]

    for size in range(1, col_len+1):  # 1개부터 col 개수까지 조합만들기
        make_candidate(relation, [], size, 0)
    return len(rtn_data)


aa = [["100","ryan","music","2"],["200","apeach","math","2"],["300","tube","computer","3"],["400","con","computer","4"],["500","muzi","music","3"],["600","apeach","music","2"]]

print(solution(aa))
