import java.util.*;

class Solution {
    public int solution(String[][] relation) {
        int answer = 0;
        int row = relation.length;
        int col = relation[0].length;

        boolean[] check = new boolean[col];
        Set<String> ckSet = new HashSet<>();

        for (int i = 0; i < col; i++) {
            List<List<Integer>> subSet = getSubSet(col, i + 1);

            for (List<Integer> list : subSet) {
                for (int j = 0; j < row; j++) {
                    StringBuilder record = new StringBuilder();

                    for (Integer integer : list) {
                        if (check[integer]) {
                            break;
                        }

                        record.append(relation[j][integer]);
                    }
                    ckSet.add(record.toString());
                }

                if (ckSet.size() == row) {
                    answer++;

                    for (Integer integer : list) {
                        check[integer] = true;
                    }
                }

                ckSet.clear();
            }
        }

        return answer;
    }

    private List<List<Integer>> getSubSet(int col, int select) {
        List<List<Integer>> subSet = new LinkedList<>();

        for (int i = 0; (1 << col) > i; i++) {
            List<Integer> list = new LinkedList<>();

            for (int j = 0; j < col; j++) {
                if (((1 << j) & i) > 0) {
                    list.add(j);
                }
            }

            if (list.size() > 0) {
                subSet.add(list);
            }
        }

        return subSet;
    }
}