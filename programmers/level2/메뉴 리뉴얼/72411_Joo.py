"""
메뉴이름의 쌍이 key인 dict 사용(단, 정렬해서 key로 사용)
return은 길이와 상관없이 알파벳 오름차순
"""
from itertools import combinations
from collections import defaultdict

def solution(orders, course):
    answers = []
    manu_set = defaultdict(int)

    for order in orders:
        for num_manu in course:
            for manu_combination in combinations(order, num_manu):
                manu_set[tuple(sorted(list(manu_combination)))] += 1

    len_dict = defaultdict(list)
    for k, v in manu_set.items():
        if v < 2:
            continue

        len_dict[len(k)].append([v, "".join(list(k))])  # [등장횟수, 메뉴조합]

    for length, manus in len_dict.items():
        max_count = max(manus, key=lambda x: x[0])[0]  # 등장횟수 기준 max 추출
        answers.extend([manu[1] for manu in manus if manu[0] == max_count])

    return sorted(answers)

print(solution(["ABCDE", "AB", "CD", "ADE", "XYZ", "XYZ", "ACD"], [2,3,5]))
print(solution(["ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH"], [2,3,4]))
print(solution(["XYZ", "XWY", "WXA"], [2,3,4]))
