import java.util.*;

class Solution {
    public String[] solution(String[] orders, int[] courses) {
        List<String> result = new LinkedList<>();
        List<List<Integer>> orderList = new LinkedList<>();
        for (String order : orders) {
            List<Integer> charList = new LinkedList<>();

            for (char c : order.toCharArray()) {
                charList.add((int) c - 65);
            }

            orderList.add(charList);
        }

        for (int course : courses) {
            List<Integer[]> subResult = new LinkedList<>();
            List<Integer[]> combinations = generate(26, course);

            int max = 0;
            for (Integer[] combination : combinations) {
                int count = 0;
                for (List<Integer> order : orderList) {
                    if (order.containsAll(Arrays.asList(combination))) {
                        count++;
                    }
                }

                if (count >= 2) {
                    if (max == count) {
                        subResult.add(combination);
                    } else if (max < count) {
                        max = count;
                        subResult = new LinkedList<>();
                        subResult.add(combination);
                    }
                }
            }

            for (Integer[] integers : subResult) {
                StringBuilder stringBuilder = new StringBuilder();
                for (Integer integer : integers) {
                    int convert = integer + 65;

                    stringBuilder.append(((char) convert));
                }

                result.add(stringBuilder.toString());
            }
        }

        // String[]::new == new String[0] ??
        String[] answer = result.toArray(String[]::new);
        Arrays.sort(answer);
        return answer;
    }

    public List<Integer[]> generate(int n, int r) {
        List<Integer[]> combinations = new ArrayList<>();
        func(combinations, new Integer[r], 0, n-1, 0);

        return combinations;
    }

    private void func(List<Integer[]> combinations, Integer[] data, int start, int end, int index) {
        if (index == data.length) {
            Integer[] combination = data.clone();
            combinations.add(combination);

        } else if (start <= end) {
            data[index] = start;
            func(combinations, data, start + 1, end, index + 1);
            func(combinations, data, start + 1, end, index);
        }
    }
}