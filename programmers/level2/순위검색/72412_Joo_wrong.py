def solution(info, query):
    answer = []
    LANGS, JOBS, GROUPS, FOODS = ['cpp', 'java', 'python'], ['backend', 'frontend'], ['junior', 'senior'], ['chicken', 'pizza']
    applicants = dict()

    for LANG in LANGS:
        applicants[LANG] = dict()
        for JOB in JOBS:
            applicants[LANG][JOB] = dict()
            for GROUP in GROUPS:
                applicants[LANG][JOB][GROUP] = dict()
                for FOOD in FOODS:
                    applicants[LANG][JOB][GROUP][FOOD] = list()

    for target_info in info:
        lang, job, group, food, score = target_info.split()
        score = int(score)
        applicants[lang][job][group][food].append(score)

    for q in query:
        q = q.split()
        target = list()
        lang, job, group, food, score = q[0], q[2], q[4], q[6], q[7]
        score = int(score)
        if lang == '-':  # all
            for LANG in LANGS:
                target.append(applicants[LANG])
        else:
            target.append(applicants[lang])

        target1 = list()
        if job == '-':
            for t in target:
                for JOB in JOBS:
                    target1.append(t[JOB])
        else:
            for t in target:
                target1.append(t[job])

        target = list()
        if group == '-':
            for t in target1:
                for GROUP in GROUPS:
                    target.append(t[GROUP])
        else:
            for t in target1:
                target.append(t[group])

        target1 = list()
        if food == '-':
            for t in target:
                for FOOD in FOODS:
                    target1.extend(t[FOOD])
        else:
            for t in target:
                target1.extend(t[food])

        filtered = [tt for tt in target1 if tt >= score]
        answer.append(len(filtered))

    return answer

print(solution(["java backend junior pizza 150","python frontend senior chicken 210","python frontend senior chicken 150","cpp backend senior pizza 260","java backend junior chicken 80","python backend senior chicken 50"]
         ,["java and backend and junior and pizza 100","python and frontend and senior and chicken 200","cpp and - and senior and pizza 250","- and backend and senior and - 150","- and - and - and chicken 100","- and - and - and - 150"]))