import java.util.*;

class Information {
    String info;
    int score;

    Information(String info, int score) {
        this.info = info;
        this.score = score;
    }
}

class Solution {
    public int[] solution(String[] infos, String[] query) {
        int[] answer = new int[query.length];
        Map<String, List<Integer>> candidates = new HashMap<>();

        for (String info : infos) {
            List<Information> informationList = findCases(info);

            for (Information information : informationList) {
                List<Integer> scoreList = candidates.get(information.info);

                if (scoreList == null) {
                    scoreList = new ArrayList<>();
                    scoreList.add(information.score);
                    candidates.put(information.info, scoreList);

                } else {
                    scoreList.add(information.score);
                    candidates.put(information.info, scoreList);
                }
            }
        }

        for (String key : candidates.keySet()) {
            // Map 순서 정렬 TreeMap
            // Map<Integer, Integer> treeMap = new TreeMap<>(candidates.get(key));
            List<Integer> list = candidates.get(key);
            Collections.sort(list);
            candidates.put(key, list);
        }

        for (int i = 0; i < query.length; i++) {
            String[] splitQuery = query[i].split(" ");
            String info = splitQuery[0] + splitQuery[2] + splitQuery[4] + splitQuery[6];
            int score = Integer.parseInt(splitQuery[7]);

            List<Integer> list = candidates.get(info);
            if (list == null) {
                answer[i] = 0;

            } else {
                int value = getBinarySearch(list, score);
                answer[i] = list.size() - value;
            }
        }

        return answer;
    }

    private int getBinarySearch(List<Integer> list, int score) {
        int start = 0;
        int end = list.size() - 1;

        while(end >= start) {
            int median = (start + end) / 2;
            if(list.get(median) < score) {
                start = median + 1;

            } else {
                end = median - 1;
            }
        }

        return start;
    }

    private List<Information> findCases(String info) {
        String[] splitInfo = info.split(" ");
        List<Information> list = new ArrayList<>();
        for (int a = 0; a < 2; a++) {
            for (int b = 0; b < 2; b++) {
                for (int c = 0; c < 2; c++) {
                    for (int d = 0; d < 2; d++) {
                        String language = a == 0 ? "-" : splitInfo[0];
                        String work = b == 0 ? "-" : splitInfo[1];
                        String career = c == 0 ? "-" : splitInfo[2];
                        String food = d == 0 ? "-" : splitInfo[3];
                        int score = Integer.parseInt(splitInfo[4]);

                        if (b == 0) {
                            work = "-";
                        }

                        if (c == 0) {
                            career = "-";
                        }

                        if (d == 0) {
                            food = "-";
                        }

                        list.add(new Information(language + work + career + food, score));
                    }
                }
            }
        }

        return list;
    }
}
