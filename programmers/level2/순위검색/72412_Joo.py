from itertools import combinations
import bisect

def solution(info, query):
    answer = []
    all_conbination = {}
    for applicant in info:
        applicant_data = applicant.split()
        conditions = applicant_data[:4]  # 조건 추출
        score = int(applicant_data[-1])  # score 추출
        
        for n in range(5):
            condition_combi = list(combinations(range(4), n))  # 가능한 모든 조합 추출

            for selected_condition in condition_combi:
                condition_list = conditions.copy()
                for condi in selected_condition:  # 선택된 조건은 -으로 변경
                    condition_list[condi] = '-'
                
                changed_condition_list = '::'.join(condition_list)  # ::로 연결하여 key로 사용

                # 새로 만든 key로 데이터 삽입
                if changed_condition_list in all_conbination:
                    all_conbination[changed_condition_list].append(score)
                else:
                    all_conbination[changed_condition_list] = [score]

    # 딕셔너리 내 모든 값 정렬
    for value in all_conbination.values():
        value.sort()
    
    # query 조건에 부합하는 데이터 개수 추출
    for q in query:
        look_query = [look_condi for look_condi in q.split() if look_condi != 'and']

        look_query_condi = '::'.join(look_query[:4])
        look_query_score = int(look_query[-1])

        if look_query_condi in all_conbination:  # 조회 조건에 해당하는 값이 있으면
            data = all_conbination[look_query_condi]

            # 처음으로 score 조건 값과 같거나 큰 인덱스를 lower bound 알고리즘으로 find
            lower_bound_idx = bisect.bisect_left(data, look_query_score)
            answer.append(len(data) - lower_bound_idx)  # lower bound 인덱스부터 끝까지의 개수가 정답
        else:
            answer.append(0)

    return answer