import java.util.*;
import java.util.stream.IntStream;

class Solution {
    public int[] solution(String s) {
        int[] answer = {};
        s = s.substring(2, s.length()-2).replace("},{", "-");
        String [] tmp = s.split("-");
        answer = new int[tmp.length];
        
        //길이가 짧은 것부터 정렬
        Arrays.sort(tmp, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        
        

        for(int i=0; i<tmp.length; i++) {
            String [] tuple = tmp[i].split(",");
            
            for(int j=0 ; j<tuple.length ; j++) {
                int num = Integer.parseInt(tuple[j]);
                if(!IntStream.of(answer).anyMatch(x -> x == num)) {
                    answer[i] = num;
                }
            }
        }
        return answer;
    }
}