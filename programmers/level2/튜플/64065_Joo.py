def solution(s):

    s = s.replace("{","").replace("},", "|").replace("}", "")
    a = s.split("|")

    input = []
    for target in a:
        element = target.split(",")
        element = list(map(int, element))
        input.append(element)
    #### list변환 완료 -> 이게 꼭 필요한 부분인가.. -> 정규표현식을 쓰면 좀 더 좋았겠네

    answer = []
    input = sorted(input, key=lambda x:len(x))

    check = dict()
    for target in input:
        for val in target:
            if not check.get(val):
                answer.append(val)
                check[val] = 1

    return answer


print(solution("{{1,2,3},{2,1},{1,2,4,3},{2}}"))