from collections import Counter


def solution(s):
    s = s.replace('{', '')
    s = s.replace('}', '')
    counter = Counter(s.split(','))
    counter = sorted(counter.items(), key=lambda x: -x[1])
    answer = [int(element) for element, freq in counter]
    return answer
