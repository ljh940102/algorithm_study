from collections import deque
def solution(cacheSize, cities):
    answer = 0
    cache = deque(maxlen=cacheSize)
    cacheHit = 1
    cacheMiss = 5
    for city in cities:
        city = city.lower()
        if city in cache:
            answer +=1
        else:
            answer += 5
        cache.appendleft(city)
    return answer

cities = ["Jeju", "Pangyo", "Seoul", "NewYork", "LA", "Jeju", "Pangyo", "Seoul", "NewYork", "LA"]
cacheSize = 3
solution(cacheSize,cities)