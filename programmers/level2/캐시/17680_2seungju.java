import java.util.*;

class Solution {
    private static final int MAX_LENGTH = 100_000;

    public int solution(int cacheSize, String[] cities) {
        if (cacheSize == 0) {
            return cities.length * 5;
        }

        int answer = 0;

        cities = Arrays.stream(cities).map(String::toLowerCase).toArray(String[]::new);

        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < cities.length; i++) {
            String city = cities[i];
            if (map.get(city) == null) {
                answer += 5;

                if (map.size() >= cacheSize) {
                    map.remove(getMinValueKey(map));
                }
            } else {
                answer++;
            }

            map.put(city, i);
        }

        return answer;
    }

    private String getMinValueKey(Map<String, Integer> map) {
        int min = MAX_LENGTH;
        String minValueKey = "";
        for (String key : map.keySet()) {
            int value = map.get(key);

            if (value < min) {
                minValueKey = key;
                min = value;
            }
        }

        return minValueKey;
    }
}