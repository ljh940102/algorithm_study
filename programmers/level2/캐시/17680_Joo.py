"""
LRU -> 가장 앞쪽에 있는 놈 빼기
    -> 존재여부는 hashmap으로 관리
"""

import collections
def solution(cacheSize, cities):
    if cacheSize == 0:
        return len(cities)*5
    answer = 0
    cache = collections.deque()
    cache_map = set()
    # cities = map(lambda x:x.lower(), cities)
    cities = [x.lower() for x in cities]
    """
    map과 list comprehension이 같은 기능을 한다면 comprehension 사용이 좋름
    -> comprehension을 사용하는 것이 좀더 명시적이고 파이썬 스러운 코드
       또한, lambda를 선언하는 경우 속도도 comprehension이 빠름.
       단. 미리 정의된 함수를 사용할 떈 map이 약속하게 빠름. 하지만 그렇다고 map을 고집할 이유는 못 됨. 
    """
    
    for city in cities:
        if city in cache_map:  # 캐시에 존재 -> 최우측으로 이동
            cache.remove(city)
            cache.append(city)
            answer += 1
        else:  # 캐시에 미존재 -> 최좌측에서 뺴고, city는 최우측 삽입
            if len(cache) < cacheSize:  # 단, 캐시가 남았으면 append
                cache.append(city)
                cache_map.add(city)
            else:
                removed = cache.popleft()
                cache_map.remove(removed)
                cache.append(city)
                cache_map.add(city)
            answer += 5
    return answer

print(solution(5,["Jeju", "Pangyo", "Seoul", "NewYork", "LA", "SanFrancisco", "Seoul", "Rome", "Paris", "Jeju", "NewYork", "Rome"]))