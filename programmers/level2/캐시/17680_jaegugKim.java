import java.util.*;
class Solution {
    public int solution(int cacheSize, String[] cities) {
        if(cacheSize == 0){
            return 5*cities.length;
        }
        int answer = 0;
        LinkedList<String> cache = new LinkedList<>();
        LinkedList<Integer> order = new LinkedList<>();
        
        int flag = 0;
        for(int i = 0; i<cities.length; i++){
            String city = cities[i].toLowerCase();
            if(!cache.contains(city) && cache.size() < cacheSize){
                cache.add(city);
                flag++;
                order.add(flag);
                answer += 5;
            }
            else if (!cache.contains(city) && cache.size() >= cacheSize){
                int idx = order.indexOf(Collections.min(order));
                cache.remove(idx);
                order.remove(idx);
    
                cache.add(city);
                flag++;
                order.add(flag);
                answer += 5;
            }
            else{
                flag++;
                order.set(cache.indexOf(city), flag);
                answer++;
            }
        }
        
        return answer;
    }
}