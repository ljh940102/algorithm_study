import java.util.*;
class Solution {
    public int[] solution(String msg) {
        int[] answer = {};
        ArrayList<String> lib = new ArrayList<>();
        ArrayList<Integer> ans = new ArrayList<>();
        //사전 초기화
        for(int i = 65; i < 91; i++){
            lib.add(Character.toString((char)i));    
        }
        
        int max = 1; //사전에 등록된 문자열의 최대길이를 저장
        for(int i=0; i<msg.length(); i++){
            for(int j=i+max; j>i; j--){
                //i는 문자열의 시작, idx는 끝. idx는 msg 길이로 제한
                int idx = j;
                if( idx >=msg.length()){
                    idx = msg.length();
                }
                
                String w = msg.substring(i,idx);
                if(lib.contains(w)){ //w가 lib에 있다면 lib의 인덱스를 ans에 더해준다.
                    ans.add(lib.indexOf(w)+1);
                    if(idx<msg.length()){ //문자열이 끝나지 않았다면 w+c 를 lib에 저장
                        lib.add(w + msg.charAt(idx));    
                    }else{ //문자열이 끝났으면 w 만 lib에 저장
                        lib.add(w);   
                    }
                    if(idx-i+1 > max){ // w의 최대길이를 기록
                        max = idx-i+1; 
                    }
                    i = idx-1; // c의 인덱스부터 다시 loop를 시작한다.
                    break;
                }
            }
        }
        
        answer = new int[ans.size()];
        for(int i =0; i<ans.size(); i++){
            answer[i] = ans.get(i);
        }
        
        return answer;
    }
}
