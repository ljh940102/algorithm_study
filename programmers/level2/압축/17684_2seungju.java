import java.util.*;

class Solution {
    private List<Integer> result = new ArrayList<>();
    private String message;

    public int[] solution(String msg) {
        Map<String, Integer> dictionary = new HashMap<>();

        message = msg;
        for (int i = 0; i < 26; i++) {
            char word = (char) (65 + i);
            dictionary.put(String.valueOf(word), i + 1);
        }

        while (message.length() > 0) {
            func(dictionary, message);
        }

        return result.stream().mapToInt(x -> x).toArray();
    }

    private void func(Map<String, Integer> dictionary, String word) {
        if (dictionary.get(word) != null) {
            result.add(dictionary.get(word));

            if (message.length() > word.length() + 1 && dictionary.get(message.substring(0, word.length() + 1)) == null) {
                dictionary.put(message.substring(0, word.length() + 1), dictionary.size() + 1);

            }
            message = message.substring(word.length());

        } else {
            func(dictionary, word.substring(0, word.length() -1));
        }
    }
}