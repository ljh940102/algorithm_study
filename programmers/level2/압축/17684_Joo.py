def solution(msg):
    answer = []

    # 기본사전 생성
    dictionary = dict()
    for val in range(1, 27):
        dictionary[chr(val + 64)] = val

    insert_index = 27  # 새로 사전에 넣을 index 값
    idx, size = 0, 1  # 비교할 문자열 시작인덱스와 size

    while True:
        # 사전에 있는 가장 긴 단어 찾기
        target_str = msg[idx:idx + size]
        while dictionary.get(target_str):
            size += 1
            target_str = msg[idx:idx + size]

            # 문자열 끝에 도달한 경우 return
            if idx + size > len(msg):
                answer.append(dictionary[target_str])
                return answer

        # 사전에 없는 경우 로직
        answer.append(dictionary[target_str[:-1]])
        dictionary[target_str] = insert_index
        insert_index += 1
        idx = idx + size - 1
        size = 1

    return answer

print(solution("TOBEORNOTTOBEORTOBEORNOT"))
