"""
user id를 key로하는 사전으로 데이터 관리하면 될듯.
최종 출력도 리스트로 가지고 있다 마지막 유정정보 dict이용해서 변환하여 출력
"""

MESSAGE = dict()  # action에 따른 메세지 출력
MESSAGE['Enter'] = "님이 들어왔습니다."
MESSAGE['Leave'] = "님이 나갔습니다."

def solution(record):

    user_info = dict()  # user_id를 키로, nickname을 값으로 하는 dict
    history = []        # 최종 출력할 내역

    for line in record:
        if line[:5] == 'Leave':
            action, user_id = line.split(" ")
        else:
            action, user_id, nickname = line.split(" ")

        if 'Leave' != action:
            user_info[user_id] = nickname

        if 'Change' != action:
            history.append([user_id, action])

    rtn = [user_info.get(user_id)+MESSAGE.get(action) for user_id, action in history]
    return rtn


# print(solution(["Enter uid1234 Muzi", "Enter uid4567 Prodo", "Leave uid1234", "Enter uid1234 Prodo", "Change uid4567 Ryan"]))