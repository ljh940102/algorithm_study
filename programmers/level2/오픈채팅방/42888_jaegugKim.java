import java.util.*;
class Solution {
    public String[] solution(String[] record) {
        String[] answer = {};
        HashMap<String, String> uidName = new HashMap<String, String>();
        ArrayList<String> chatList = new ArrayList<>();
        
        for(int i = 0; i<record.length; i++){
            String[] tmp = record[i].split(" ");
            for(int j = 0; j < tmp.length; j++){
                if(tmp[0].equals("Enter") || tmp[0].equals("Change")){
                    uidName.put(tmp[1], tmp[2]);
                }
            }
        }
        
        for(int i = 0; i<record.length; i++){
            String[] tmp = record[i].split(" ");
                if(tmp[0].equals("Enter")){
                    chatList.add(uidName.get(tmp[1])+"님이 들어왔습니다.");
                }else if(tmp[0].equals("Leave")){
                    chatList.add(uidName.get(tmp[1])+"님이 나갔습니다.");
                }
        }
        
        answer = new String[chatList.size()];
        for(int i = 0; i<chatList.size(); i++){
            answer[i] = chatList.get(i);
        }
        return answer;
    }
}