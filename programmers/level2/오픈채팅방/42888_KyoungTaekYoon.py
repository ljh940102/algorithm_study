from collections import defaultdict
def solution(record):
    idDict = defaultdict(int)
    answer = []
    for sentence in record:
        if len(sentence.split(" ")) == 3:
            action, id, nickName = sentence.split(" ")
        else:
            action, id = sentence.split(" ")
        if action == "Enter":
            idDict[id] = nickName
        elif action == "Leave":
            continue
        elif action == "Change":
            idDict[id] = nickName

    for sentence in record:
        temp = ""
        if len(sentence.split(" ")) == 3:
            action, id, nickName = sentence.split(" ")
        else:
            action, id = sentence.split(" ")
        if action == "Enter":
            temp += idDict[id] + "님이 들어왔습니다."
        elif action == "Leave":
            temp += idDict[id] + "님이 나갔습니다."
        if temp != "":
            answer.append(temp)
    return answer

record = ["Enter uid1234 Muzi", "Enter uid4567 Prodo","Leave uid1234","Enter uid1234 Prodo","Change uid4567 Ryan"]
solution(record)