import java.util.*;

class Log {
    private String id;
    private String action;

    public Log(String id, String action) {
        this.id = id;
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String nickname) {
        this.id = nickname;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}

class Solution {
    public String[] solution(String[] record) {
        int m = record.length;
        int n = 3;
        Queue<Log> queue = new LinkedList<>();

        String[][] matrix = new String[m][n];

        for (int i = 0; i < m; i++) {
            String[] splitStr = record[i].split(" ");

            for (int j = 0; j < splitStr.length; j++) {
                matrix[i][j] = splitStr[j];
            }
        }

        Map<String, String> userMap = new HashMap<>();
        for (String[] strings : matrix) {
            String actionCode = strings[0];

            switch (actionCode) {
                case "Enter" :
                    queue.offer(new Log(strings[1], "님이 들어왔습니다."));
                    userMap.put(strings[1], strings[2]);
                    break;

                case "Change" :
                    userMap.put(strings[1], strings[2]);
                    break;

                default:
                    queue.offer(new Log(strings[1], "님이 나갔습니다."));
            }
        }

        String[] answer = new String[queue.size()];
        int index = 0;

        for (Log log : queue) {
            answer[index++] = userMap.get(log.getId()) + log.getAction();
        }

        return answer;
    }
}