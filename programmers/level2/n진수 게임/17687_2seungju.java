import java.util.*;

class Solution {
    private static final Map<Integer, String> map = Map.of(
            10, "A",
            11, "B",
            12, "C",
            13, "D",
            14, "E",
            15, "F"
    );

    public String solution(int n, int t, int m, int p) {
        StringBuilder sumConvert = new StringBuilder();

        for (int i = 0; i < t * m; i++) {
            sumConvert.append(getConvertN(n, i));
        }

        String[] strArray = sumConvert.toString().split("");

        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < strArray.length; i++) {
            if (i * m + p > strArray.length - 1 || answer.length() == t) {
                break;
            }

            answer.append(strArray[i * m + p - 1]);
        }

        return answer.toString();
    }

    private StringBuilder getConvertN(int n, int index) {
        StringBuilder stringBuilder = new StringBuilder();

        if (index == 0) {
            stringBuilder.append(0);

        } else {
            int count = index;

            while (count > 0) {
                int value = count % n;
                if (map.get(value) != null) {
                    stringBuilder.insert(0, map.get(value));

                } else {
                    stringBuilder.insert(0, value);
                }

                count /= n;
            }
        }
    }
}