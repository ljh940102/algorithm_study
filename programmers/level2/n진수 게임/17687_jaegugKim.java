class Solution {
    public String solution(int n, int t, int m, int p) {
        StringBuffer answer = new StringBuffer("");
        StringBuffer full = new StringBuffer("0");
        
        for(int i = 0; full.length() < m*(t-1)+p; i++) { //튜브가 마지막으로 말해야할 숫자까지
            full.append(convert(i, n)); //인덱스에 해당하는 숫자를 n진법으로 변환하여 full에 append
        }
        for (int i = 0; i < t; i++){
            answer.append(full.charAt(m*i+p-1)); //튜브가 말해야하는 숫자만 answer에 append
        }
        return answer.toString();
    }
    
    public StringBuffer convert(int num, int base){ //진법 변환 함수
        StringBuffer each = new StringBuffer("");
        while (num != 0) {
            if (num % base >= 10){ //10 이상은 알파뱃으로 변환
                each.append(String.valueOf((char) (num % base + 55)));   
            }else{
                each.append(String.valueOf(num % base));
            }
            num /= base;
        }
        return each.reverse();
    }
}
