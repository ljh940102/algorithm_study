"""
진법에 따라 (t*m)개까지 구하고, p와 m에 따라 인덱싱
"""
CHARACTER = "0123456789ABCDEF"

def convert(target, n):
    val, left = divmod(target, n)
    result = CHARACTER[left]
    return convert(val, n) + result if val != 0 else CHARACTER[left]  # 몫이 0인경우 나머지 리턴


def solution(n, t, m, p):
    answer_str = []
    criteria = t * m
    current_value = 0
    while True:
        if len(answer_str) > criteria:
            break
        answer_str.extend(list(convert(current_value, n)))
        current_value += 1

    return "".join(answer_str[slice(p-1, (m*t), m)])