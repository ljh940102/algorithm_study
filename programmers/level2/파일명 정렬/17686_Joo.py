"""
인풋을 정규표현식을 이용해 HEAD와 NUMBER로 나눈다. (TAIL을 버림) 나눈 값을 list로 저장.
단, 소문자변환한다. 또한 모두 같은경우 입력순서를 유지하므로 순서를 유지해준다.
HEAD, NUMBER 순으로 정렬한다. (sort함수가 stable이므로 같은 경우 순서는 유지 됨.)

"""
import re

def solution(files):
    # 정규표현식 선언
    head = re.compile('[a-zA-Z.\-\s]+')
    number = re.compile('[0-9]+')

    info = []
    for file in files:
        current_info = [file, head.search(file).group().lower(), int(number.search(file).group())]  # [origin_filename, HEAD, NUMBER]
        info.append(current_info)

    info = sorted(info, key=lambda x:(x[1],x[2]))
    answer = [x[0] for x in info]

    return answer
