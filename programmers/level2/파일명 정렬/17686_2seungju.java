import java.util.*;

class FileInfo {
    private String head;
    private String originNum;
    private int num;
    private String tail;

    FileInfo(String head, String num, String tail) {
        this.head = head;
        this.originNum = num;
        this.num = Integer.parseInt(num);
        this.tail = tail;
    }

    public String getHeadForCompare() {
        return head.toUpperCase();
    }

    // 숫자 원본 유지
    public String getOriginNum() {
        return originNum;
    }

    public String getHead() {
        return head;
    }

    public int getNum() {
        return num;
    }

    public String getTail() {
        return tail;
    }
}

class Solution {
    private static final List<String> numberList = List.of("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    public String[] solution(String[] files) {
        List<FileInfo> fileInfoList = new ArrayList<>();
        for (String file : files) {
            String[] splitName = file.split("");

            StringBuilder sumHead = new StringBuilder();
            StringBuilder sumNumber = new StringBuilder();
            StringBuilder sumTail = new StringBuilder();
            for (int i = 0; i < file.length(); i++) {
                if (sumTail.toString().equals("") && numberList.contains(splitName[i])) {
                    sumNumber.append(splitName[i]);

                } else if (sumNumber.toString().equals("")) {
                    sumHead.append(splitName[i]);

                } else {
                    sumTail.append(splitName[i]);
                }
            }

            fileInfoList.add(new FileInfo(sumHead.toString(), sumNumber.toString(), sumTail.toString()));
        }

        fileInfoList.sort(Comparator.comparing(FileInfo::getHeadForCompare).thenComparing(FileInfo::getNum));

        String[] answer = new String[files.length];
        for (int i = 0; i < answer.length; i++) {
            FileInfo fileInfo = fileInfoList.get(i);
            answer[i] = fileInfo.getHead() + fileInfo.getOriginNum() + fileInfo.getTail();
        }

        return answer;
    }
}