import java.util.*;
import java.util.regex.*;

class Solution {
    public String[] solution(String[] files) {
        Pattern headPattern = Pattern.compile("(.*?)[0-9]"); // 숫자로 끝나는 문자
        Pattern numPattern = Pattern.compile("[0-9]{1,5}"); // 5자 이내의 숫자
        
        Arrays.sort(files, new Comparator<String>() {
            @Override
            public int compare(String fileNm1, String fileNm2) {
                String head1 = patternMatch(headPattern, fileNm1, fileNm1, 1); //숫자를 제외한 첫번째 group
                String head2 = patternMatch(headPattern, fileNm2, fileNm2, 1);
                int result = head1.toLowerCase().compareTo(head2.toLowerCase());
                
                if ( result == 0 ) { //head가 동일하면 숫자 비교
                    String num1 = patternMatch(numPattern, fileNm1, "0", 0); //전체
                    String num2 = patternMatch(numPattern, fileNm2, "0", 0);                    
                    result = Integer.valueOf(num1) - Integer.valueOf(num2);
                }
                return result;
            }
        });		
        return files;	
    }

    //정규표현식에 맞는 문자열 추출 - 실패하면 문자열 fail 반환
    public String patternMatch (Pattern pattern, String fileNm, String fail, int idx){
        Matcher matcher = pattern.matcher(fileNm);
        return (matcher.find())?matcher.group(idx):fail;
    }
}
