"""
바구니는 stack
인형은 board[0]의 길이만큼의 queue을 선언해서 표현한다.
"""
from collections import deque

def solution(board, moves):
    answer = 0
    bucket, dolls = list(), list()
    row_len, col_len = len(board[0]), len(board)
    [dolls.append(deque()) for _ in range()]

    for idx_row in range(col_len):
        for idx_col in range(row_len):
            if board[idx_row][idx_col] != 0:
                dolls[idx_col].appendleft(board[idx_row][idx_col])

    for pick in moves:
        pick_idx = int(pick) - 1
        if len(dolls[pick_idx]) == 0:  # 해당열에 인형없으면 skip
            continue

        picked_doll = dolls[pick_idx].pop()
        if len(bucket) > 0:
            if bucket[-1] == picked_doll:
                bucket.pop()
                answer += 2
                continue
        bucket.append(picked_doll)

    return answer