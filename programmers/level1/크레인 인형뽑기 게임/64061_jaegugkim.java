import java.util.*;

class Solution {
    public int solution(int[][] board, int[] moves) {
        int answer = 0;
        ArrayList<Integer> basket = new ArrayList<>();
        
        for(int i =0; i<moves.length; i++){
            for(int j=0; j<board.length; j++){//위에서부터 순차적으로 탐색
                if(board[j][moves[i]-1]!=0){//인형 발견하면
                    if(basket.size()>=1 //바구니에 인형이 있고 **false면 다음 조건으로 넘어가지 않음 > 조건 순서가 바뀌면 에러 발생
                    && basket.get(basket.size()-1)==board[j][moves[i]-1]){//맨 위 인형이 발견한 인형과 같으면
                        answer=answer +2;
                        basket.remove(basket.size()-1);
                    }else{
                        basket.add(board[j][moves[i]-1]);
                    }
                    board[j][moves[i]-1]=0;//뽑은 인형은 0으로 변환
                    break;
                }
            }
        }
        return answer;
    }
}
