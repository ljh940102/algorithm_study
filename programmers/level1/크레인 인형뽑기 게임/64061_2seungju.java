import java.util.*;

class Solution {
    public static int solution(int[][] board, int[] moves) {
        Queue<Integer> pick = new LinkedList<>();
        moves = Arrays.stream(moves).map(x -> x - 1).toArray();

        for (int move : moves) {
            for (int j = 0; j < board.length; j++) {
                int item = board[j][move];
                if (item != 0) {
                    pick.offer(item);
                    board[j][move] = 0;

                    break;
                }
            }
        }

        int total = pick.size();

        Stack<Integer> result = new Stack<>();
        int item = 0;
        while (!pick.isEmpty()) {
            int temp = pick.poll();

            if (result.isEmpty()) {
                result.push(temp);

                continue;
            }

            item = result.pop();
            if (item != temp) {
                result.push(item);
                result.push(temp);
            }
        }

        return total - result.size();
    }
}