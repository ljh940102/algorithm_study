from collections import deque
def pick_puppet(board, move):
    for y in range(len(board)):
        if board[y][move] != 0:
            temp = board[y][move]
            board[y][move] = 0
            return temp
    return 0


def solution(board, moves):
    answer = 0
    container = []
    for move in moves:
        puppet = pick_puppet(board, move - 1)
        if puppet != 0:
            if container:
                if container[len(container) - 1] == puppet:
                    answer += 2
                    container.pop()
                else:
                    container.append(puppet)
            else:
                container.append(puppet)

    return answer

board = [[0,0,0,0,0],[0,0,1,0,3],[0,2,5,0,1],[4,2,4,4,2],[3,5,1,3,1]]
moves = [1,5,3,5,1,2,1,4]
print(solution(board,moves))