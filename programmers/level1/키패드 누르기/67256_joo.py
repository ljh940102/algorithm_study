"""
사실상 관건은 2 5 8 @을 어떤 손이 누르냐이다. 나머지는 dict로 해결
"""


def solution(numbers, hand):
    answer = []
    press = {1: "L", 4: "L", 7: "L", 3: "R", 6: "R", 9: "R"}
    loc = {1: (0, 0), 2: (0, 1), 3: (0, 2), 4: (1, 0), 5: (1, 1), 6: (1, 2), 7: (2, 0), 8: (2, 1), 9: (2, 2), 0: (3, 1), "*": (3, 0), "#": (3, 2)}

    current_L, current_R = '*', "#"

    for number in numbers:
        if press.get(number):
            if press[number] == "L":
                current_L = number
            else:
                current_R = number
            answer.append(press[number])
            continue

        dist_L = abs(loc[number][0] - loc[current_L][0]) + abs(loc[number][1] - loc[current_L][1])
        dist_R = abs(loc[number][0] - loc[current_R][0]) + abs(loc[number][1] - loc[current_R][1])

        if dist_L == dist_R:  # 거리가 같은 경우
            if hand == "right":
                answer.append("R")
                current_R = number
            else:
                answer.append("L")
                current_L = number
        elif dist_L < dist_R:
            answer.append("L")
            current_L = number
        else:
            answer.append("R")
            current_R = number

    return "".join(answer)


print(solution([1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5], "right"))
# print(solution([1, 2, 3, 4, 5, 6, 7, 8, 9, 0], "right"))
