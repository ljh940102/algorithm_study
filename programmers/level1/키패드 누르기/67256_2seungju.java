import java.util.*;

class Point {
    int x;
    int y;

    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

class Solution {
    public String solution(int[] numbers, String hand) {
        int[][] pad = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 6, 9},
                {-1, 0, -1}
        };

        Point left = new Point(1, 4);
        Point right = new Point(3, 4);

        Map<Integer, Point> map = new HashMap<>();
        for (int i = 1; i <= 9; i++) {
            int x = i % 3 != 0 ? i % 3 : 3;
            int y = i % 3 == 0 ? i / 3 : i / 3 + 1;

            map.put(i, new Point(x, y));
        }
        map.put(0, new Point(2, 4));

        StringBuilder answer = new StringBuilder();
        for (int number : numbers) {
            if (number == 1 || number == 4 || number == 7) {
                answer.append("L");
                left = map.get(number);

            } else if (number == 3 || number == 6 || number == 9) {
                answer.append("R");
                right = map.get(number);

            } else {
                Point target = map.get(number);

                if (getDistance(target, left) > getDistance(target, right)) {
                    answer.append("R");
                    right = target;

                } else if (getDistance(target, left) < getDistance(target, right)) {
                    answer.append("L");
                    left = target;

                } else {
                    if (hand.equals("right")) {
                        answer.append("R");
                        right = target;
                    } else {
                        answer.append("L");
                        left = target;
                    }                }
            }
        }

        return answer.toString();
    }

    private int getDistance(Point target, Point hand) {
        return Math.abs(target.x - hand.x) + Math.abs(target.y - hand.y);
    }
}