import java.util.*;

class StageInfo {
    int stage;
    double fail;

    public StageInfo(int stage, double fail) {
        this.stage = stage;
        this.fail = fail;
    }

    public int getStage() {
        return stage;
    }

    public double getFail() {
        return fail;
    }
}

class Solution {
    public int[] solution(int N, int[] stages) {
        int[] stageCount = new int[N+2];

        for (int stage : stages) {
            stageCount[stage]++;
        }

        int total = Arrays.stream(stageCount).sum();

        List<StageInfo> stageInfoList = new ArrayList<>();
        for (int i = 1; i < N + 1; i++) {
            int count = total;
            for (int j = 1; j < i; j++) {
                count -= stageCount[j];
            }

            if (count == 0) {
                stageInfoList.add(new StageInfo(i, 0));

            } else {
                stageInfoList.add(new StageInfo(i,(double) stageCount[i] / count));
            }
        }

        stageInfoList.sort(Comparator.comparing(StageInfo::getFail).reversed());

        return stageInfoList.stream().mapToInt(StageInfo::getStage).toArray();
    }
}