"""
stage별로 유저수
낮은 스테이지부터 실패율 구하고, 해당 스테이지에 있는 유저만큼 다음 스테이지 모수에서 뺴주기
단, N까지만 수행(N+1은 all clear로 실패율에 영향 X)
Ex.
[1 3 2 1 0 1]
1/8 3/7 2/4 1/2 0/1 -> 오름차순 이되, 같을 경우 stage 내림차순
"""

def solution(N, stages):
    fail = [0 for _ in range(N+2)]  # index 0은 사용 X

    for stage in stages:
        fail[stage] += 1

    cur_user = len(stages)
    fail_rates = []
    for stage in range(1, N+1):  # clear 제외
        if cur_user == 0:
            fail_rates.append((stage, 0))
            continue

        fail_rate = fail[stage]/cur_user
        fail_rates.append((stage, fail_rate))  # idx와 실패율을 tuple형태로 저장
        cur_user -= fail[stage]

    fail_rates = sorted(fail_rates, key=lambda x: (-x[1], x[0]))

    return [target[0] for target in fail_rates]