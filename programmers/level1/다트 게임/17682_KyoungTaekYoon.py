def solution(dartResult):
    answer = 0
    dartList = dartResult
    i = 0
    resultList = []
    bonusDict = {}
    bonusDict["S"] = 1
    bonusDict["D"] = 2
    bonusDict["T"] = 3
    while i < len(dartList):
        toAdd = 0
        if(dartList[i].isdigit() and dartList[i+1].isdigit()):
            score = dartList[i] + dartList[i+1]
            toAdd += 2
        else:
            score = dartList[i]
            toAdd += 1
        bonus = dartList[i+toAdd]
        toAdd += 1
        if i + toAdd < len(dartList):
            if dartList[i + toAdd].isdigit():
                i = i + toAdd
                tup = (score,bonus)
                resultList.append(tup)
            else:
                option = dartList[i+ toAdd ]
                tup = (score,bonus, option)
                resultList.append(tup)
                i = i + toAdd + 1
        else:
            i = i + toAdd
            tup = (score, bonus)
            resultList.append(tup)


    def calculateScore(resultList):
        scoreList = []
        scoreIndex = 0
        for resultGroup in resultList:
            temp = 0
            score, bonus, option = "", "", ""
            if len(resultGroup) == 2:
                score, bonus = resultGroup
            else:
                score, bonus, option = resultGroup
            temp = int(score) ** bonusDict[bonus]
            scoreList.append(temp)
            if option == "*":
                if len(scoreList) > 1:
                    scoreList[-2] *= 2
                    scoreList[-1] *= 2
                else:
                    scoreList[-1] *= 2
            elif option == "#":
                scoreList[-1] *= -1
        return sum(scoreList)
    answer = calculateScore(resultList)

    return answer
dartResult = "1S2D*3T"
answer = 37
#dartResult = "1D2S#10S"
answer = 9
dartResult = "1S*2T*3S"
solution(dartResult)
