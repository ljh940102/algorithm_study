import java.util.*;

class Dart {
    int score;
    String bonus;
    int option = 1;

    public int operateBonus() {
        int bonusInt = 0;
        if (bonus.equals("S")) {
            bonusInt = 1;
        } else if (bonus.equals("D")) {
            bonusInt = 2;
        } else {
            bonusInt = 3;
        }

        return (int) Math.pow(score, bonusInt);
    }
}

class Solution {
    public static int solution(String dartResult) {
        String[] strings = dartResult.split("");
        Stack<Dart> stack = new Stack<>();
        for (int i = 0; i < strings.length; i++) {
            Dart dart = new Dart();

            if (strings[i].equals("S") || strings[i].equals("D") || strings[i].equals("T")) {
                if (dart.score == 0 && i - 2 >= 0 && strings[i - 2].equals("1")) {
                    dart.score = Integer.parseInt(strings[i - 2] + strings[i - 1]);
                } else {
                    dart.score = Integer.parseInt(strings[i - 1]);
                }

                dart.bonus = strings[i];

                if (i + 1 != strings.length) {
                    if (strings[i + 1].equals("*")) {
                        if (!stack.isEmpty()) {
                            Dart preDart = stack.pop();
                            preDart.option *= 2;
                            stack.push(preDart);
                        }

                        dart.option *= 2;

                    } else if (strings[i + 1].equals("#")) {
                        dart.option = -1;
                    }

                    i += 1;
                }

                stack.push(dart);
            }
        }

        int answer = 0;

        while (!stack.isEmpty()) {
            Dart dart = stack.pop();

            int score = dart.operateBonus();
            if (dart.option != 0) {
                score = score * dart.option;
            }

            answer += score;
        }

        return answer;
    }
}