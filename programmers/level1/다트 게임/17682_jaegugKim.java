import java.util.*;

class Solution {
    public int solution(String dartResult) {
        int answer = 0;
        ArrayList<Integer> score = new ArrayList<>();
        String[] dart = new String[dartResult.length()];
        int flag = 0;
        for(int i=0; i<dartResult.length();i++){
            if(dartResult.charAt(i)=='S'){
                score.add(Integer.parseInt(dartResult.substring(flag, i)));
                flag = i+1;
            }
            if(dartResult.charAt(i)=='D'){
                score.add((int)Math.pow(Double.parseDouble(dartResult.substring(flag, i)),2));
                flag = i+1;
            }
            if(dartResult.charAt(i)=='T'){
                score.add((int)Math.pow(Double.parseDouble(dartResult.substring(flag, i)),3));
                flag = i+1;
            }
            if(dartResult.charAt(i)=='*'){
                if(score.size()==1){
                    score.set(0, score.get(0)*2);
                }else{
                    score.set(score.size()-2, score.get(score.size()-2)*2);
                    score.set(score.size()-1, score.get(score.size()-1)*2);
                }
                flag = i+1;
            }
            if(dartResult.charAt(i)=='#'){
                score.set(score.size()-1, score.get(score.size()-1)*(-1));
                flag = i+1;
            }
        }
        for(int i=0; i<score.size(); i++){
            answer+= score.get(i);
        }
        return answer;
    }
}
