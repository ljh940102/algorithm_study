class Solution {
	public String[] solution(int n, int[] arr1, int[] arr2) {
		String[] answer = new String[n];

        for (int i = 0; i < arr1.length; i++) {
            String encode = Integer.toBinaryString(arr1[i] | arr2[i]);
            encode = "0".repeat(Math.max(0, n - encode.length())) + encode;

            answer[i] = encode.replace('0', ' ').replace('1', '#');
        }

		return answer;
	}
}
