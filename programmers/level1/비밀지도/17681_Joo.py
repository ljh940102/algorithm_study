def solution(n, arr1, arr2):
    answer = []
    for map1_row, map2_row in zip(arr1, arr2):
        decrypted_row = format((map1_row | map2_row), 'b').zfill(n).replace("1", "#").replace("0", " ")
        answer.append(decrypted_row)
    return answer

print(solution(6, [46, 33, 33 ,22, 31, 50], [27 ,56, 19, 14, 14, 10]))
