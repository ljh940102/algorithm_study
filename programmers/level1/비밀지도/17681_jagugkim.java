class Solution {
    public String[] solution(int n, int[] arr1, int[] arr2) {
        String[] answer = {};
        answer = new String[n];
        boolean[][] secMap = new boolean[n][n];
        for(int i=0; i<n; i++){
            int num = arr1[i];
            for(int j=0; j<n; j++){
                if(num%2==1){
                    secMap[i][n-j-1]=true;
                }
                num=num/2;
            }
            num = arr2[i];
            for(int j=0; j<n; j++){
                if(num%2==1){
                    secMap[i][n-j-1]=true;
                }
                num=num/2;
            }
        }
        for(int i=0; i<n; i++){
            StringBuffer s = new StringBuffer("");
            for(int j=0; j<n; j++){
                if(secMap[i][j]){
                    s.append("#");   
                }else{
                    s.append(" ");
                }
            }
            answer[i] = s.toString();
        }
        return answer;
    }
}
