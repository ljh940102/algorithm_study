class Solution {
    public boolean solution(int[][] key, int[][] lock) {
        int m = key.length;
        int n = lock.length;

        for (int r = 0; r < 4; r++) {
            for (int dy = -(m - 1); dy < n; dy++) {
                for (int dx = - (m - 1); dx < n; dx++) {
                    if (findFunc(key, lock, dx, dy)) {
                        return true;
                    }
                }
            }

            key = rotate(key);
        }

        return false;
    }

    private boolean findFunc(int[][] key, int[][] lock, int dx, int dy) {
        int m = key.length;
        int n = lock.length;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int state = lock[i][j];

                if (!checkLength(i - dy, j - dx, m)) {
                    state += key[i - dy][j - dx];
                }

                if (state != 1) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean checkLength(int i, int j, int length) {
        return i < 0 || j < 0 || i >= length || j >= length;
    }

    private int[][] rotate(int[][] key) {
        int[][] rotateKey = new int[key.length][key.length];
        int m = key.length;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                rotateKey[i][j] = key[j][m - i - 1];
            }
        }

        return rotateKey;
    }
}