import copy
"""
필요한 기능
1. 열쇠 회전하는 기능
2. 상하좌우 이동하며 가능한 모든 경우의 수 만들기 (자물쇠 상하좌우로 M-1 만큼 padding
3. 열쇠가 열리는지 체크
    a. 열쇠의 1이 자물쇠의 모든 0을 채워야함
    b. 자물쇠의 모든 0이 채워져야 함
    c. 열쇠의 1과 자물쇠의 1이 만나면 안됨
"""

def rotate(target_key):
    """
    회전은 규칙이 있다. 아래는 회전 후의 배치가 어떻게 되는지 설명
    row -> N-1 부터 0까지 역순으로
    col -> row의 인덱스 그대로
    """
    rotated_key = copy.deepcopy(target_key)
    N = len(target_key)
    for row in range(len(target_key)):
        for col in range(len(target_key[0])):
            rotated_key[row][col] = target_key[N-col-1][row]
    return rotated_key


def padding_lock(target_lock, M):
    """lock보다 행과 열이 2M만큼 더 큰 배열 만들어서 중앙에 배치"""
    N, padding = len(target_lock), 2*M-1   # 2*M-1 만큼 추가 패딩 필요
    board = [[3 for _ in range(N+padding)] for _ in range(N+padding)]

    for row in range(N):
        for col in range(N):
            board[M+row][M+col] = target_lock[row][col]
    return board


def check(board, key, board_row, board_col, lock_size, lock_num):
    """
    unlocked 여부는 (자물쇠의 돌기의 수)와 (자물쇠가 홈이면서 열쇠가 돌기인 경우의 수)의 합이 자물쇠의 크기와 같은지를 여부로
    """
    is_unlocked = False
    M = len(key)
    mark = 0
    for row in range(M):
        for col in range(M):
            meet = key[row][col] + board[board_row + row][board_col + col]
            if meet == 2:  # 돌기끼리 만남
                return False
            elif meet == 1 and key[row][col] == 1:  # 열쇠의 돌기로 채워진 경우
                mark += 1

    if mark + lock_num == lock_size:
        is_unlocked = True

    return is_unlocked


def solution(key, lock):
    N, M = len(lock), len(key)
    lock_num = sum([sum(target) for target in lock])  # 자물쇠 돌기의 총합

    board = padding_lock(lock, M)  # IndexError를 위해 패딩된 board 선언
    L = len(board)  # padding된 board의 길이

    for _ in range(4):
        for board_row in range(L-M+1):  # 끝의 M만큼은 검사 X
            for board_col in range(L-M+1):
                if check(board, key, board_row, board_col, N*N, lock_num):  # 풀렸으면 바로 리턴
                    return True

        key = rotate(key)  # 회전

    return False
