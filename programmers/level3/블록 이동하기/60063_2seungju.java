import java.util.*;

class Robot {
    int x, y;
    int direction;
    int time;

    Robot(int x, int y, int direction, int time) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.time = time;
    }
}

class Solution {
    private int N;
    private int[][] Board;
    private final int[] dx = {0, 1, 0, -1};
    private final int[] dy = {1, 0, -1, 0};
    private final int[] rdx = {-1, 1, 1, -1};
    private final int[] rdy = {1, 1, -1, -1};

    public int solution(int[][] board) {
        Board = board;
        N = board.length;
        Queue<Robot> queue = new LinkedList<>();
        queue.add(new Robot(0, 0, 0, 0));

        // 초기 로봇 위치
        boolean[][][] visit = new boolean[N][N][4];
        visit[0][0][0] = true;

        return bfs(queue, visit);
    }

    private int bfs(Queue<Robot> queue, boolean[][][] visit) {
        // Queue에서 꺼낸 로봇의 x, y, 다른 x, y, 방향, 시간
        int x;
        int y;
        int ox;
        int oy;
        int direction;
        int time;

        // 로봇이 이동 후 가지게 되는 위치 및 방향 (nox : next other x)
        int nx;
        int ny;
        int nox;
        int noy;
        int nDirection;

        // 회전할 때 판단해야 할 벽의 위치
        int rx, ry;

        while (!queue.isEmpty()) {
            Robot robot = queue.poll();

            x = robot.x;
            y = robot.y;
            direction = robot.direction;
            time = robot.time;
            ox = x + dx[direction];
            oy = y + dy[direction];

            // 도착하면 리턴
            if (isFinish(x, y) || isFinish(ox, oy)) {
                return time;
            }

            // 상하좌우 이동
            for (int i = 0; i < 4; i++) {
                nx = x + dx[i];
                ny = y + dy[i];
                nox = ox + dx[i];
                noy = oy + dy[i];
                if (isInvalid(nx, ny) || isInvalid(nox, noy) || // 벽 밖으로 나갔는지 체크
                        Board[nx][ny] == 1 || Board[nox][noy] == 1 || // 벽인지 체크
                        visit[nx][ny][direction]) {                    // 이미 방문한 곳인지 체크
                    continue;
                }

                visit[nx][ny][direction] = true;
                queue.add(new Robot(nx, ny, direction, time + 1));
            }

            // x, y를 기준으로 90도 회전
            for (int i = 1; i < 4; i += 2) {
                nDirection = (direction + i) % 4;
                nox = x + dx[nDirection];
                noy = y + dy[nDirection];

                int temp = (i == 1) ? nDirection : direction;
                rx = x + rdx[temp];
                ry = y + rdy[temp];

                if (isInvalid(nox, noy) || isInvalid(rx, ry) ||
                        Board[nox][noy] == 1 || Board[rx][ry] == 1 ||
                        visit[x][y][nDirection]){
                    continue;
                }

                visit[x][y][nDirection] = true;
                queue.add(new Robot(x, y, nDirection, time + 1));
            }

            // 방향 반대 처리
            direction = (direction + 2) % 4;

            // ox, oy를 기준으로 90도 회전
            for (int i = 1; i < 4; i += 2) {
                nDirection = (direction + i) % 4;
                nx = ox + dx[nDirection];
                ny = oy + dy[nDirection];

                int temp = (i == 1) ? nDirection : direction;
                rx = ox + rdx[temp];
                ry = oy + rdy[temp];

                nDirection = (nDirection + 2) % 4;

                if (isInvalid(nx, ny) || isInvalid(rx, ry) ||
                        Board[nx][ny] == 1 || Board[rx][ry] == 1 ||
                        visit[nx][ny][nDirection]){
                    continue;
                }

                visit[nx][ny][nDirection] = true;
                queue.add(new Robot(nx, ny, nDirection, time + 1));
            }
        }

        return -1;
    }

    // 맵 밖으로 나갔는지 체크
    private boolean isInvalid(int x, int y) {
        return x < 0 || y < 0 || x >= N || y >= N;
    }

    private boolean isFinish(int x, int y) {
        return x == N - 1 && y == N - 1;
    }
}