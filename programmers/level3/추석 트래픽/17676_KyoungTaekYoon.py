from datetime import datetime, timedelta

def solution(lines):
    def count_log_on_1sec(t):
        cnt = 0
        for time in times:
            if time[1] >= t and time[0] < t + timedelta(milliseconds=1000):
                cnt += 1
        return cnt

    times = []

    for line in lines:
        time, day, delay_sec = line.split(' ')
        end = datetime.strptime(time+' '+day,'%Y-%m-%d %H:%M:%S.%f') #시간을 datetime형태로 바꿈
        delay_millisec = int(float(delay_sec[:-1])*1000) #걸리는 시간을 밀리세컨드형태로 변환
        start = end - timedelta(milliseconds=delay_millisec) + timedelta(milliseconds=1)
        times.append((start, end))

    max_cnt = 0

    for time in times:
        max_cnt = max(max_cnt, count_log_on_1sec(time[0]), count_log_on_1sec(time[1])) #현재 최대, 시작과 1초 사이, 끝나고 1초 사이에

    return max_cnt
lines= [
"2016-09-15 20:59:57.421 0.351s",
"2016-09-15 20:59:58.233 1.181s",
"2016-09-15 20:59:58.299 0.8s",
"2016-09-15 20:59:58.688 1.041s",
"2016-09-15 20:59:59.591 1.412s",
"2016-09-15 21:00:00.464 1.466s",
"2016-09-15 21:00:00.741 1.581s",
"2016-09-15 21:00:00.748 2.31s",
"2016-09-15 21:00:00.966 0.381s",
"2016-09-15 21:00:02.066 2.62s"
]
solution(lines)