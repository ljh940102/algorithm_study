import java.util.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.time.ZoneId;

class Traffic {
    private long start;
    private long end;

    Traffic(long start, long end) {
        this.start = start;
        this.end = end;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }
}

class Solution {
    public static int solution(String[] lines) {
        List<Traffic> trafficList = new ArrayList<>();
        for (String line : lines) {
            String s = line.substring(0, 23);
            String t = line.substring(24, line.length() - 1);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

            LocalDateTime localDateTime = LocalDateTime.parse(s, formatter);
            long sEpoch = localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            long tEpoch = (long) (Double.parseDouble(t) * 1000) - 1;

            trafficList.add(new Traffic(sEpoch - tEpoch, sEpoch));
        }
        int max = 1;
        for (int i = 0; i < trafficList.size() - 1; i++) {
            int count = 1;
            long section = trafficList.get(i).getEnd() + 1000;
            for (int j = i + 1; j < trafficList.size(); j++) {
                if (trafficList.get(j).getStart() < section) {
                    count++;
                }
            }

            max = Math.max(max, count);
        }

        return max;
    }
}