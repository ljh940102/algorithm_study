"""
dict에 시간을 키로하는 값에 +1 해서 해당 시간의 처리되고 있는 요청을 표시해주자.
최대 200만개
시간만 잘 계산해서 넣어주기
"""
import datetime


def is_inside(target, start, end):
    if target[0] <= end and target[1] >= start:  # between은 1초보단 긴 막대를 못 잡는다.
        return True

    return False


def get_throughput(compare_start, tasks):
    num_task = 0
    compare_end = compare_start + 999

    for check in tasks:
        if is_inside(check, compare_start, compare_end):
            num_task += 1
    return num_task


def solution(lines):
    tasks = list()
    for line in lines:
        date, time, processing = line.split()
        end = datetime.datetime.strptime(date + " " + time, '%Y-%m-%d %H:%M:%S.%f')
        end_ms = end.timestamp() * 1000
        length = float(processing.replace("s", "")) * 1000
        start_ms = end_ms - length + 1  # 처음과 끝시간 포함
        print(start_ms)
        print(end_ms)

        tasks.append((start_ms, end_ms))

    max_val = 1
    for start, end in tasks:
        max_val = max(max_val, get_throughput(start, tasks), get_throughput(end, tasks))

    return max_val

# print(solution(["2016-09-15 20:59:57.421 0.351s","2016-09-15 20:59:58.233 1.181s","2016-09-15 20:59:58.299 0.8s","2016-09-15 20:59:58.688 1.041s","2016-09-15 20:59:59.591 1.412s","2016-09-15 21:00:00.464 1.466s","2016-09-15 21:00:00.741 1.581s","2016-09-15 21:00:00.748 2.31s","2016-09-15 21:00:00.966 0.381s","2016-09-15 21:00:02.066 2.62s"]))

# print(solution(["2016-09-15 01:00:04.001 2.0s", "2016-09-15 01:00:07.000 2s"]))
print(solution(["2016-09-15 00:00:00.000 0.001s"]))
