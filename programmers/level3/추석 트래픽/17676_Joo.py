"""
dict에 시간을 키로하는 값에 +1 해서 해당 시간의 처리되고 있는 요청을 표시해주자.
최대 200만개
시간만 잘 계산해서 넣어주기
"""
import datetime

    if target[0] <= end and target[1] >= start:  # between은 1초보단 긴 막대를 못 잡는다.
        return True

    return False

def get_throughput(compare_start, tasks):
    num_task = 0
    compare_end = compare_start + 999
    for check in tasks:
        if is_inside(check[0], compare_start, compare_end) or is_inside(check[1], compare_start, compare_end):
            num_task += 1

    return num_task


def solution(lines):
    tasks = list()
    for num, line in enumerate(lines):
        date, time, processing = line.split()
        end_ms = datetime.datetime.strptime(date + " " + time, '%Y-%m-%d %H:%M:%S.%f').timestamp() * 1000
        length = float(processing.replace("s", "")) * 1000
        start_ms = end_ms - length + 1  # 처음과 끝시간 포함
        tasks.append((start_ms, end_ms))

    max_val = 0
    for start, end in tasks:
        max_val = max(max_val, get_throughput(start, tasks))
        max_val = max(max_val, get_throughput(end, tasks))

    return max_val
