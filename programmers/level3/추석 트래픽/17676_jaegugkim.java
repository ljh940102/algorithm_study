import java.util.*;
import java.math.*;

class Solution {
    public int solution(String[] lines) {
        int answer = 0;
        int[] startTimes = new int[lines.length];
        int[] endTimes = new int[lines.length];
        BigDecimal bd1000 = new BigDecimal("1000");

        for(int i = 0 ; i < lines.length ; ++i) {//각 시작,종료시각 구하기
            String[] log = lines[i].split(" ");
            String[] date = log[1].split(":");
            endTimes[i] = Integer.parseInt(date[0]) * 60 * 60 * 1000 
                        + Integer.parseInt(date[1]) * 60 * 1000
                        + new BigDecimal(date[2]).multiply(bd1000).intValue();//float 형변환시 소수점 오류 발생
            int time = new BigDecimal(log[2].substring(0, log[2].length() - 1)).multiply(bd1000).intValue();
            startTimes[i] = endTimes[i] - time + 1;
        }
        
        for(int i = 0 ; i < lines.length ; ++i) {
			int count = 0;
            for(int j = 0 ; j < lines.length ; ++j) {//종료시간을 기준으로 구간 확인
                if(startTimes[j] >= endTimes[i] && startTimes[j] < endTimes[i]+1000
                || endTimes[j] >= endTimes[i] && endTimes[j] < endTimes[i]+1000
                || startTimes[j] <= endTimes[i] && endTimes[j] >= endTimes[i]+1000) {
                    count++;
                } 
            }
            if(count>answer){
                answer = count;
            }
        }
        return answer;
    }
}
