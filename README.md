## 알고리즘 문제풀이 스터디

#### 11월 4주차 문제
+ 문자열압축 - https://programmers.co.kr/learn/courses/30/lessons/60057
+ 괄호변환 - https://programmers.co.kr/learn/courses/30/lessons/60058
+ 튜플 - https://programmers.co.kr/learn/courses/30/lessons/64065

#### 구성원
+ 이주현
	* joohdev@gmail.com
+ 윤경택
	* yonf99679@gmail.com
+ 김재국
	* gldydhk@gmail.com
+ 이상현
	* sahyle9417@gmail.com
+ 이승주
    * a01065253918@gmail.com

#### Directory 구성
+ 알고리즘명 > 문제명

#### File Name 구성
+ 문제명_이름_코멘트(선택).extension<br>
	ex) <br>
	_strStr_Joo_fail.py_<br>
	_strStr_Joo.java_

#### 벌금 Rule
+ 문제 안푸는 경우 문제 당 벌금 3천원
	* 문제를 풀지 못한 경우, Best 답안 파악 후 설명할 경우 벌금면제
+ Study 미참여시 벌금 3천원
+ 벌금계좌: 카카오 3333105889586
