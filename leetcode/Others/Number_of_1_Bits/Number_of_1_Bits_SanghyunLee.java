class Solution {

	int hammingWeight(int n) {
		int count = 0;
		// 좌측 끝(31번째 bit, MSB)부터 우측 끝(0번째 bit, LSB)까지 bit 확인
		for (int i = 31; i >= 0; i--) {
			if (getBit(n, i) == true) {
				count++;
			}
		}
		return count;
	}

	// 우측 끝에서부터 i번째 bit가 1인지 여부 반환
	boolean getBit(int n, int i) {
		// 1 << i : 1을 좌측으로 i bit만큼 shift
		// & : Bitwise And
		return (n & (1 << i)) != 0;
	}
}
