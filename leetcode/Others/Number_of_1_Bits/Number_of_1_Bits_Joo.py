class Solution:
    def hammingWeight(self, n: int) -> int:
        rtn = 0
        for target in format(n,'b'):
            rtn += int(target)
        return rtn