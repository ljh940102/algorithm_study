public class Solution {
    // you need to treat n as an unsigned value
    public int hammingWeight(int n) {
        int count = 0;
        for (int i = 0; i < 32; i++) {
            //1이랑 and 연산으로 1이 나오면 count 증가
            if ((n & 1) == 1) {
                count++;
            }
            //n을 1만큼 우측으로 shift
            n = n>>>1;
        }
        return count;
    }
}