class Solution {

    // stack이 더 좋음
    public static int evalRPN(String[] tokens) {
        List<String> tokenList = new ArrayList<>(Arrays.asList(tokens));

        int index = 0;
        while (tokenList.size() >= 3) {
            if (isOperator(tokenList.get(index))) {
                int a = Integer.parseInt(tokenList.get(index - 2));
                int b = Integer.parseInt(tokenList.get(index - 1));
                int result = calculator(a, b, tokenList.get(index));

                tokenList.remove(index);
                tokenList.remove(index - 1);
                tokenList.remove(index - 2);
                tokenList.add(index - 2, String.valueOf(result));
                index -= 3;
            }
            index++;
        }

        return Integer.parseInt(tokenList.get(0));
    }

    private static boolean isOperator(String str) {
        return str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/");
    }

    private static int calculator(int a, int b, String operator) {
        int result;
        switch (operator) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            default:
                result = a / b;
        }

        return result;
    }
}