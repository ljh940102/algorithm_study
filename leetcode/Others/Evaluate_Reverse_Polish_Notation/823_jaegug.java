class Solution {
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for(int i = 0; i < tokens.length; i++){
            if(tokens[i].equals("+")){
                int sum = stack.pop() + stack.pop();
                stack.push(sum);
            }else if(tokens[i].equals("-")){
                int token1 = stack.pop();
                int token2 = stack.pop();
                stack.push(token2 - token1);
            }else if(tokens[i].equals("*")){
                int mul = stack.pop() * stack.pop();
                stack.push(mul);
            }else if(tokens[i].equals("/")){
                int token1 = stack.pop();
                int token2 = stack.pop();
                stack.push(token2/token1);
            }else{
                stack.push(Integer.parseInt(tokens[i]));
            }
        }
        
        return stack.pop();
    }
}
