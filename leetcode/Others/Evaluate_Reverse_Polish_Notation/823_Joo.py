class Solution:
    def add(self, a, b):
        return a + b

    def sub(self, a, b):
        return a - b

    def multiply(self, a, b):
        return a * b

    def divide(self, a, b):
        return int(a / b)

    def evalRPN(self, tokens: List[str]) -> int:
        operator = {
            "+": self.add,
            "-": self.sub,
            "*": self.multiply,
            "/": self.divide
        }

        stack = []
        for token in tokens:
            if token in operator:
                right = stack.pop()
                left = stack.pop()

                result = operator[token](left, right)

                stack.append(result)
            else:
                stack.append(int(token))

        return stack.pop()

