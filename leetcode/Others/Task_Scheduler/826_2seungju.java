class Solution {
    public int leastInterval(char[] tasks, int n) {
        int[] counts = new int[26];
        for (char task : tasks) {
            counts[task - 'A']++;
        }

        int max = 0;
        for (int count : counts) {
            max = Math.max(max, count);
        }
        max--;

        int spaces = max * (n + 1);
        for (int count : counts) {
            spaces -= Math.min(count, max);
        }


        return tasks.length + Math.max(0, spaces);
    }
}