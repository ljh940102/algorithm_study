from collections import Counter


class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:
        if not n:
            return len(tasks)

        counter = Counter(tasks)

        most_task, most_count = counter.most_common(n=1)[0]
        min_space = most_count + (n * (most_count - 1))  # 최소한으로 필요한 공간
        # 최다빈도 task수 + 사이에 들어갈 cooling 공간

        for task, count in counter.items():
            if task == most_task:
                continue

            if count == most_count:  # 최대값과 count가 같은 대상이 있으면, 최소공간이 하나 더 필요
                min_space += 1

        return max(min_space, len(tasks))