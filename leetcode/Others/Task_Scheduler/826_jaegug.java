class Solution {
    public int leastInterval(char[] tasks, int n) {
        HashMap<Character, Integer> countMap = new HashMap<>();
        
        
        for(int i = 0; i < tasks.length; i++){
            countMap.put(tasks[i], countMap.getOrDefault(tasks[i], 0)+1);
        }
        
        int maxCounts = 0;
        for(Character key : countMap.keySet()){
            maxCounts = Math.max(maxCounts, countMap.get(key));
        }
        
        // 가장 count가 많은 task만 존재한다고 생각하고 period를 idle로 채움
        int idleSum = (maxCounts - 1) * n;
        
        for(Character key : countMap.keySet()){
            // idle 자리에 넣을 수 있는 task 수만큼 idle을 대체(감소)시킴
            // 한 period 사이의 간격이 maxCounts - 1 이므로,
            // maxCounts - 1 값을 초과하는 task를 idle에 넣을 수 없음
            idleSum -= Math.min(maxCounts - 1, countMap.get(key)); 
        }
        // 16번 line에서 사용한 maxCounts key도 22번 line에서 빼버렸으니 다시 더해주자
        idleSum += maxCounts - 1; 
        
        return tasks.length + Math.max(0, idleSum);
    }
}
