    def majorityElement(self, nums: List[int]) -> int:
        num_dict = defaultdict(int)
        cur_max,cur_max_num = 0, 0
        for i, num in enumerate(nums):
            num_dict[num] += 1
            if num_dict[num] > cur_max:
                cur_max_num = num
                cur_max = num_dict[num]
        return cur_max_num