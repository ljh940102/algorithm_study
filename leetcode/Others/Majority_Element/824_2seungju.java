class Solution {
    public int majorityElement(int[] nums) {
        int maxKey = Integer.MIN_VALUE;
        int maxValue = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.merge(num, 1, Integer::sum);
            int value = map.get(num);
            if (maxValue < value) {
                maxKey = num;
                maxValue = value;
            }
        }

        return maxKey;
    }
}