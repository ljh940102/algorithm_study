class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> answer = new ArrayList<List<Integer>>();
        int[][] tmp = new int[numRows][numRows];
        for(int i=0; i<numRows; i++){
            List<Integer> row = new ArrayList<>();
            for(int j=0; j<=i; j++){
                if(j==0 || j==i){
                    tmp[i][j] = 1;
                    row.add(1);
                }else{
                    tmp[i][j] = tmp[i-1][j-1] + tmp[i-1][j];
                    row.add(tmp[i][j]);
                }
            }
            answer.add(row);
        }
        return answer;
    }
}