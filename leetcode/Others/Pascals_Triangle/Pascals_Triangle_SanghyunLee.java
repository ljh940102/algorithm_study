import java.util.*;

class Solution {
	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> all_lists = new LinkedList<>();
		if (numRows == 0) {
			return all_lists;
		}

		all_lists.add(Arrays.asList(1));
		if (numRows == 1) {
			return all_lists;
		}

		all_lists.add(Arrays.asList(1, 1));
		if (numRows == 2) {
			return all_lists;
		}

		List<Integer> prev_row = all_lists.get(1);
		for (int rowIdx = 3; rowIdx <= numRows; rowIdx++) {
			List<Integer> curr_row = new ArrayList<>();

			curr_row.add(1);
			for (int colIdx = 1; colIdx < rowIdx - 1; colIdx++) {
				curr_row.add(prev_row.get(colIdx - 1) + prev_row.get(colIdx));
			}
			curr_row.add(1);

			all_lists.add(curr_row);
			prev_row = curr_row;
		}

		return all_lists;
	}

}
