class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        if numRows == 0: 
            return []
        elif numRows == 1: 
            return [[1]]
        pasTri = [[1]] # start
        for i in range(1, numRows):
            row = [1] #첫번째 숫자
            for j in range(1,i):
                row.append(pasTri[i-1][j-1] + pasTri[i-1][j]) #위에서 두 개를 더함
            row.append(1)#마지막 숫자
            pasTri.append(row)
        return pasTri