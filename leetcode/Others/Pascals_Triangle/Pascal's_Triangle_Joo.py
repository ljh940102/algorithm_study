class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        if numRows == 0:
            return []
        elif numRows == 1:
            return [[1]]
        elif numRows == 2:
            return [[1],[1,1]]

        rtn = [[1],[1,1]]
        for row in range(2, numRows):
            append_list = [1]
            for idx in range(1, row):
                append_list.append(rtn[row-1][idx-1] + rtn[row-1][idx])
            append_list.append(1)
            rtn.append(append_list)

        return rtn
