class Solution {

	// 해당 범위에 해당하는 숫자들의 합계에서 실제 합계을 빼면 missing number
	public int missingNumber(int[] nums) {
		int n = nums.length;
		int sum = (n * (n + 1)) / 2;
		int arraysum = 0;
		for (int i = 0; i < n; i++) {
			arraysum = arraysum + nums[i];
		}
		return (sum - arraysum);
	}

	/*
	// 위의 정답보다 느리므로 참고만 할 것
	// 정렬 후 인덱스와 값이 일치하지 않는 숫자가 missing number
	public int missingNumber(int[] nums) {
		Arrays.sort(nums);
		for (int i = 0; i < nums.length; i++) {
			if (i != nums[i]) {
				return i;
			}
		}
		// length - 1까지 모두 일치하는 경우에는 맨 마지막 숫자가 없는 것임
		return nums.length;
	}
	*/
}
