"""
공간은 1이고 시간이 n인 방법?
-> 저장은 변수하나만 하고, n순회를 여러번 하는 방식?!
-> 0부턴 n까지 더한값에 nums의 총합을 이용하면 될듯
"""

class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        total = sum(range(len(nums)+1))
        return total - sum(nums)