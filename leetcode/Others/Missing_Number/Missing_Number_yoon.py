class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        record = [0] * (len(nums) + 1)
        for i in range(len(nums)):
            record[nums[i]] = 1
        for i in range(len(nums) + 1):
            if record[i] == 0:
                return i