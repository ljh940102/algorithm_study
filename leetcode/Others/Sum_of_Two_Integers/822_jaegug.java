class Solution {
    public int getSum(int a, int b) {
        
        // 이진법 덧셈
        while (b != 0) { 
            int sum = a ^ b;
            int carry = (a & b) << 1; // 둘다 1인 경우 자리 올림
            a = sum;
            b = carry;
        }
        
        return a;
        
        /*
        int target = a;
        if(b > 0) {
            for(int i = 0; i < b; i++){
                target++;
            }    
        }else{
            for(int i = 0; i > b; i--){
                target--;
            }
        }
        
        return target;
        */
    }
}
