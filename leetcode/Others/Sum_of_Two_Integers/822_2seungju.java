
class Solution {
    public int getSum(int a, int b) {
        int c;
        while(b != 0){
            // AND 연산 b는 반드시 1이기 때문에 TRUE라면 시프트
            c = (a & b) << 1;
            // XOR 연산
            a = a ^ b;
            b = c;
        }

        return a;
    }
}