class Solution:
    def reverseBits(self, n: int) -> int:
        n = format(n, 'b').zfill(32)
        reversed_n = n[::-1]
        return int(reversed_n, 2)