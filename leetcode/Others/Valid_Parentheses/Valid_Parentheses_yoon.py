class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        for char in s:
            if char == "(" or char == "{" or char == "[": # 왼쪽 스택 확인
                stack.append(char) 
            elif len(stack) <= 0:
                return False
            elif char == ")" and stack.pop() != "(": #맞는지 확인
                return False
            elif char == "]" and stack.pop() != "[":
                return False
            elif char == "}" and stack.pop() != "{":
                return False
        if len(stack) == 0:
            return True
        return False