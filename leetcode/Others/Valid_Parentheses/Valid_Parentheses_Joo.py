class Solution:
    def isValid(self, s: str) -> bool:
        stack = list()
        pair = dict()
        pair[')'] = '('
        pair['}'] = '{'
        pair[']'] = '['

        for target in s:
            if self.isOpen(target):  # open paren인 경우 처리
                stack.append(target)

            else:  # close paren인 경우 처리
                if not stack:
                    return False

                if pair[target] == stack[-1]:
                    stack.pop()
                else:
                    return False

                """
                if target == ')':
                    if stack[-1] == '(':
                        stack.pop()
                    else:
                        return False
                elif target == '}':
                    if stack[-1] == '{':
                        stack.pop()
                    else:
                        return False
                elif target == ']':
                    if stack[-1] == '[':
                        stack.pop()
                    else:
                        return False
                """
        if stack:
            return False
        else:
            return True

    def isOpen(self,target):
        if target == '(' or target == '{' or target == '[':
            return True
        return False
