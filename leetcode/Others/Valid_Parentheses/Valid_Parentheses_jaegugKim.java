class Solution {
    public boolean isValid(String s) {
        char[] stack = new char[s.length()+1];
        int head = 0;
        for(int i = 0; i<s.length(); i++){
            if(s.charAt(i) == '{' || s.charAt(i) == '(' || s.charAt(i) == '['){
                head++;    
                stack[head] = s.charAt(i);
            }
            else if(stack[head] == '{' && s.charAt(i) == '}' || 
                stack[head] == '(' && s.charAt(i) == ')' || 
                stack[head] == '[' && s.charAt(i) == ']'){
                head--;
            }else{
                return false;   
            }
        }
        if(head>0){
            return false;
        }
        return true;
    }
}