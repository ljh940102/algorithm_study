class Solution:
    def hammingDistance(self, x: int, y: int) -> int:
        x, y = format(x, 'b'), format(y, 'b')

        if len(x) > len(y):
            y = y.zfill(len(x))
        else:
            x = x.zfill(len(y))

        rtn = 0
        for idx in range(len(x)):
            if x[idx] != y[idx]:
                rtn += 1

        return rtn
