import random


class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.num_list = list()
        self.check_idx_dict = dict()
        self.length = 0

    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if val in self.check_idx_dict:
            return False

        self.num_list.append(val)
        self.check_idx_dict[val] = self.length
        self.length += 1

        return True

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val not in self.check_idx_dict:
            return False

        idx = self.check_idx_dict[val]  # list안 val의 인덱스

        # swap
        self.num_list[-1], self.num_list[idx] = self.num_list[idx], self.num_list[-1]
        self.length -= 1

        # idx dict 갱신
        self.check_idx_dict[self.num_list[idx]] = idx

        self.num_list.pop()
        del (self.check_idx_dict[val])

        return True

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        return self.num_list[random.randrange(0, self.length)]