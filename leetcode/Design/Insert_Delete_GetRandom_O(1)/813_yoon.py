class RandomizedSet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.randomized_dict = {}
    

    def insert(self, val: int) -> bool:
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        """
        if val in self.randomized_dict:
            return False
        self.randomized_dict[val] = val
        return True

    def remove(self, val: int) -> bool:
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        """
        if val in self.randomized_dict:
            del self.randomized_dict[val]
            return True
        return False

    def getRandom(self) -> int:
        """
        Get a random element from the set.
        """
        randomNum = random.choice(list(self.randomized_dict.keys()))
        return randomNum
