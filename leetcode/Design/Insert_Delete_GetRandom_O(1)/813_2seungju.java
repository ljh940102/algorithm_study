class RandomizedSet {
    private final Set<Integer> set;
    private final Random random;

    public RandomizedSet() {
        this.set = new HashSet<>();
        this.random = new Random();
    }

    public boolean insert(int val) {
        if (set.contains(val)) {
            return false;
        } else {
            set.add(val);
            return true;
        }
    }

    public boolean remove(int val) {
        if (set.contains(val)) {
            set.remove(val);
            return true;
        } else {
            return false;
        }
    }

    public int getRandom() {
        int result = 0;

        int randomNumber = random.nextInt(set.size());
        for (int n : set) {
            if (randomNumber == 0) {
                return n;
            }
            randomNumber--;
        }

        return result;
    }
}

public class RandomizedSet {
    private final Random random;
    private final List<Integer> list;
    private final Map<Integer, Integer> map;


    public RandomizedSet() {
        this.random = new Random();
        this.list = new ArrayList<>();
        this.map = new HashMap<>();
    }

    public boolean insert(int val) {
        if (map.containsKey(val)) {
            return false;
        } else {
            list.add(val);
            map.put(val, list.size() - 1);
            return true;
        }
    }

    public boolean remove(int val) {
        if (map.containsKey(val)) {
            System.out.println(map);
            System.out.println(list);
            list.remove(map.get(val));
            map.remove(val);
            System.out.println(list);

            return true;
        } else {
            return false;
        }
    }

    public int getRandom() {
        return list.get(random.nextInt(list.size()));
    }
}
