class Station {
    String start;
    String end;
    int time;

    @Override
    public String toString() {
        return start + ", " + end + ", " + time;
    }
}

class UndergroundSystem {
    Map<Integer, Station> stationMap;
    Map<String, Integer> totalMap;
    Map<String, Integer> countMap;

    public UndergroundSystem() {
        stationMap = new HashMap<>();
        totalMap = new HashMap<>();
        countMap = new HashMap<>();

    }

    public void checkIn(int id, String stationName, int t) {
        Station station = new Station();
        station.start = stationName;
        station.time = t;

        stationMap.put(id, station);
    }

    public void checkOut(int id, String stationName, int t) {
        Station user = stationMap.get(id);
        user.end = stationName;
        user.time = t - user.time;

        String key = user.start + "," + user.end;
        totalMap.merge(key, user.time, Integer::sum);
        countMap.merge(key, 1, Integer::sum);
    }

    public double getAverageTime(String startStation, String endStation) {
        String key = startStation + "," + endStation;
        return (double) totalMap.get(key) / countMap.get(key);
    }
}

/**
 * Your UndergroundSystem object will be instantiated and called as such:
 * UndergroundSystem obj = new UndergroundSystem();
 * obj.checkIn(id,stationName,t);
 * obj.checkOut(id,stationName,t);
 * double param_3 = obj.getAverageTime(startStation,endStation);
 */