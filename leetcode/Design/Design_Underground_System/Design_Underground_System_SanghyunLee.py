from collections import defaultdict


class UndergroundSystem:
	def __init__(self):
		self.start_info = defaultdict()
		self.routes = defaultdict(list)

	def checkIn(self, id: int, stationName: str, t: int) -> None:
		self.start_info[id] = (stationName, t)

	def checkOut(self, id: int, stationName: str, t: int) -> None:
		start_station, start_time = self.start_info[id]
		del self.start_info[id]
		route = (start_station, stationName)
		self.routes[route].append(t - start_time)

	def getAverageTime(self, startStation: str, endStation: str) -> float:
		total = sum(self.routes[(startStation, endStation)])
		count = len(self.routes[(startStation, endStation)])
		return total / count


# Your UndergroundSystem object will be instantiated and called as such:
obj = UndergroundSystem()

opers = ["checkIn","checkIn","checkIn","checkOut","checkOut","checkOut","getAverageTime","getAverageTime","checkIn","getAverageTime","checkOut","getAverageTime"]
params = [[45,"Leyton",3],[32,"Paradise",8],[27,"Leyton",10],[45,"Waterloo",15],[27,"Waterloo",20],[32,"Cambridge",22],["Paradise","Cambridge"],["Leyton","Waterloo"],[10,"Leyton",24],["Leyton","Waterloo"],[10,"Waterloo",38],["Leyton","Waterloo"]]
for oper, param in zip(opers, params):
	print(f"{oper}, {param}")
	if oper == "checkIn":
		id, stationName, t = param
		obj.checkIn(id,stationName,t)
	elif oper == "checkOut":
		id, stationName, t = param
		obj.checkOut(id,stationName,t)
	else:
		startStation, endStation = param
		param_3 = obj.getAverageTime(startStation,endStation)
		print(param_3)
