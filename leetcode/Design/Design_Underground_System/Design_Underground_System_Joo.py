from collections import defaultdict


class UndergroundSystem:
    def __init__(self):
        self.spent_time_table = defaultdict(list)  # 결과적으로 걸린 시간 저장 -> {(출발역,도착역): 걸린시간}
        self.check_in_table = dict()  # 체크인 대상 저장

    def checkIn(self, id: int, stationName: str, t: int) -> None:
        self.check_in_table[id] = (stationName, t)

    def checkOut(self, id: int, stationName: str, t: int) -> None:
        start_station, start_time = self.check_in_table[id]

        del (self.check_in_table[id])  # check in 삭제 처리

        spent_time = t - start_time  # 소요시간

        self.spent_time_table[(start_station, stationName)].append(spent_time)

    def getAverageTime(self, startStation: str, endStation: str) -> float:
        target_list = self.spent_time_table[(startStation, endStation)]

        return sum(target_list) / len(target_list)