from collections import defaultdict
class UndergroundSystem:

    def __init__(self):
        self.check_in_dict = {}
        self.avg_time_dict = defaultdict(list)

    def checkIn(self, id: int, stationName: str, t: int) -> None:
        if id not in self.check_in_dict:
            self.check_in_dict[id] = (stationName, t)

    def checkOut(self, id: int, stationName: str, t: int) -> None:
        if id in self.check_in_dict:
            check_in_station, check_in_time = self.check_in_dict[id]
            self.avg_time_dict[(check_in_station, stationName)].append(t - check_in_time)
            del self.check_in_dict[id]

    def getAverageTime(self, startStation: str, endStation: str) -> float:
        return sum(self.avg_time_dict[(startStation, endStation)]) / len(self.avg_time_dict[(startStation, endStation)])


# Your UndergroundSystem object will be instantiated and called as such:
# obj = UndergroundSystem()
# obj.checkIn(id,stationName,t)
# obj.checkOut(id,stationName,t)
# param_3 = obj.getAverageTime(startStation,endStation)
