import sys

class MinStack:

    def __init__(self):
        self.stack = []
        self.min = None

    def push(self, x: int) -> None:
        self.stack.append(x)
        if self.min is None:  # self.min을 조건을 주면 0일때도 포함되어 버림..
            self.min = x
        else:
            self.min = min(self.min, x)

    def pop(self) -> None:
        if self.min == self.stack[-1]:
            if len(self.stack) > 1:
                self.min = min(self.stack[:-1])
            else:
                self.min = None
        self.stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return self.min

# Your MinStack object will be instantiated and called as such:
# obj = MinStack()
# obj.push(x)
# obj.pop()
# param_3 = obj.top()
# param_4 = obj.getMin()
