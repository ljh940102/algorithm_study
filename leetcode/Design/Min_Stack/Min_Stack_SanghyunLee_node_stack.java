import java.util.*;

class MinStack_NodeStack {

    class Node {
        int value;
        int min;
        Node(int value, int min) {
            this.value = value;
            this.min = min;
        }
    }

    LinkedList<Node> stack;
    
    /** initialize your data structure here. */
    public MinStack_NodeStack() {
        this.stack = new LinkedList<>();
    }
    
    public void push(int x) {
        int min = stack.isEmpty() ? x : stack.getFirst().min;
        stack.addFirst(new Node(x, Math.min(x, min)));
    }
    
    public void pop() {
        stack.removeFirst();
    }
    
    public int top() {
        return stack.getFirst().value;
    }
    
    public int getMin() {
        return stack.getFirst().min;
    }
}
