class MinStack {
    MinStack top;
    MinStack tail;
    int value;
    int min;

    /** initialize your data structure here. */
    public MinStack() {
        top = null;
        tail = null;
        min = Integer.MAX_VALUE;
    }

    public void push(int x) {
        MinStack minStack = new MinStack();
        minStack.tail = top;
        minStack.value = x;
        top = minStack;
        top.min = min > x ? x : min;
        min = min > x ? x : min;
    }

    public void pop() {
        top = top.tail;
        if(top != null) {
            min = top.min;
        }else{
            min = Integer.MAX_VALUE;
            top = null;
        }
    }

    public int top() {
        return top.value;
    }

    public int getMin() {
        return top.min;
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */