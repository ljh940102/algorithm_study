class Solution {
    int[] origin;
    int[] rand;
    
    public Solution(int[] nums) {
        origin = nums;
        //rand = nums; 이따위로 하면 shuffle 함수에서 origin 값도 변경됨
        rand = Arrays.copyOf(nums, nums.length);
    }
    
    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        return origin;
    }
    
    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        for(int i = 0; i<rand.length; i++){
            double dValue = Math.random();
            int iValue = (int)(dValue * 10)%rand.length;
            int tmp = rand[iValue];
            rand[iValue] = rand[i];
            rand[i] = tmp;
        }
        
        return rand;
    }
    
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int[] param_1 = obj.reset();
 * int[] param_2 = obj.shuffle();
 */