import copy, random


class Solution:
    origin_nums = []
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.origin_nums = copy.deepcopy(nums)

    def reset(self) -> List[int]:
        return self.origin_nums

    def shuffle(self) -> List[int]:
        length = len(self.nums)
        for _ in range(length):
            idx1 = random.randrange(length)
            idx2 = random.randrange(length)
            self.nums[idx1], self.nums[idx2] = self.nums[idx2], self.nums[idx1]
        return self.nums

# Your Solution object will be instantiated and called as such:
# obj = Solution(nums)
# param_1 = obj.reset()
# param_2 = obj.shuffle()