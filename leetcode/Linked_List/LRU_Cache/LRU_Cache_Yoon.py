class Node:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None
    

class LRUCache:

    def __init__(self, capacity: int):
        self.cache = {}
        self.capacity = capacity
        self.head = Node(-1, -1)
        self.tail = Node(-1, -1)
        self.head.next = self.tail
        self.tail.prev = self.head

    def remove_node(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev
    
    def add_node(self, node):
        temp = self.head.next
        self.head.next = node
        node.prev = self.head
        node.next = temp
        temp.prev = node
    
    def move_to_head(self,node):
        self.remove_node(node)
        self.add_node(node)
    
    def pop_tail(self):
        temp = self.tail.prev
        self.tail.prev = self.tail.prev.prev
        self.tail.prev.next = self.tail
        return temp
    
    def get(self, key: int) -> int:
        if key not in self.cache:
            return -1
        else:
            node_to_reorder = self.cache[key]
            self.move_to_head(node_to_reorder)
            return node_to_reorder.value

    def put(self, key: int, value: int) -> None:
        if key in self.cache:
            node = self.cache[key]
            node.value = value
            self.move_to_head(node)
        else:
            if len(self.cache) == self.capacity:
                node_pop = self.pop_tail()
                del self.cache[node_pop.key]
            node_to_add = Node(key, value)
            self.add_node(node_to_add)
            self.cache[key] = node_to_add


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
