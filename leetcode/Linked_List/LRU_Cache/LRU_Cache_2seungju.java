class LRUCache {
    private Map<Integer, Integer> cache = new HashMap<>();
    private Queue<Integer> queue = new LinkedList<>();
    private int capacity;

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        int value;
        if (cache.get(key) != null) {
            value = cache.get(key);

            int count = 0;
            while (count < capacity) {
                int temp = queue.poll();
                if (temp != key) {
                    queue.offer(temp);
                }

                count++;
            }
            queue.offer(key);

        } else {
            value = -1;
        }

        return value;
    }

    public void put(int key, int value) {
        cache.put(key, value);
        queue.offer(key);

        if (queue.size() > capacity) {
            int temp = queue.poll();
            cache.remove(temp);
        }

    }
}
