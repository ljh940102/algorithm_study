class Node:
    def __init__(self, value, key, left=None, right=None):
        self.left = left
        self.right = right
        self.value = value
        self.key = key

    def __repr__(self):
        return str(self.key)


class NodeList:
    def __init__(self):
        self.leftmost = None
        self.rightmost = None
        self.len = 0

    def add(self, target_node):
        self.len += 1

        if self.leftmost:  # node가 하나라도 있는 경우
            self.leftmost.left = target_node
            target_node.right = self.leftmost

        else:  # node가 없는 경우
            self.rightmost = target_node

        self.leftmost = target_node

    def delete(self, target_node):
        self.len -= 1

        if target_node.left:
            target_node.left.right = target_node.right
        else:
            self.leftmost = target_node.right

        if target_node.right:
            target_node.right.left = target_node.left
        else:
            self.rightmost = target_node.left

    def __repr__(self):  # __repr__ vs __str__
        rtn_str = []
        curr_node = self.leftmost
        while curr_node:
            rtn_str.append(f"{curr_node.key}: {curr_node.value}({curr_node.left},{curr_node.right})")
            curr_node = curr_node.right

        return ", ".join(rtn_str)


class LRUCache:
    """
    Orderdict로 풀면 너무 쉬우니..
    dict와 double list 사용
    """

    def __init__(self, capacity: int):
        self.capacity = capacity
        self.cache_dict = dict()  # {key: Node}
        self.cache_list = NodeList()

    def get(self, key: int) -> int:
        if key in self.cache_dict:
            target_node = self.cache_dict[key]

            self.cache_list.delete(target_node)
            target_node.left, target_node.right = None, None

            self.cache_list.add(target_node)

            return target_node.value
        else:
            return -1

    def put(self, key: int, value: int) -> None:
        if key in self.cache_dict:
            target_node = self.cache_dict[key]

            # value update
            target_node.value = value

            # cache list refresh
            self.cache_list.delete(target_node)
            target_node.left, target_node.right = None, None
            self.cache_list.add(target_node)

        else:
            new_node = Node(value=value, key=key)
            self.cache_dict[key] = new_node
            self.cache_list.add(new_node)

        if self.cache_list.len > self.capacity:  # exceed
            delete_target = self.cache_list.rightmost
            self.cache_list.delete(delete_target)
            del (self.cache_dict[delete_target.key])