class LRUCache {
    public class CacheNode{
        int key;
        int value;
        CacheNode recent;
        CacheNode old;
        public CacheNode(int key, int value){
            this.key = key;
            this.value = value;
        }
    }
    CacheNode LRU;
    CacheNode oldest;
    
    Map<Integer, CacheNode> cacheMap;
    
    private final int capacity;

    
    public LRUCache(int capacity) {
        LRU = null;
        oldest = null;
        this.capacity = capacity;
        cacheMap = new HashMap<>();    
    }

    public int get(int key) {
        if(!cacheMap.containsKey(key)){
            return -1;
        }else{
            CacheNode current = cacheMap.get(key);
            
            if(current != LRU){
                if(current == oldest){
                    oldest = oldest.recent;
                }
                
                if(current.recent != null){
                    current.recent.old = current.old;
                }
                if(current.old != null){
                    current.old.recent = current.recent;
                }
                current.old = LRU;
                LRU.recent = current;
                current.recent = null;
                LRU = current;
            }
            return current.value;
        }
    }
    
    public void put(int key, int value){
        if(get(key) == -1){
            CacheNode current = new CacheNode(key, value);
            
            if(LRU == null){
                LRU = current;
                oldest = current;
            }else{
                current.old = LRU;
                LRU.recent = current;
                LRU = current;
            }
            
            cacheMap.put(key, current);
            
            if(cacheMap.size() > capacity){
                cacheMap.remove(oldest.key);
                oldest.recent.old = null;
                oldest = oldest.recent;
            }
        }else{
            cacheMap.get(key).value = value;
        }
    }
}

/*
class LRUCache extends LinkedHashMap<Integer, Integer> {
    private final int capacity;

    public LRUCache(int capacity) {
        super(capacity, 0.75f, true);
        this.capacity = capacity;
    }

    public int get(int key) {
        return super.getOrDefault(key, -1);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > capacity;
    }
}
*/
/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */

