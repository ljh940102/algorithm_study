/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode oddEvenList(ListNode head) {
        ListNode answer = head;
        
        if(head != null && head.next != null){
            
            ListNode evenNode = head.next;
            ListNode conn = evenNode;
            
            while(head.next != null && head.next.next != null){
                head.next = head.next.next;
                evenNode.next = head.next.next;
                head = head.next;
                evenNode = evenNode.next;
            }
        
            head.next = conn;
        }
        
        return answer;
    }
}
