import java.util.*;

class Solution {
    public ListNode oddEvenList(ListNode head) {
        Stack<Integer> odd = new Stack<>();
        Stack<Integer> even = new Stack<>();

        int index = 1;
        while (head != null) {
            int a = head.val;

            if (index % 2 == 1) {
                odd.push(a);

            } else {
                even.push(a);
            }

            head = head.next;

            index++;
        }

        ListNode result;
        if (even.isEmpty()) {
            result = null;

        } else {
            result = new ListNode(even.pop());
            result = makeNode(even, result);
        }

        result = makeNode(odd, result);

        return result;
    }

    private ListNode makeNode(Stack<Integer> stack, ListNode listNode) {
        while (!stack.isEmpty()) {
            listNode = new ListNode(stack.pop(), listNode);
        }

        return listNode;
    }
}