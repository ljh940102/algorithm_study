# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        OddNode = head
        
        EvenNode = None
        if head is None:
            return head
        if head.next is not None:
            EvenNode = head.next
        EvenNodeStart = EvenNode

        while OddNode.next is not None and EvenNode.next is not None:
            if OddNode is not None:
                OddNode.next = OddNode.next.next
                OddNode = OddNode.next
            if EvenNode is not None:
                EvenNode.next = EvenNode.next.next
                EvenNode = EvenNode.next
        OddNode.next = EvenNodeStart
        return head