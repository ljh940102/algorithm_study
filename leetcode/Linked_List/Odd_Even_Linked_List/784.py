# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def oddEvenList(self, head: ListNode) -> ListNode:
        length, check_len = 0, head
        while check_len:
            length += 1
            check_len = check_len.next

        if length < 3:
            return head

        odd, even = head, head.next
        origin_even = head.next

        target = head.next.next  # 세번쨰 노드부터 접근
        idx = 3
        while target:
            if idx % 2 == 1:  # 홀수
                odd.next = target
                odd = odd.next
                target = target.next
                odd.next = None


            else:  # 짝수
                even.next = target
                even = even.next
                target = target.next
                even.next = None
            idx += 1

        odd.next = None
        even.next = None

        odd.next = origin_even

        return head