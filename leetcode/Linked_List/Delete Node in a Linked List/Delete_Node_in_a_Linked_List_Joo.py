"""
문제이해하는데 좀 걸렸네..
head 포인터는 주어지지 않고 단순하게 node가 주어졌을 때, 해당 node를 지우는 함수를 만드는게 문제
"""
class Solution:
    def deleteNode(self, node):
        node.val = node.next.val
        node.next = node.next.next