/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        if(head==null)
            return false;

        //ListNode.val 범위가 -10^5~10^5 라고 했으므로 탐색하면서 100001 셋팅
        //100001을 만나면 순회하는 NodeList, null을 만나면 순회하지 않는 NodeList
        while(head.val!=100001){
            head.val = 100001;
            head = head.next;
            if(head==null)
                return false;
        }
        
        return true;
    }
}