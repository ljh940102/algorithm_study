/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public boolean hasCycle(ListNode head) {
        //이전 풀이는 ListNode.val에 모든 값이 들어갈 수 있으면 정답을 찾을 수 없음
        //다른 풀이법을 찾아보다가 Remove Nth Node From End의 토끼거북이 방법 이야기가 나와서 구현해봄
        //fast 노드와 slow 노드를 각기 다른 속도로 탐색하며 서로 만나면 true 반환
        //이전 풀이보다는 속도면에서 조금 떨어짐
        ListNode fast = head;
        ListNode slow = head;
        while(fast!=null && fast.next!=null){
            fast = fast.next.next;
            slow = slow.next;
            if(slow==fast){
                return true;
            }
        }
        return false;
    }
}