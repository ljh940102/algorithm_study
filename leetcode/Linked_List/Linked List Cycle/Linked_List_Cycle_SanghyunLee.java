class Solution {
    public boolean hasCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        // fast가 더이상 이동할 수 없는 리스트의 끝에 도달할 때까지 반복
        // 끝이 있다면 어차피 fast가 먼저 끝에 도달할 것이므로 slow는 검사할 필요 없음
        // 끝이 없다(cycle이 존재한다)면 loop를 벗어날 수 없으니 무조건 만날 수 밖에 없음
        while(fast != null && fast.next != null) {
        	fast = fast.next.next;
        	slow = slow.next;
        	if(fast == slow) {
        		return true;
        	}
        }
        return false;
    }
}

/*
class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
}
*/
