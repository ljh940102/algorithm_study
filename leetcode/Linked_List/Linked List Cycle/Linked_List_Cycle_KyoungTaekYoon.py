# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        prev = head
        Front = head
        while Front != None and Front.next != None:
            prev = prev.next
            Front = Front.next.next
            if prev == Front:
                return True
        return False
            
        