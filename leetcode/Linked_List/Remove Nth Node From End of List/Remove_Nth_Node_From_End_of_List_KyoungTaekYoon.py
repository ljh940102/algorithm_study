# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        slowNode = head
        fastNode = head
        for i in range(n):
            fastNode = fastNode.next
        if not fastNode:
            return head.next
        while fastNode.next is not None:
            slowNode = slowNode.next
            fastNode = fastNode.next
        slowNode.next = slowNode.next.next
        return head