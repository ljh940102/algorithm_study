/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

class Solution {
	public ListNode removeNthFromEnd(ListNode head, int n) {
		// head 앞에 새로운 노드 추가 (첫번째 노드 삭제 시 exception 방지)
		ListNode head_prev = new ListNode(0);
		head_prev.next = head;
		
		// fast와 slow 모두 0번째 노드(head_prev)를 가리킴
		ListNode fast = head_prev;
		ListNode slow = head_prev;

		// fast는 n + 1번째 노드를 가리킴
		for (int i = 1; i <= n + 1; i++) {
			fast = fast.next;
		}
		// fast와 slow는 n + 1칸 차이남
		while (fast != null) {
			// fast는 맨 뒤 null까지 이동
			fast = fast.next;
			// slow는 fast에서 null에서 n + 1칸 앞에 위치 (삭제할 노드 한칸 앞)
			slow = slow.next;
		}
		// 맨뒤에서 n번째 노드 삭제
		slow.next = slow.next.next;
		return head_prev.next;
	}
	/*
	public ListNode removeNthFromEnd(ListNode head, int n) {
		LinkedList<ListNode> linkedList = new LinkedList<>();
		ListNode node = head;
		while (node != null) {
			if (linkedList.size() == (n + 1)) {
				linkedList.removeFirst();
			}
			linkedList.add(node);
			node = node.next;
		}
		// node는 삭제할 노드의 prev

		// 첫번째 노드를 삭제해야하는 경우
		if (linkedList.size() == n) {
			return head.next;
		}

		node = linkedList.getFirst();
		node.next = node.next.next;
		return head;
	}
	*/
}
