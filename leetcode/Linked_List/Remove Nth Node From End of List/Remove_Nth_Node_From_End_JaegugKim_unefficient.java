/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        int length = 1;
        ListNode tmp = head;
        
        //ListNode 길이 구하기
        while(tmp.next != null){
            tmp= tmp.next;
            length++;
        }
        
        //length가 1일 때 예외처리(안 하면 NullPointer Exception)
        if(length==1 && n==1){
            return null;
        }
        
        //length 와 n이 동일하면 맨 처음 노드만 제거
        if(length == n){
            tmp = head.next;
            head = tmp;
            return head;
        }
        
        //length - n 까지 이동
        tmp = head;
        for(int i=0; i<length-n-1; i++){
            tmp=tmp.next;
        }

        //n이 1이면 마지막 노드만 제거
        if(n == 1){
            tmp.next = null;
            return head;
        }
        
        //일반적인 케이스에서는 중간 노드 하나만 제거
        tmp.next = tmp.next.next;
        
        return head;
    }
}