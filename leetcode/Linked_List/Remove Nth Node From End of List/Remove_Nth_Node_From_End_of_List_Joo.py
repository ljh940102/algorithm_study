# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

"""
포인터 2개 사용하기 -> 첫번째가 n번 next 한 뒤, 새로운 포인터 선언.
이후 첫번째 포인트가 움직일 떄 두번째도 같이 움직이면서 간격 유지하고, 
tail에 도달했을 때 두 번째 포인터가 가르키는 node 삭제
"""

class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        first = head
        second = head

        if head.next is None:  # 노드가 한개인 case는 따로 처리
            head = None
            return head

        if n == 1:  # tail 노드인 경우
            while first.next.next is not None:
                first = first.next
            first.next = None
            return head

        for move in range(n-1):
            first = first.next

        while first.next is not None:
            first = first.next
            second = second.next

        second.val = second.next.val
        second.next = second.next.next

        return head