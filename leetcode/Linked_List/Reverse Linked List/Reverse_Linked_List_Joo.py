# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if head is None or head.next is None:  # 노드가 한개이거나 없는 케이스 처리
            return head

        next_node = None
        prev_node = head
        current_node = head.next

        while current_node.next is not None:
            next_node = current_node.next
            current_node.next = prev_node
            prev_node = current_node
            current_node = next_node

        current_node.next = prev_node
        head.next = None

        return current_node
