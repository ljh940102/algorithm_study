/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

class Solution {
	public ListNode reverseList(ListNode head) {
		// 길이 제약이 주어지지 않았으므로, 길이 0에 대한 대비 필요
		if(head == null) {
			return head;
		}
		ListNode prev = null;
		ListNode curr = head;
		while(curr != null) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		return prev;
	}
}



