/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode answer = null;
        while(head != null){
            //다음 노드를 tmp에 저장
            ListNode tmp = head.next;
            //현재 노드의 다음 노드를 answer 로 지정
            head.next = answer;
            //answer에 현재 노드의 주소를 할당
            answer = head;
            //처리가 끝난 노드는 head에서 제거
            head = tmp;
        }
        return answer;
    }
}
