# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if head == None:
            return head
        prev = None
        Current = head
        while Current is not None:
            temp = Current.next
            Current.next = prev
            prev = Current
            Current = temp
        head = prev
        return prev