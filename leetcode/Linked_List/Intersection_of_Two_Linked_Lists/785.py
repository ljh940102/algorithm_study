class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        """
        headA 끝까지 순회하면서, node 주소를 key로하는 dict에 방문 여부 체크.
        이후 headB 순회하면서 dict에 있는 노드중 가장 먼저 만난 노드가 정답
        """
        check = dict()
        while headA:
            check[id(headA)] = True
            headA = headA.next

        while headB:
            if id(headB) in check:
                return headB
            headB = headB.next

        return None