/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        
        int lengthA = getLength(headA);
        int lengthB = getLength(headB);
        
        if(lengthA > lengthB){
            headA = cutLongList(headA, lengthA - lengthB);
        }else{
            headB = cutLongList(headB, lengthB - lengthA);
        }
        
        while(headA != null){
            if(headA == headB) return headA;
            headA = headA.next;
            headB = headB.next;
        }
        return null;
    }
    
    private int getLength(ListNode head){
        int length = 0;
        
        while(head != null){
            length++;
            head = head.next;
        }
            
        return length;
    }
    
    private ListNode cutLongList(ListNode head, int lengthDiff){
        while(lengthDiff>0){
            head = head.next;
            lengthDiff--;
        }
        return head;
    }
}
