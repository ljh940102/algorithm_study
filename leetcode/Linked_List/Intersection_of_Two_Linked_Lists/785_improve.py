class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        len_a, len_b = 0, 0
        check_a, check_b = headA, headB
        while check_a:
            len_a += 1
            check_a = check_a.next

        while check_b:
            len_b += 1
            check_b = check_b.next

        if len_a > len_b:
            for _ in range(len_a - len_b):
                headA = headA.next
        elif len_a < len_b:
            for _ in range(len_b - len_a):
                headB = headB.next
        while headA and headB:
            if headA is headB:
                return headA

            headA = headA.next
            headB = headB.next

        return None
