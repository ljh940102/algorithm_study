import java.util.*;

public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        Set<ListNode> set = new HashSet<>();
        while (headA != null) {
            set.add(headA);

            headA = headA.next;
        }

        while (headB != null) {
            if (set.contains(headB)) {
                break;
            }

            headB = headB.next;
        }

        return headB;

//        a + b is same
//        while (a != b) {
//            a = (a == null) ? headB : a.next;
//            b = (b == null) ? headA : b.next;
//        }
//
//        return a;
    }
}