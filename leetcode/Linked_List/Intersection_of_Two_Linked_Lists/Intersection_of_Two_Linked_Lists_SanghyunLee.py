# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:

    def get_length(self, head):
        length = 0
        while head:
            length += 1
            head = head.next
        return length

    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        lenA = self.get_length(headA)
        lenB = self.get_length(headB)
        while lenA < lenB:
            headB = headB.next
            lenB -= 1
        while lenB < lenA:
            headA = headA.next
            lenA -= 1
        while headA != headB:
            headA = headA.next
            headB = headB.next
        return headA
