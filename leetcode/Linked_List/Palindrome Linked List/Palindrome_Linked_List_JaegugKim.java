/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public boolean isPalindrome(ListNode head) {
        if(head == null || head.next == null)
            return true;
        
        //중간 찾기 (Remove Nth Node From End 참고)
        ListNode fast = head;
        ListNode slow = head;
        while(fast.next!=null && fast.next.next!=null){
            fast = fast.next.next;
            slow = slow.next;
        }
        ListNode center = slow.next;
        
        //처음부터 올바른 순서로 탐색할 노드와
        ListNode straight = head;
        //끝에서부터 반대 순서로 탐색할 노드 셋팅(Reverse Linked List 참고)
        ListNode reverse = null;
        while(center != null){
            ListNode tmp = center.next;
            center.next = reverse;
            reverse = center;
            center = tmp;
        }
        
        //탐색하며 비교하면서 일치하지 않으면 false return
        while(reverse!=null){
            if(first.val != reverse.val)
                return false;
            first = first.next;
            reverse = reverse.next;
        }
        /*
        원래는 중간 찾기에서 중간지점까지의 length를 구한 다음 for문을 돌렸는데,(<=length)
        길이가 홀수일 때는 reverse에서 nullpointerException을 뱉어서 reverse가 null이 되기 전까지만 실행
        */
        
        
        return true;
    }
}