import java.util.*;

class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int a = l1.val;
        int b = l2.val;
        ListNode listNode1 = l1.next;
        ListNode listNode2 = l2.next;

        int sum = a + b;
        Stack<ListNode> stack = new Stack<>();
        stack.push(new ListNode(sum % 10));

        while (true) {
            if (listNode1 != null) {
                a = listNode1.val;
                listNode1 = listNode1.next;

            } else {
                a = 0;
            }

            if (listNode2 != null) {
                b = listNode2.val;
                listNode2 = listNode2.next;

            } else {
                b = 0;
            }

            sum = a + b + (sum / 10);

            if (sum == 0 && listNode1 == null && listNode2 == null) {
                break;
            } else {
                stack.push(new ListNode(sum % 10));
            }
        }

        ListNode result = new ListNode(stack.pop().val);
        while (!stack.isEmpty()) {
            result = new ListNode(stack.pop().val, result);
        }

        return result;
    }
}