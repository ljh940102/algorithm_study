class Solution:
    def calc(self, next_node, target, carry):

        if carry:
            carry = False
            target += 1

        if target > 9:
            carry = True
            target = target % 10

        next_node.val = target
        prev_node = next_node
        next_node.next = ListNode()
        next_node = next_node.next

        return prev_node, next_node, target, carry

    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        rtn = ListNode()
        next_node = rtn
        carry = False

        while l1 and l2:
            target = l1.val + l2.val

            prev_node, next_node, target, carry = self.calc(next_node, target, carry)

            l1 = l1.next
            l2 = l2.next

        # 마지막 루프에 빈 node가 추가되므로, 삭제
        prev_node.next = None

        # l1이 남은 경우
        if l1:
            next_node = ListNode()
            prev_node.next = next_node
            while l1:
                target = l1.val
                prev_node, next_node, target, carry = self.calc(next_node, target, carry)
                l1 = l1.next

        # l1이 남은 경우
        if l2:
            next_node = ListNode()
            prev_node.next = next_node

            while l2:
                target = l2.val
                prev_node, next_node, target, carry = self.calc(next_node, target, carry)
                l2 = l2.next

        if carry:
            prev_node.next = ListNode()
            prev_node.next.val = 1
        else:
            prev_node.next = None

        return rtn