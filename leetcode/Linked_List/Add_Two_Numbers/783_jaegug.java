/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int sum = 0;
        boolean carry = false;
        ListNode answer = new ListNode();
        ListNode node = answer;
        
        while(l1 != null || l2 != null){
            int val1 = 0, val2 = 0;
            
            if(l1 != null){
                val1 = l1.val;    
                l1 = l1.next;
            }
            
            if(l2 != null){
                val2 = l2.val;
                l2 = l2.next;
            }
            
            sum = val1 + val2;
            
            if(carry) sum++;
            
            if(sum >= 10){
                sum -= 10;
                carry = true;
            }else{
                carry = false;
            }
            
            ListNode temp  = new ListNode(sum);
            node.next = temp;
            node = node.next;            
        }

        if(carry){
            ListNode temp  = new ListNode(1);
            node.next = temp;            
        }
            
        return answer.next;
    }
}
