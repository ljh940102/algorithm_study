class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:

        if l1 is None:
            return l2
        if l2 is None:
            return l1

        if l1.val <= l2.val:
            rtn = now_node = ListNode(l1.val,None)
            l1 = l1.next
        else:
            rtn = now_node = ListNode(l2.val,None)
            l2 = l2.next

        while l1 is not None and l2 is not None:  # 두 개 리스트가 모두 None이 아닌 동안 수행
            if l1.val <= l2.val:
                now_node.next = ListNode(l1.val,None)
                now_node = now_node.next
                l1 = l1.next
            else:
                now_node.next = ListNode(l2.val, None)
                now_node = now_node.next
                l2 = l2.next

        if l1 is None:
            while l2 is not None:
                now_node.next = ListNode(l2.val, None)
                now_node = now_node.next
                l2 = l2.next

        if l2 is None:
            while l1 is not None:
                now_node.next = ListNode(l1.val, None)
                now_node = now_node.next
                l1 = l1.next

        return rtn
