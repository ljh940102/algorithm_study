/*
class ListNode {
	int val;
	ListNode next;
	ListNode() {}
	ListNode(int val) {
		this.val = val;
	}
	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}
*/

class linked_list_leet_easy_Merge_Two_Sorted_Lists_Solution {
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		// dummy node
		ListNode head_prev = new ListNode(0, null);
		ListNode tail = head_prev;

		while (l1 != null && l2 != null) {
			if (l1.val < l2.val) {
				tail.next = l1;
				tail = tail.next;
				l1 = l1.next;
			}
			else if (l1.val == l2.val) {
				// 이 부분에서 l1추가 로직 다 끝나고나서 l2추가 로직 시작해야 하는 것 유의
				tail.next = l1;
				tail = tail.next;
				l1 = l1.next;

				tail.next = l2;
				tail = tail.next;
				l2 = l2.next;
			}
			else if (l1.val > l2.val) {
				tail.next = l2;
				tail = tail.next;
				l2 = l2.next;
			}
		}
		if (l1 != null) {
			tail.next = l1;
		}
		else if (l2 != null) {
			tail.next = l2;
		}
		return head_prev.next;
	}
	/*
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		// dummy node
		ListNode head_prev = new ListNode(-1);
		ListNode tail = head_prev;

		while (true) {
			if (l1 == null) {
				tail.next = l2;
				break;
			}
			if (l2 == null) {
				tail.next = l1;
				break;
			}
			if (l1.val < l2.val) {
				tail.next = l1;
				l1 = l1.next;
			}
			 else {
				tail.next = l2;
				l2 = l2.next;
			}
			tail = tail.next;
		}
		return head_prev.next;
	}
	*/
}