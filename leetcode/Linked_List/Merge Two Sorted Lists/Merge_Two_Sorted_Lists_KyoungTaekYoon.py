# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        Current = head = ListNode(0)
        FirstGroup = l1
        SecondGroup = l2
        
        while FirstGroup is not None and SecondGroup is not None:
            if FirstGroup.val > SecondGroup.val:
                Current.next = SecondGroup
                SecondGroup = SecondGroup.next
            else:
                Current.next = FirstGroup
                FirstGroup = FirstGroup.next
            Current = Current.next
        
        if FirstGroup is None:
            Current.next = SecondGroup
        else:
            Current.next = FirstGroup
        return head.next