/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode answer = null;
        //초기값 셋팅 - new 로 0 넣는 방법 몰랐음
        //target ListNode 가 모두 null이면 null 반환
        if(l1 == null && l2 == null){
            return answer;   
        //l1 이 null 이면 l2 할당 후 할당된 l2 노드 삭제
        }else if(l1 == null){
            answer = l2;
            l2 = l2.next;
        }else if(l2 == null){
            answer = l1;
            l1 = l1.next;
        //l1 값이 l2 값보다 작으면 l1 할당 후 할당된 l1 노드 삭제
        }else if(l1.val < l2.val){
            answer = l1;
            l1 = l1.next;
        }else if(l1.val >= l2.val){
            answer = l2;
            l2 = l2.next;
        }
    
        //더 작은 값을 탐색할 임시 노드 생성
        ListNode tmp = answer;
        
        // l1, l2 모두 null 만 남을 때까지 loop
        while(l1 != null || l2 != null){
            // 임시 노드의 next 노드에 더 작은 값 할당
            if(l1 == null){
                tmp.next = l2;
                l2 = l2.next;
            }else if(l2 == null){
                tmp.next = l1;
                l1 = l1.next;
            }else if(l1.val < l2.val){
                tmp.next = l1;
                l1 = l1.next;
            }else if(l1.val >= l2.val){
                tmp.next = l2;
                l2 = l2.next;
            }
            // next 방향으로 한칸씩 이동하며 탐색
            tmp = tmp.next;
        }
        
        return answer;
    }
}