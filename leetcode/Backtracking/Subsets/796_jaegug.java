class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        boolean[] visited = new boolean[nums.length];
        List<List<Integer>> answer = new ArrayList<>();
        
        genSubsets(0, nums, visited, answer);
        return answer;
    }
    
    private void genSubsets(int cnt, int[] nums, boolean[] visited, List<List<Integer>> answer){
        if(cnt == nums.length){
            List<Integer> subset = new ArrayList<>();
            
            for(int i = 0; i < visited.length; i++){
                if(visited[i]){
                    subset.add(nums[i]);
                }
            }
            answer.add(subset);
            return;
        }
        
        visited[cnt] = false;
        genSubsets(cnt+1, nums, visited, answer);
        
        visited[cnt] = true;
        genSubsets(cnt+1, nums, visited, answer);
    }
}
