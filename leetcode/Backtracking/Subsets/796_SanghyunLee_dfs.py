from typing import List


class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        return self.dfs(done=[], yet=nums, ret=[])

    def dfs(self, done, yet, ret):
        ret.append(done)
        for i, num in enumerate(yet):
            self.dfs(done + [num], yet[i+1:], ret)
        return ret


#print(Solution().subsets([0]))
#print(Solution().subsets([1, 2, 3]))
