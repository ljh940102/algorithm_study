class Solution:
    rtn = []

    def combi(self, nums, choose, start, by):
        if len(choose) == by:
            self.rtn.append(choose[:])  # 얕은복사
            return

        for idx in range(start, len(nums)):  # start 이용해서 중복방지
            choose.append(nums[idx])
            self.combi(nums, choose, idx + 1, by)
            choose.pop()

    def subsets(self, nums: List[int]) -> List[List[int]]:
        if not nums:
            return []

        self.rtn = [[]]

        for by in range(len(nums)):  # 길이별로 추출
            self.combi(nums, [], 0, by + 1)

        return self.rtn