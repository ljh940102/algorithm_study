class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i <= nums.length; i ++) {
            func(result, new boolean[nums.length], nums, 0, i);
        }

        return result;
    }


    public void func(List<List<Integer>> result, boolean[] check, int[] nums, int index, int r) {
        if (r == 0) {
            List<Integer> combination = new ArrayList<>();
            for (int i = 0; i < nums.length; i++) {
                if (check[i]) {
                    combination.add(nums[i]);
                }
            }

            result.add(combination);

        } else if (index < nums.length) {
            check[index] = true;
            func(result, check, nums, index + 1, r - 1);

            check[index] = false;
            func(result, check, nums, index + 1, r);
        }
    }
}