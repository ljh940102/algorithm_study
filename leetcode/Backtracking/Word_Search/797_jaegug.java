class Solution {
    private boolean answer = false;
    
    public boolean exist(char[][] board, String word) {
        
        boolean[][] visited = new boolean[board.length][board[0].length];
        
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                findWord(0, j, i, board, word, visited);
            }
        }
        return answer;
    }
    
    public void findWord(int cnt, int x, int y, char[][] board, String word, boolean[][] visited){
        if(board[y][x] == word.charAt(cnt)){
            if(cnt == word.length()-1){
                answer = true;
                return;
            }else{
                visited[y][x] = true;
                if(x+1 < board[0].length && !visited[y][x+1]){
                    findWord(cnt+1, x+1, y, board, word, visited);
                }
                if(y+1 < board.length && !visited[y+1][x]){
                    findWord(cnt+1, x, y+1, board, word, visited);
                }
                if(x-1 >= 0 && !visited[y][x-1]){
                    findWord(cnt+1, x-1, y, board, word, visited);
                }
                if(y-1 >= 0 && !visited[y-1][x]){
                    findWord(cnt+1, x, y-1, board, word, visited);
                }
                visited[y][x] = false;
            }                
        }
    }
}
