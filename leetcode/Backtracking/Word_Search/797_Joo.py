from collections import Counter


class Solution:
    """
    먼저 board를 전체 순회해서 start point 저장하기 및 word의 전체 letters가 있는지 확인
    start포인트에서 dfs 탐색. dfs param은 visited, word, word_idx, target_point(idx tuple)
    dfs 탐색 포인트에서 상하좌우를 살펴서 원하는 letter가 있으면 재귀호출.(visited이 달라지겠지)
    """
    board = [[]]
    move_r, move_c = [-1, 1, 0, 0], [0, 0, -1, 1]
    m, n = 0, 0

    def is_safe(self, r, c, visited):
        if (r, c) in visited:  # 중복방문시 False
            return False

        if r < 0 or r >= self.m:
            return False

        if c < 0 or c >= self.n:
            return False

        return True

    def find_word(self, visited, word, word_curr_idx, target_point):
        if word[word_curr_idx] != self.board[target_point[0]][target_point[1]]:  # letter가 다른 경우 -> False
            return False

        if word_curr_idx == len(word) - 1:  # 탐색완료 -> True
            return True

        visited.append(target_point)  # 방문기록

        for move_idx in range(4):  # 상하좌우 탐색
            next_r, next_c = target_point[0] + self.move_r[move_idx], target_point[1] + self.move_c[move_idx]

            if self.is_safe(next_r, next_c, visited):
                if self.find_word(visited, word, word_curr_idx + 1, (next_r, next_c)):
                    return True

        visited.pop()  # 방문기록 reset

        return False

    def exist(self, board: List[List[str]], word: str) -> bool:
        self.m, self.n, self.board = len(board), len(board[0]), board

        start_points, letters = [], []
        for r_idx in range(self.m):
            for c_idx in range(self.n):
                letters.append(board[r_idx][c_idx])  # 존재하는 letter 저장

                if board[r_idx][c_idx] == word[0]:  # start point
                    start_points.append((r_idx, c_idx))

        # 사용하지 않는 변수를 del로 삭제해주는게 의미가 있을까?..
        if Counter(word) - Counter(letters):  # board로 커버 불가능
            return False

        for start_r, start_c in start_points:
            # self.find_word(visited=[], word=word, word_curr_idx=0, target_point=(start_r, start_c))
            if self.find_word([], word, 0, (start_r, start_c)):
                return True

        return False