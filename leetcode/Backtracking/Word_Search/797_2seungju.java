class Solution {
    private static final int[] X = {0, 0, 1, 0, -1};
    private static final int[] Y = {0, 1, 0, -1, 0};

    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;

        char[] alphabet = word.toCharArray();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (func(board, new boolean[m][n], alphabet, j, i, 0)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean func(char[][] board, boolean[][] check, char[] alphabet, int x, int y, int index) {
        int m = board.length;
        int n = board[0].length;

        for (int i = 0; i < X.length; i++) {
            int nextX = x + X[i];
            int nextY = y + Y[i];

            if (nextX < 0 || nextX >= n || nextY < 0 || nextY >= m) {
                continue;
            }

            if (board[nextY][nextX] == alphabet[index] && !check[nextY][nextX]) {
                if (index == alphabet.length - 1) {
                    return true;
                }

                check[nextY][nextX] = true;

                if (func(board, check, alphabet, nextX, nextY, index + 1)) {
                    return true;
                }

                check[nextY][nextX] = false;

            }
        }

        return false;
    }
}