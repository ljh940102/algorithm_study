class Solution:
    rtn, criteria = [], 0

    def find_backtracking(self, target_list, num_open, num_close):
        # num_open > num_close면 break
        if num_open > num_close:
            return

        if len(target_list) == self.criteria:
            self.rtn.append("".join(target_list))
            return

        if num_open > 0:
            target_list.append("(")
            self.find_backtracking(target_list, num_open - 1, num_close)
            target_list.pop()

        if num_close > 0:
            target_list.append(")")
            self.find_backtracking(target_list, num_open, num_close - 1)
            target_list.pop()

    def generateParenthesis(self, n: int) -> List[str]:
        self.rtn, self.criteria = [], n * 2
        # open과 close의 선택 개수로 backtracking
        self.find_backtracking([], n, n)

        return self.rtn