from itertools import product

class Solution:
    mapping = {
        "2": ["a", "b", "c"],
        "3": ["d", "e", "f"],
        "4": ["g", "h", "i"],
        "5": ["j", "k", "l"],
        "6": ["m", "n", "o"],
        "7": ["p", "q", "r", "s"],
        "8": ["t", "u", "v"],
        "9": ["w", "x", "y", "z"]
    }

    def letterCombinations(self, digits: str) -> List[str]:
        # 각 숫자를 mapping list로 변경
        target_lists = [self.mapping[digit] for digit in digits]

        if not target_lists:
            return []

        rtn = target_lists[0]
        for target_list in target_lists[1:]:  # 저장해둔 mapping 값으로 product 수행
            rtn = list(product(rtn, target_list))
            rtn = ["".join(elem) for elem in rtn]  # 각 조합이 list형태이므로, str형태로 변경

        return rtn