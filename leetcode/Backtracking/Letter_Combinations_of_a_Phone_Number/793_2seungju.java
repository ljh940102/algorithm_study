class Solution {
    private Map<Integer, List<String>> map = new HashMap<>();

    public List<String> letterCombinations(String digits) {
        map.put(2, List.of("a", "b", "c"));
        map.put(3, List.of("d", "e", "f"));
        map.put(4, List.of("g", "h", "i"));
        map.put(5, List.of("j", "k", "l"));
        map.put(6, List.of("m", "n", "o"));
        map.put(7, List.of("p", "q", "r", "s"));
        map.put(8, List.of("t", "u", "v"));
        map.put(9, List.of("w", "x", "y", "z"));

        List<String> result = new ArrayList<>();

        if (digits.equals("")) {
            return result;
        }

        String[] digit = digits.split("");

        func(result, digit, "", 0);

        return result;
    }

    private void func(List<String> result, String[] digit, String str, int index) {
        int number = Integer.parseInt(digit[index]);
        for (String d : map.get(number)) {
            if (digit.length > index + 1) {
                func(result, digit, str + d, index + 1);

            } else {
                result.add(str + d);
            }
        }
    }
}