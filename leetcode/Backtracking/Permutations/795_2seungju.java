class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        permutation(result, nums, 0);

        return result;
    }

    public void permutation(List<List<Integer>> result, int[] arr, int depth) {
        if (depth == arr.length) {
            List<Integer> list = new ArrayList<>();
            for (int value : arr) {
                list.add(value);
            }

            result.add(list);

            return;
        }

        for (int i = depth; i < arr.length; i++) {
            swap(arr, depth, i);
            permutation(result, arr, depth + 1);
            swap(arr, depth, i);
        }
    }

    public void swap(int[] arr, int depth, int i) {
        int temp = arr[depth];
        arr[depth] = arr[i];
        arr[i] = temp;
    }
}
