class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        visited = [False] * len(nums)
        result = []
        self.dfs(result, visited, [], nums)
        return result

    def dfs(self, result, visited, subset, nums):
        if len(subset) == len(nums):
            result.append(subset)
        for i in range(len(nums)):
            if not visited[i]:
                visited[i] = True
                self.dfs(result, visited, subset + [nums[i]], nums)
                visited[i] = False
