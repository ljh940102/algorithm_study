class Solution:
    n = 0
    rtn = []

    def permu(self, nums, choose):
        if len(choose) == self.n:
            # deepcopy시 시간효율 10%이상 떨어짐
            # 얕은복사 깊은복사 제대로알고 구별쓰기 유의
            self.rtn.append(choose[:])
            return

        for num in nums:
            if num not in choose:
                choose.append(num)
                self.permu(nums, choose)
                choose.pop()

    def permute(self, nums: List[int]) -> List[List[int]]:
        # itertools를 쓰면 쉽지만.. 연습겸 구현해보기
        self.n = len(nums)
        self.rtn = []

        self.permu(nums, [])

        return self.rtn