class Solution {
    fun removeDuplicates(s: String, k: Int): String {

        var charRepeatStack: Stack<Pair<Char, Int>> = Stack()

        s.forEach { c ->
            if (charRepeatStack.empty() || charRepeatStack.peek().first != c) {
                charRepeatStack.push(Pair(c, 1))
            } else {
                val cnt = charRepeatStack.peek().second + 1

                if(cnt == k){
                    charRepeatStack.pop()
                } else{
                    charRepeatStack.push(charRepeatStack.pop().copy(second = cnt))
                }
            }
        }

        var answer = ""
        
        while (charRepeatStack.isNotEmpty()) {
            
            val pair = charRepeatStack.pop()
            
            for(idx in 0 until pair.second){
                answer += pair.first
            }
        }
        
        return answer.reversed()
    }
}
