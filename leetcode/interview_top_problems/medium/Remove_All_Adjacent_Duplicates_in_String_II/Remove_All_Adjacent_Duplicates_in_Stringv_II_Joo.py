class Solution:
    def removeDuplicates(self, s: str, k: int) -> str:
        stack = []
        for ch in s:
            if stack:
                info = stack.pop()

                if info[0] == ch:
                    info[1] += 1

                    if info[1] == k:  # k번 등장하면 stack에 다시 추가하지 않고 스킵
                        continue

                    stack.append(info)
                else:
                    stack.append(info)
                    stack.append([ch, 1])

            else:
                stack.append([ch, 1])

        rtn = []

        for top_ch, top_cnt in stack:
            rtn.append(top_ch * top_cnt)

        return "".join(rtn)