class Solution:
    def removeDuplicates(self, s: str, k: int) -> str:
        # chr, cnt
        stack = []

        for c in s:
            # match -> merge cnt
            if stack and stack[-1][0] == c:
                stack[-1][1] += 1
                # cnt overflow -> pop
                if stack[-1][1] == k:
                    stack.pop()
                continue
            stack.append([c,1])

        answer = ""
        for chr, cnt in stack:
            answer += chr * cnt
        return answer


print(Solution().removeDuplicates(s="abcd", k=2))
print(Solution().removeDuplicates(s="deeedbbcccbdaa", k=3))
print(Solution().removeDuplicates(s="pbbcggttciiippooaais", k=2))

