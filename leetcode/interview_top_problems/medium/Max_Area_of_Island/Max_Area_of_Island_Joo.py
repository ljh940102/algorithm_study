class Solution:
    def is_safe(self, r, c):
        if r < 0 or r >= len(self.grid):
            return False

        if c < 0 or c >= len(self.grid[0]):
            return False

        if self.grid[r][c] == 0:
            return False

        return True

    def dfs(self, r, c):
        if not self.is_safe(r, c):
            return 0

        self.grid[r][c] = 0  # visited 역할

        val = 1
        for idx in range(4):
            val += self.dfs(r + self.move_r[idx], c + self.move_c[idx])

        return val

    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        self.rtn, self.grid = 0, grid
        self.move_r, self.move_c = (1, -1, 0, 0), (0, 0, 1, -1)

        for r in range(len(grid)):
            for c in range(len(grid[0])):
                self.rtn = max(self.rtn, self.dfs(r, c))

        return self.rtn
