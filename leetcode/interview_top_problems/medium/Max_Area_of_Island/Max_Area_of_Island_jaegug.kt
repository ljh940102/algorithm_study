class Solution {
    fun maxAreaOfIsland(grid: Array<IntArray>): Int {

        var result = 0
        
        val rows = grid.size
        val cols = grid[0].size
        
        for (i in 0 until rows) {
            for (j in 0 until cols) {
                if (grid[i][j] == 1) {
                    val area = infect(grid, i, j, rows, cols, 0)
                    result = Math.max(result, area)
                }
            }
        }

        return result
    }

    fun infect(grid: Array<IntArray>, i: Int, j: Int, rows: Int, cols: Int, area: Int): Int {
        if (i < 0 || i >= rows || j < 0 || j >= cols || grid[i][j] != 1) {
            return area
        }

        grid[i][j] = 2
        
        val dirs = arrayOf(arrayOf(-1, 0), arrayOf(1, 0), arrayOf(0, -1), arrayOf(0, 1))
        
        var sum = area + 1
        
        dirs.forEach { dir ->
            sum = infect(grid, i + dir[0], j + dir[1], rows, cols, sum)
        }

        return sum
    }
}
