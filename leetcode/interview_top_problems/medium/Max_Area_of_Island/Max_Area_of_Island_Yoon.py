from collections import deque
class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        visited = [[False] * n for _ in range(m)]
        directions = [(1,0), (0,1), (-1,0), (0, -1)]
        bfs = deque()
        ans = 0
        for r in range(m):
            for c in range(n):
                if not visited[r][c] and grid[r][c] == 1:
                    bfs.append((r, c))
                    temp_ans = 0
                    visited[r][c] = True
                    while bfs:
                        cur_r, cur_c = bfs.pop()
                        temp_ans += 1
                        for dr, dc in directions:
                            temp_r, temp_c = cur_r + dr, cur_c + dc
                            if 0 <= temp_r < m and 0 <= temp_c < n:
                                if not visited[temp_r][temp_c] and grid[temp_r][temp_c] == 1:
                                    visited[temp_r][temp_c] = True
                                    bfs.append((temp_r, temp_c))
                    ans = max(ans, temp_ans)
                    
        return ans
                    