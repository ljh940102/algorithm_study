from collections import deque
from typing import List


class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        answer = 0
        row_num = len(grid)
        col_num = len(grid[0])

        row_moves = [0, 0, 1, -1]
        col_moves = [1, -1, 0, 0]

        visited = set()
        q = deque()

        for row_init in range(row_num):
            for col_init in range(col_num):

                if grid[row_init][col_init] != 1:
                    continue

                if (row_init, col_init) in visited:
                    continue

                area_size = 0
                visited.add((row_init, col_init))
                q.append((row_init, col_init))

                while q:
                    row_curr, col_curr = q.popleft()

                    area_size += 1

                    for row_move, col_move in zip(row_moves, col_moves):
                        row_next = row_curr + row_move
                        col_next = col_curr + col_move

                        if row_next < 0 or row_next >= row_num:
                            continue

                        if col_next < 0 or col_next >= col_num:
                            continue

                        if grid[row_next][col_next] != 1:
                            continue

                        if (row_next, col_next) in visited:
                            continue

                        visited.add((row_next, col_next))
                        q.append((row_next, col_next))

                answer = max(answer, area_size)

        return answer


"""
print(Solution().maxAreaOfIsland(
    grid = [
        [0,0,1,0,0,0,0,1,0,0,0,0,0],
        [0,0,0,0,0,0,0,1,1,1,0,0,0],
        [0,1,1,0,1,0,0,0,0,0,0,0,0],
        [0,1,0,0,1,1,0,0,1,0,1,0,0],
        [0,1,0,0,1,1,0,0,1,1,1,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0],
        [0,0,0,0,0,0,0,1,1,1,0,0,0],
        [0,0,0,0,0,0,0,1,1,0,0,0,0]
    ]
))
"""
