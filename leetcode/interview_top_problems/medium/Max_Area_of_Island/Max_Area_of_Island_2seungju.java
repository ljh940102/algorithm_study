class Solution {
    private int[] X = {1, -1 , 0, 0};
    private int[] Y = {0, 0 , 1, -1};
    private int count = 0;

    public int maxAreaOfIsland(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;

        int max = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                count = 0;

                if (grid[i][j] == 1) {
                    dfs(grid, j, i);
                    max = Math.max(max, count);
                }
            }
        }

        return max;
    }

    private void dfs(int[][] grid, int x, int y) {
        count += 1;
        grid[y][x] = 0;

        for (int i = 0; i < 4; i++) {
            int newX = x + X[i];
            int newY = y + Y[i];
            if (!checkLength(newX, newY, grid.length, grid[0].length) && grid[newY][newX] == 1) {
                dfs(grid, newX, newY);
            }
        }
    }

    private boolean checkLength(int x, int y, int m, int n) {
        return y < 0 || x < 0 || y >= m || x >= n;
    }
}