from collections import defaultdict


class TweetCounts:

    def __init__(self):
        self.tweet_map = defaultdict(list)
        self.time_window_dict = {
            "minute": 60,
            "hour": 3600,
            "day": 86400
        }

    def recordTweet(self, tweetName: str, time: int) -> None:
        bisect.insort(self.tweet_map[tweetName], time)  # 정렬상태 유지하면서 insert

    def getTweetCountsPerFrequency(self, freq: str, tweetName: str, startTime: int, endTime: int) -> List[int]:
        rtn = []
        time_window = self.time_window_dict[freq]

        target_data = self.tweet_map.get(tweetName)

        if not target_data:
            return [0]

        curr_idx = 0

        curr_time = startTime

        while curr_time <= endTime:
            cnt_in_chunk = 0
            window_start, window_end = curr_time, curr_time + time_window
            if window_end > endTime:  # window가 최종시간을 넘은 경우
                window_end = endTime + 1

            if curr_idx >= len(target_data):  # idx를 초과한 경우
                rtn.append(cnt_in_chunk)
                curr_time = window_end
                continue

            while window_end > target_data[curr_idx]:
                if window_start <= target_data[curr_idx]:
                    cnt_in_chunk += 1
                    curr_idx += 1
                else:
                    curr_idx += 1

                if curr_idx >= len(target_data):
                    break

            rtn.append(cnt_in_chunk)
            curr_time = window_end

        return rtn