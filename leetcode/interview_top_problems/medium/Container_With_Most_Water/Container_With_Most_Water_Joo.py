class Solution:
    def maxArea(self, height: List[int]) -> int:
        left, right = 0, len(height) - 1
        max_water = 0

        while (left < right):  # 모든 경우 탐색
            curr_water = (right - left) * min(height[left], height[right])  # 너비 * 최소높이
            max_water = max(max_water, curr_water)

            if height[left] >= height[right]:
                right -= 1
            else:
                left += 1

        return max_water