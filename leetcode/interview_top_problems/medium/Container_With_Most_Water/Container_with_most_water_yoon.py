class Solution:
    def maxArea(self, height: List[int]) -> int:
        left, right = 0, len(height) - 1
        result = 0
        while True:
            shorter_height, shorter_dir = 0, ""
            if height[left] < height[right]:
                shorter_height = height[left]
                shorter_dir = "left"
            else:
                shorter_height = height[right]
                shorter_dir = "right"
            result = max(result, (right - left) * shorter_height)
            foundLonger = False
            if shorter_dir == "left":
                for i in range(left, right):
                    if shorter_height < height[i]:
                        left = i
                        foundLonger = True
                        break
            else:
                for i in range(right, left, -1):
                    if shorter_height < height[i]:
                        right = i
                        foundLonger = True
                        break
            if not foundLonger:
                break
                        
        return result
