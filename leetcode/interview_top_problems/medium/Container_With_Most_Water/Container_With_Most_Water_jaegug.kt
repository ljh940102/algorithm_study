class Solution {
    fun maxArea(height: IntArray): Int {
        var max = 0
        var left = 0
        var right = height.size - 1
    
        while (right > left) {
            val volume = (right - left) * if(height[right] > height[left]) height[left] else height[right]

            if(height[left] < height[right]) left++ else right--
            
            max = if(max < volume) volume else max
        }
        return max;
    }
}
