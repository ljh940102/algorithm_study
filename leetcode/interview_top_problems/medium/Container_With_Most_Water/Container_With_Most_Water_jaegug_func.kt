class Solution {
    fun maxArea(height: IntArray): Int {
        return findMaxArea(height, 0, 0, height.size-1)
    }

    fun findMaxArea(height:IntArray, max:Int, left:Int, right:Int): Int{
        val volume = (right - left) * if(height[right] > height[left]) height[left] else height[right]

        return if(right > left) 
                findMaxArea(height, if(max < volume) volume else max, 
                    if(height[left] < height[right]) left+1 else left, 
                    if(height[left] < height[right]) right else right-1) 
            else max
    }
}
