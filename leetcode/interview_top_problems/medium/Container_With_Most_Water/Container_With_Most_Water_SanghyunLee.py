from typing import List


class Solution:
    def maxArea(self, height: List[int]) -> int:
        max_area = 0

        left = 0
        right = len(height) - 1

        while left < right:
            left_height = height[left]
            right_height = height[right]
            area = (right - left) * min(left_height, right_height)
            max_area = max(max_area, area)
            if left_height < right_height:
                left += 1
            else:
                right -= 1
        
        return max_area


"""
print(Solution().maxArea([1,8,6,2,5,4,8,3,7]))
print(Solution().maxArea([1,1]))
print(Solution().maxArea([4,3,2,1,4]))
print(Solution().maxArea([1,2,1]))
"""
