class Solution {
    fun rotate(matrix: Array<IntArray>): Unit {
        return matrix.let {
            var start = 0
            var end = matrix.size - 1
            while(start < end){
                var tmp = 0
                for (i in start..end-1) {
                    tmp = matrix[start][i]
                    matrix[start][i] = matrix[end - (i - start)][start]
                    matrix[end - (i - start)][start] = matrix[end][end - (i - start)]
                    matrix[end][end - (i - start)] = matrix[i][end]
                    matrix[i][end] = tmp
                }
                start++
                end--
            }
        }
    }
}
