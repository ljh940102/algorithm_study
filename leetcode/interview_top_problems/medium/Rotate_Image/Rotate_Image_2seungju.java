class Solution {
    public void rotate(int[][] matrix) {
        int n = matrix.length;
        for (int i = 0; i < (n + 1) / 2; i++) {
            for (int j = 0; j < n / 2; j++) {
                int temp = matrix[n - j - 1][i];
                matrix[n - j - 1][i] = matrix[n - i - 1][n - j - 1];
                matrix[n - i - 1][n - j - 1] = matrix[j][n - i - 1];
                matrix[j][n - i - 1] = matrix[i][j];
                matrix[i][j] = temp;
            }
        }
    }

//     public void rotate(int[][] matrix) {
//         Stack<int[]> stack = new Stack<>();
//         for (int[] newIntArray : matrix) {
//             stack.push(newIntArray);
//         }

//         int index = 0;
//         matrix = new int[matrix.length][matrix.length];
//         while (!stack.isEmpty()) {
//             int[] newIntArray = stack.pop();
//             for (int i = 0; i < newIntArray.length; i++) {
//                 matrix[i][index] = newIntArray[i];
//             }

//             index++;
//         }
//     }
}