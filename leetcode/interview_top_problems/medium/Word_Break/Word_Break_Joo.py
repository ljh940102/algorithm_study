class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        wordDict = set(wordDict)
        n = len(s)

        dp = [False for _ in range(n + 1)]  # s(0:i)의 부분 문자열을 만들 수 있는지 여부
        dp[0] = True  # s(0:0)은 ""이므로 True처리

        for end_idx in range(1, n + 1):
            # 다시 index 0 부터 조회하면서 부분문자열 체크
            for start_idx in range(end_idx):
                if not dp[start_idx]:  # s(0:start_idx)가 생성 불가하면 s(start_idx:end_idx) 생성 가능여부가 무의미하므로 skip 처리
                    continue
                if s[start_idx:end_idx] in wordDict:
                    dp[end_idx] = True
                    break

        return dp[-1]