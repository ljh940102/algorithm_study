from typing import List


class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        length = len(s)
        words = set(wordDict)

        # dp[idx] = words로 s[0:idx] 만들 수 있는지 여부 기록
        dp = [False for _ in range(length + 1)]

        # 초기값인 s[0:0]은 빈 문자열("")이며 가능여부는 True로 세팅
        dp[0] = True

        for end in range(1, length + 1):
            for start in range(end):

                substr = s[start:end]
                if dp[start] and substr in words:
                    dp[end] = True
                    # dp[end]가 가능한지 확인하는게 목적이라서
                    # 가능한게 확인되면 더 이상 볼 필요 없으므로 break
                    break

        # 전체 문자열(s[0:length]) 생성 가능한지 여부 반환
        return dp[-1]


"""
print(Solution().wordBreak(s="applepenapple", wordDict=["apple","pen"]))
print(Solution().wordBreak(s="leetcode", wordDict=["leet","code"]))
print(Solution().wordBreak(s="applepenapple", wordDict=["apple","pen"]))
print(Solution().wordBreak(s="catsandog", wordDict=["cats","dog","sand","and","cat"]))
"""
