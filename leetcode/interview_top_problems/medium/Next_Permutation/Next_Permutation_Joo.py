class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        순열을 정렬했을 떄, 마지막은 역배열이다.
        즉, 뒤에서부터 역배열인 인덱스까지 살펴보고 역배열이 깨지는 인덱스까지는 순열의 max순번이라는 의미.
        이 떄, 다음 순열을 구하는 방법이 관건.
        1. 역배열이 꺠진수와 역배열에서 역배열을 깬 수보다 큰 수 중 가장 작은수를 골라 swap한다
            -> why? 역배열은 깬 수보다 작은 수는 이전(previous) 순열에 포함되는 대상
                    더 큰 수중 다로 다음으로 큰 수가 아니라면 next가 아니라 훨씬 앞에 있는 순열인 대상
                    즉, 역배열을 깬 수보다는 크지만, next를 발라내기 위해 가장 작은 수를 선택해서 새로운 순열(다음수열)의 맨 앞으로 내보낸다.
        2. 나머지는 오름차순으로 정렬
            -> 순열의 맨 앞머리를 바꿨으니, 나머지는 최소순열로 만들어야한다. (최소순열=작은값들이 앞으로 가있는 순열)
        """
        swap_target_idx = None
        curr_num = float('-inf')
        for idx in range(len(nums) - 1, -1, -1):
            if nums[idx] >= curr_num:
                curr_num = nums[idx]
            else:
                swap_target_idx = idx
                break

        if swap_target_idx == None:  # 전체가 역배열인 경우
            nums.sort()
            return

        swap_source_idx, swap_source_num = float('inf'), float('inf')
        for idx in range(swap_target_idx + 1, len(nums)):
            if nums[swap_target_idx] < nums[idx]:
                if swap_source_num > nums[idx]:
                    swap_source_idx, swap_source_num = idx, nums[idx]

        nums[swap_target_idx], nums[swap_source_idx] = nums[swap_source_idx], nums[swap_target_idx]  # swap

        nums[swap_target_idx + 1:len(nums)] = sorted(nums[swap_target_idx + 1:len(nums)])  # 나머지 부분은 정렬(최소 순열로 만들기)