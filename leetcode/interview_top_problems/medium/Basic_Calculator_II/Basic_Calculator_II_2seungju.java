class Solution {
    public int calculate(String s) {
        String[] split = s.split("");
        List<String> list = new ArrayList<>(Arrays.asList(split));

        for (int i = 0; i < list. size(); i++) {
            if (list.get(i).equals(" ")) {
                list.remove(i);
                i--;
            }
        }

        List<Integer> numList = new ArrayList<>();
        List<String> operation = new ArrayList<>();
        int index = 0;
        for (int i = 0; i < list.size(); i++) {
            String temp = list.get(i);
            if (temp.equals("*") || temp.equals("-") || temp.equals("/") || temp.equals("+")) {
                operation.add(temp);

                numList.add(getNum(list, index, i));

                index = i + 1;
            }
        }

        numList.add(getNum(list, index, list.size()));

        if (numList.size() == 1) {
            return numList.get(0);
        }

        for (int i = 0; i < operation.size(); i++) {
            if (operation.get(i).equals("/") || operation.get(i).equals("*")) {
                int num1 = numList.get(i);
                int num2 = numList.get(i + 1);

                if (operation.get(i).equals("/")) {
                    numList.add(i, num1 / num2);
                } else {
                    numList.add(i, num1 * num2);
                }

                numList.remove(i + 1);
                numList.remove(i + 1);
                operation.remove(i);

                i--;
            }
        }

        for (int i = 0; i < operation.size(); i++) {
            if (operation.get(i).equals("+") || operation.get(i).equals("-")) {
                int num1 = numList.get(i);
                int num2 = numList.get(i + 1);

                if (operation.get(i).equals("+")) {
                    numList.add(i, num1 + num2);
                } else {
                    numList.add(i, num1 - num2);
                }

                numList.remove(i + 1);
                numList.remove(i + 1);
                operation.remove(i);

                i--;
            }
        }

        return numList.get(0);
    }

    private int getNum(List<String> list, int start, int end) {
        StringBuilder stringNum = new StringBuilder();
        for (int i = start; i < end; i++) {
            stringNum.append(list.get(i));
        }

        return Integer.parseInt(stringNum.toString());
    }
}