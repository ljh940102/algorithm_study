class Solution:
    def calculate(self, s: str) -> int:
        if len(s) == 1:
            return int(s)

        s = f"{s.replace(' ', '')}+"  # 마지막 숫자 포함을 위헤 + append

        stack = []
        cur, operators = 0, "+"
        for ch in s:
            if ch.isdigit():
                cur = (cur * 10) + int(ch)
                continue

            if operators == '-':
                stack.append(-cur)
            elif operators == '+':
                stack.append(cur)
            elif operators == '*':
                stack.append(stack.pop() * cur)
            elif operators == '/':
                stack.append(int(stack.pop() / cur))

            cur = 0
            operators = ch  # 연산자를 뒤에서 바꾸는게 핵심
            # ch가 연산자일 때, 현재 연산자가 아니라 이전 연산자에 대한 계산이 수행 됨

        return sum(stack)