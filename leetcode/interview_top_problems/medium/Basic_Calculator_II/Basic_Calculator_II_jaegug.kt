class Solution {
    fun calculate(s: String): Int {
        var answer = 0L
        
        val valueStack = Stack<Long>()

        val chars: CharArray = s.replace(" ", "").toCharArray()

        var operation = '+'

        var curValue = 0L
        
        for (i in chars.indices) {
            if (chars[i] in '0'..'9') {
                curValue = curValue * 10 + (chars[i] - '0')
            }
            if (i == chars.size - 1
                || chars[i] !in '0'..'9'
            ) {
                if (operation == '+') {
                    valueStack.push(curValue)
                } else if (operation == '-') {
                    valueStack.push(-curValue)
                } else if (operation == '*') {
                    valueStack.push(valueStack.pop() * curValue)
                } else if (operation == '/') {
                    valueStack.push(valueStack.pop() / curValue)
                }
                operation = chars[i]
                curValue = 0L
            }
        }

        while (!valueStack.isEmpty()) {
            answer += valueStack.pop()
        }
        
        return answer.toInt()
    }
}
