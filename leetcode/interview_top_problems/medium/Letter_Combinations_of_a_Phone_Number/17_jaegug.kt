class Solution {
    
    private final val mapping = arrayOf("", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz");
    
    fun letterCombinations(digits: String): List<String> {
        
        var ret : MutableList<String> = ArrayList()
        
        if(digits.length == 0) return ret
        
        combination("", digits, 0, ret)
        
        return ret
    }

    public fun combination(prefix: String, digits: String, offset: Int, ret: MutableList<String>){
        
        if(offset >= digits.length) {
            ret.add(prefix)
            return
        }
        
        val letters = mapping[digits[offset] - '0'];
        
        for(i in 0..letters.length-1){
            combination(prefix + letters[i], digits, offset+1, ret);
        }
    }
}
