class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        if digits == "":
            return []
        num_dict = {}
        num_dict["2"] = "abc"
        num_dict["3"] = "def"
        num_dict["4"] = "ghi"
        num_dict["5"] = "jkl"
        num_dict["6"] = "mno"
        num_dict["7"] = "pqrs"        
        num_dict["8"] = "tuv"
        num_dict["9"] = "wxyz"
        result = []
        current = ""
        self.dfs(digits, 0, current, result, num_dict)
        return result
    def dfs(self,digits, index, current, result,num_dict):
        if index == len(digits):
            result.append(current)
            return
        for char in num_dict[digits[index]]:
            self.dfs(digits, index+1, current + char, result, num_dict)
        
