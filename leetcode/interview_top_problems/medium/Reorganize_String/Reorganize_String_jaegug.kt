class Solution {
    fun reorganizeString(s: String): String {
        
        val alphaMap = HashMap<Char, Int>()

        s.forEach {
            alphaMap[it] = alphaMap.getOrDefault(it, 0)+1
        }

        val priorityQueue = PriorityQueue<MutableMap.MutableEntry<Char, Int>> { (_, a), (_, b) -> b - a }

        alphaMap.entries.forEach {
            priorityQueue.add(it)
        }

        val sb = StringBuilder()
        while (!priorityQueue.isEmpty()) {
            val fstEntry = priorityQueue.poll()
            val sndEntry = priorityQueue.poll()
            val fstCnt = fstEntry.value
            if (sndEntry == null) { // 한가지 문자열이 남은 경우
                if (fstCnt > 1) { // 이 경우 반복 안하는 문자열을 만들 수가 없음
                    return ""
                }
                sb.append(fstEntry.key)
                break
            }
            sb.append(fstEntry.key)
            sb.append(sndEntry.key)
            if (fstCnt > 1) { //아직 알파벳이 두개 이상 남았으면 줄여서 다시 priorityQueue에 넣어준다
                fstEntry.setValue(fstCnt - 1)
                priorityQueue.add(fstEntry)
                val sndEntryCnt = sndEntry.value
                if (sndEntryCnt > 1) {
                    sndEntry.setValue(sndEntryCnt - 1)
                    priorityQueue.add(sndEntry)
                }
            }
        }
        return sb.toString()
    }
}
