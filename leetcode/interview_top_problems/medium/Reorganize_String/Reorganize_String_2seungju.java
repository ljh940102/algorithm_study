class Solution {
    public String reorganizeString(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            map.merge(s.charAt(i), 1, Integer::sum);
        }

        PriorityQueue<Map.Entry<Character, Integer>> queue = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            queue.offer(entry);
        }

        StringBuilder result = new StringBuilder();
        while (!queue.isEmpty()) {
            Map.Entry<Character, Integer> firstEntry = queue.poll();

            int firstValue = firstEntry.getValue();
            result.append(firstEntry.getKey());
            if (queue.isEmpty()) {
                if (firstValue > 1) {
                    return "";
                }
                break;
            }

            Map.Entry<Character, Integer> secondEntry = queue.poll();
            result.append(secondEntry.getKey());

            if (firstValue > 1) {
                firstEntry.setValue(firstValue - 1);
                queue.offer(firstEntry);

                if (secondEntry.getValue() > 1) {
                    secondEntry.setValue(secondEntry.getValue() - 1);
                    queue.offer(secondEntry);
                }
            }
        }

        return result.toString();
    }
}