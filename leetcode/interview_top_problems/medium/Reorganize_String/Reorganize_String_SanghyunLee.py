from collections import Counter, deque
from heapq import heappop, heappush


class Solution:
    def reorganizeString(self, s: str) -> str:
        if not s:
            return ""

        # max heap based on freq
        heap = []

        # minus freq is used to build max heap
        for key, freq in Counter(s).items():
            heappush(heap, [-freq,key])

        result = []

        # to prevent consecutive insertion, keep prev (previsouly appended value)
        # do not push prev to heap yet (to prevent consecutive insertion)
        prev = heappop(heap)
        result.append(prev[1])
        prev[0] += 1

        while heap:
            # get current root of heap (prev is not exist in heap)
            # do not push curr to heap yet (to prevent consecutive insertion)
            curr = heappop(heap)
            result.append(curr[1])
            curr[0] += 1

            # after curr has appended to result, prev can be pushed again
            # if prev has remaining freq -> push again (else skip)
            if prev[0] < 0:
                heappush(heap, prev)

            # update prev (previouly appended value)
            prev = curr

        if len(result) == len(s):
            return "".join(result)
        else:
            return ""


# print(Solution().reorganizeString("eeeeeabcd"))
# print(Solution().reorganizeString("eeeeeaabb"))
