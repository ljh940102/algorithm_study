import heapq
class Solution:
    def reorganizeString(self, s: str) -> str:
        nodeDict = {}
        for letter in s:
            node = (-1, letter)
            if letter not in nodeDict:
                nodeDict[letter] = node
            else:
                node = nodeDict[letter]
                node = (node[0] - 1, letter)
                nodeDict[letter] = node
        heapList = []
        for node in nodeDict.values():
            heapq.heappush(heapList, node)
        result = ""
        beforeNode = (0, "")
        beforeLetter = ""
        while heapList:
            num, letter = heapq.heappop(heapList)
            if num != 0:
                result = "".join((result,letter))
                if beforeNode != (0, ""):
                    heapq.heappush(heapList, beforeNode)
                beforeNode = (num + 1, letter)
        if beforeNode[0] != 0:
            return ""
        return result
        