from collections import defaultdict
class Solution:
    def deleteAndEarn(self, nums: List[int]) -> int:
        memo = [0] * (max(nums) + 1)
        for num in nums:
            memo[num] += num
        curr = 0
        prev = 0
        for num in memo:
            prev, curr = curr, max(curr,prev+num)
        return curr
        
            