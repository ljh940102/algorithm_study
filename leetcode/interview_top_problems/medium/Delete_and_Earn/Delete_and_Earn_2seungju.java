class Solution {
    public static int deleteAndEarn(int[] nums) {
        int[] counts = new int[10001];
        for (int num : nums) {
            counts[num]++;
        }

        for (int i = 2; i < counts.length; i++) {
            counts[i] = Math.max(counts[i] * i + counts[i - 2], counts[i - 1]);
        }

        return counts[10000];
    }
}