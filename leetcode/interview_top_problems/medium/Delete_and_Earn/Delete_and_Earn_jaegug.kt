class Solution {
    fun deleteAndEarn(nums: IntArray): Int {
        
        when(nums.size){
            0 -> return 0
            1 -> return nums[0]
        }            
        
        val numCount = Array(10001, {0})
        val sum = Array(10001, {0})
        
        var max = 0
        
        nums.forEach { 
            numCount[it]++        
            if (it > max) {            
                max = it        
            }    
        }

        sum[0] = numCount[0]
        sum[1] = numCount[1]  
        
        for (i in 2 until max+1) {
            sum[i] = Math.max(sum[i-2] + numCount[i]*i, sum[i - 1])    
        }
        
        return sum[max]
    }
}
