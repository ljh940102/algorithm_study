from collections import Counter


class Solution:
    def deleteAndEarn(self, nums: List[int]) -> int:
        sum_list = [0] * 10002  # idx까지의 얻을 수 있는 최대 point 저장

        for num in nums:  # idx에 해당하는 point로 변환
            sum_list[num] += num

        for idx in range(2, len(sum_list)):  # idx가 1까지는 계산할 필요 X
            sum_list[idx] = max(sum_list[idx - 1], sum_list[idx - 2] + sum_list[
                idx])  # idx의 최대 포인트는 idx-2의 포인트럴 얻는 것 또는 해당 idx를 skip한 값(idx-1의 값) 중 max 값이다.

        return sum_list[10001];

