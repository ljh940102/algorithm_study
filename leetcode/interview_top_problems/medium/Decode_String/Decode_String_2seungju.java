class Solution {
    public String decodeString(String s) {
        int start = 0;
        int left = 0;
        int number = 0;
        int index = 0;
        while (index < s.length()) {
            char temp = s.charAt(index);
            if (temp <= 57) {
                start = index;

                if (s.charAt(index + 1) <= 57) {
                    if (s.charAt(index + 2) <= 57) {
                        number = Integer.parseInt(s.substring(index, index + 3));
                        index += 2;
                    } else {
                        number = Integer.parseInt(s.substring(index, index + 2));
                        index += 1;
                    }

                } else {
                    number = Integer.parseInt(s.substring(index, index + 1));
                }

            } else if (temp == '[') {
                left = index;
            } else if (temp == ']') {
                String text = s.substring(left + 1, index);
                StringBuilder loopText = new StringBuilder();
                for (int j = 0; j < number; j++) {
                    for (int k = 0; k < text.length(); k++) {
                        if (text.charAt(k) != '[' && text.charAt(k) != ']' && text.charAt(k) > 57) {
                            loopText.append(text.charAt(k));
                        }
                    }
                }

                s = s.substring(0, start) + loopText.toString() + s.substring(index + 1);

                left = 0;
                number = 0;
                start = 0;
                index = -1;

            }

            index++;
        }

        return s;
    }
}