from collections import deque


class Solution:
    def decodeString(self, s: str) -> str:
        stack = list()
        for ch in s:
            if ch != "]":
                stack.append(ch)

            else:  # ] 등장 -> [이 나올떄 까지 pop
                popped_list = deque()

                cur = stack.pop()
                while cur != "[":
                    popped_list.appendleft(cur)
                    cur = stack.pop()

                # [ 앞에 등장하는 숫자 추출
                cur = stack.pop()
                cur_digit = ""
                while cur.isdigit():
                    cur_digit = cur + cur_digit

                    if not stack:
                        break

                    cur = stack.pop()

                if not cur.isdigit():
                    stack.append(cur)

                target_str = "".join(popped_list) * int(cur_digit)
                [stack.append(target_ch) for target_ch in target_str]

            print(stack)

        return "".join(stack)