class Solution:
    def decodeString(self, s: str) -> str:
        bracketStack =[[]]
        turnBracket = False
        numStack = []
        tempGroup = []
        numCon = False
        for letter in s:
            if letter.isdigit():
                temp = letter
                if numCon:
                    temp = numStack.pop()
                    temp += letter
                if not numCon:
                    bracketStack.append([])
                numStack.append(temp)
                numCon = True
            elif letter == "[":
                turnBracket = True
                numCon= False
            elif letter == "]":
                turnBracket = False
                tempNum, tempGro = numStack.pop(),bracketStack.pop()
                tempResult = []
                tempNum = int(tempNum)
                for i in range(tempNum):
                    for val in tempGro:
                        tempResult.append(val)
                if bracketStack:
                    tempNum1 = bracketStack.pop()
                    tempNum1 += tempResult
                    tempResult = tempNum1
                bracketStack.append(tempResult)
            else:
                temp = bracketStack.pop()
                temp.append(letter)
                bracketStack.append(temp)
        result= ""
        for group in bracketStack:
            result = result.join(group)
        return result
        
            
            
            
