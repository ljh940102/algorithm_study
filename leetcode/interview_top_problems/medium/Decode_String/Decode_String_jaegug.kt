class Solution {
    fun decodeString(s: String): String {
        
        var charStack: Stack<Char> = Stack()
        var numStack: Stack<Int> = Stack()
        var num = ""
        
        s.forEach { c ->
            if (c.isDigit()) { //숫자가 몇자리인지 알 수 없기 때문에 '['를 만났을 때 numStack에 push
                num += c
            } else if (c === '[') {
                numStack.push(num.toInt())
                charStack.push(c)
                num = ""
            } else if (c === ']') {
                var inSquare = ""
                
                while (!charStack.empty()) { //'['를 만날 때까지 charStack에 저장된 값을 inSquare에 더해준다.
                    if (charStack.peek() === '['){
                        charStack.pop()
                        break    
                    } 
                    inSquare += charStack.pop()
                }
                for (times in 0 until numStack.pop()) {
                    for (idx in inSquare.length - 1 downTo 0) { //stack에 저장된 값이기 때문에 역순으로 가져와야 한다.
                        charStack.push(inSquare[idx])
                    }
                }
            } else {
                charStack.push(c)
            }
        }
        
        var ans = ""
        
        while (!charStack.empty()) {
            ans += charStack.pop()
        }
        
        return ans.reversed() //stack에 저장된 값이니까 reverse해서 반환
    }
}
