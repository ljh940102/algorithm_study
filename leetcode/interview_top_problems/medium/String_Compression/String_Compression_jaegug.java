class Solution {
    public int compress(char[] chars) {
        
        if(chars.length == 0){
            return 0;
        }
        
        List<Character> compress = new ArrayList<>();
        compress.add(chars[0]);
        int groupStrt = 0;
        int groupLength = 0;
        
        for(int i = 0; i < chars.length; i++){
            if(chars[i] != compress.get(compress.size()-1)){
                groupLength = i - groupStrt;
                addDigit(compress, groupLength);
                compress.add(chars[i]);
                groupStrt = i;
            }
        }
        groupLength = chars.length - groupStrt;
        addDigit(compress, groupLength);
        
        int size = 0;
        for(char tmp : compress){
            chars[size++] = tmp;
        }
        
        return size;
    }
    
    private void addDigit(List<Character> target, int num){
        if(num > 1 && num < 10){
            target.add(Character.forDigit(num, 10));
        }else if(num >= 10){
            String numStr = Integer.toString(num);
            for (int i=0; i<numStr.length(); i++) {
                target.add(numStr.charAt(i));
            }
        }
    }
    
}
