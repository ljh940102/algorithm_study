class Solution {
    public int compress(char[] chars) {
        StringBuilder compressString = new StringBuilder();

        char preChar = chars[0];
        int count = 1;
        for (int i = 1; i < chars.length; i++) {
            if (preChar == chars[i]) {
                count++;
            } else {
                compressAppend(compressString, preChar, count);

                count = 1;
                preChar = chars[i];
            }
        }
        compressAppend(compressString, preChar, count);

        for(int i = 0; i < compressString.length(); i++) {
            chars[i] = compressString.charAt(i);
        }

        return compressString.length();
    }

    private void compressAppend(StringBuilder compressString, char c, int count) {
        compressString.append(c);
        if (count > 1) {
            compressString.append(count);
        }
    }
}