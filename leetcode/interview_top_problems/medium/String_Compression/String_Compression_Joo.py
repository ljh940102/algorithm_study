class Solution:
    def compress(self, chars: List[str]) -> int:
        rtn = ""
        curr_cnt, curr_char = 0, chars[0]
        for char in chars:
            if curr_char == char:
                curr_char = char
                curr_cnt += 1
            else:
                if curr_cnt == 1:
                    curr_cnt = ""

                rtn = rtn + curr_char + str(curr_cnt)  # f stinrg 왜 안됨?
                curr_char = char
                curr_cnt = 1

        if curr_cnt == 1:
            curr_cnt = ""

        rtn = rtn + curr_char + str(curr_cnt)
        
        modified = [ch for ch in rtn]

        for idx in range(len(modified)):
            if idx < len(chars):
                chars[idx] = modified[idx]
            else:
                chars.append(modified[idx])

        return len(rtn)


