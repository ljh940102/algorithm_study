from string import ascii_uppercase
from collections import deque
class Solution:
    def numDecodings(self, s: str) -> int:
        word_dict = {}
        for i in range(26):
            word_dict[str(i+1)] = 1
        result = 0
        memo = [0] * (len(s) + 1)
        
        memo[0] = 1
        
        if s[0] == "0":
            memo[1] = 0
        else:
            memo[1] = 1
            

        for i in range(2, len(memo)):
            if s[i-1] in word_dict:
                memo[i] = memo[i - 1]
            if s[i-2:i] in word_dict:
                memo[i] += memo[i - 2]
            
        return memo[len(s)]
