class Solution:

    valid = None
    dp = None

    def numDecodings(self, s: str) -> int:
        self.valid = set(map(str, range(1, 27)))
        self.dp = dict()
        return self.get_decode_way(s)

    def get_decode_way(self, s: str) -> int:
        if s in self.dp:
            return self.dp[s]
        if len(s) == 0:
            return 0
        if len(s) == 1:
            if s in self.valid:
                self.dp[s] = 1
                return 1
            else:
                return 0

        answer = 0

        # 전체가 한 덩어리
        if s in self.valid:
            answer += 1

        # 마지막 한글자 분리하는 경우
        if s[-1] in self.valid:
            answer += self.get_decode_way(s[:-1])

        # 마지막 두글자 분리하는 경우
        if len(s) >= 3 and s[-2:] in self.valid:
            answer += self.get_decode_way(s[:-2])

        # dp에 기록
        self.dp[s] = answer
        return answer


"""
print(Solution().numDecodings("123"))
print(Solution().numDecodings("226"))
print(Solution().numDecodings("220"))
print(Solution().numDecodings("06"))
print(Solution().numDecodings("1201234"))
print(Solution().numDecodings("1234"))
"""
