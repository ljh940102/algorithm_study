class Solution {
    public int numDecodings(String s) {
        int[] dp = new int[s.length() + 1];
        dp[1] = s.charAt(0) == '0' ? 0 : 1;

        for (int i = 2; i <= s.length(); i++) {
            int lastOneNumber = Integer.parseInt(s.substring(i - 1, i));
            if (lastOneNumber != 0) {
                dp[i] = dp[i - 1];
            }

            int lastTenNumber = Integer.parseInt(s.substring(i - 2, i));
            if (lastTenNumber >= 10 && lastTenNumber <= 26) {
                dp[i] += (i >= 3) ? dp[i - 2] : 1;
            }
        }

        return dp[s.length()];
    }
}