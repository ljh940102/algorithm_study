class Solution:
    def numDecodings(self, s: str) -> int:
        dp = [0] * (len(s) + 1)  # dp idx은 s[idx-1]까지 해독 가능한 최대 개수 의미!
        dp[0] = 1
        if s[0] == "0":
            return 0
        else:
            dp[1] = 1

        for idx in range(2, len(dp)):
            if s[idx - 1] != "0":  # 기본적으로 0만 아니면 바로 이전 값 승계
                dp[idx] = dp[idx - 1]

            if 10 <= int(s[idx - 2:idx]) <= 26:  # 두자리 표현 가능한 경우 가지수 추가
                dp[idx] += dp[idx - 2]

        return dp[-1]