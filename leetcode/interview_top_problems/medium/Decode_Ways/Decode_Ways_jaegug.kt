class Solution {
    fun numDecodings(s: String): Int {
        val dp = LongArray(s.length)
        
        when(s[0]) {
            '0' -> dp[0] = 0
            else -> dp[0] = 1
        }

        for(i in 1 until s.length) {
            
            val units = s[i] - '0'
            
            if (units != 0) {
                dp[i] += dp[i - 1]
            }

            val tens = (s[i - 1] - '0') * 10 + units
            if (tens in 10..26) {
                dp[i] =  (dp[i] + if (i >= 2) dp[i - 2] else 1)
            }
        }

        return dp[s.length-1].toInt()
    }
}
