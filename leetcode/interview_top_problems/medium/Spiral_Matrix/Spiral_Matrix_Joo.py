class Solution:
    def set_init_state(self, matrix):
        """새로운 객체를 만들었어야 했는데, solutions객체에 우겨넣다보니 __init__을 쓸 수 없어서 별개 함수 선언"""
        self.rtn_path = []
        self.visited = set()
        self.coordinate = [0, 0]
        self.direct = 0
        self.move_R, self.move_C = [0, 1, 0, -1], [1, 0, -1, 0]  # east -> south -> right -> north

        self.N, self.M = len(matrix[0]), len(matrix)
        self.matrix = matrix

        # 출발칸 방문 처리 및 정답 path에 추가
        self.visited.add((0, 0))
        self.rtn_path.append(self.get_value_from_matrix())

    def turn_direct(self):
        """방향전환 수행"""
        self.direct = (self.direct + 1) % 4

    def get_value_from_matrix(self):
        """matrix에서 value 추출"""
        return self.matrix[self.coordinate[0]][self.coordinate[1]]

    def get_forward_coordinate(self):
        """현재 방향에서 한칸 이동한 좌표 산출"""
        return self.coordinate[0] + self.move_R[self.direct], self.coordinate[1] + self.move_C[self.direct]

    def can_go_forward(self):
        """현재 방향 이동가능 여부"""
        target_r, target_c = self.get_forward_coordinate()

        if target_r < 0 or target_r >= self.M:
            return False

        if target_c < 0 or target_c >= self.N:
            return False

        if (target_r, target_c) in self.visited:
            return False

        return True

    def go_straight(self):
        """작선 이동"""
        while self.can_go_forward():
            # 좌표이동
            self.coordinate[0], self.coordinate[1] = self.get_forward_coordinate()

            # return value에 추가
            self.rtn_path.append(self.get_value_from_matrix())

            # 방문여부 기록
            self.visited.add(tuple(self.coordinate))

    def move_spiral(self):
        """나선형 탐색 시작"""
        is_first_step = True
        while self.can_go_forward() or is_first_step:  # 가장 첫 step은 앞으로 나아갈 수 없어도 수행(회전 가능성이 있으므로)
            is_first_step = False
            self.go_straight()
            self.turn_direct()

    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        """starter. 원래는 여기에서 새로만든 객체를 선언하고 move_spiral 호출했어야 했음"""
        self.set_init_state(matrix)

        self.move_spiral()

        return self.rtn_path
