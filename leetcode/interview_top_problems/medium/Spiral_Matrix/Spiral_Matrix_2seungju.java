class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;

        boolean[][] visited = new boolean[m][n];

        int x = 0;
        int y = 0;
        visited[x][y] = true;

        String[] direction = {"R", "D", "L", "U"};
        int index = 0;

        List<Integer> result = new ArrayList<>();
        result.add(matrix[y][x]);

        while (result.size() < m * n) {
            index = index % 4;

            switch (direction[index]) {
                case "R":
                    x = x + 1;
                    while (isValid(x, y, m, n) && !visited[y][x]) {
                        result.add(matrix[y][x]);
                        visited[y][x] = true;
                        x = x + 1;
                    }
                    x = x - 1;

                    break;

                case "D":
                    y = y + 1;
                    while (isValid(x, y, m, n) && !visited[y][x]) {
                        result.add(matrix[y][x]);
                        visited[y][x] = true;
                        y = y + 1;
                    }
                    y = y - 1;

                    break;

                case "L":
                    x = x - 1;
                    while (isValid(x, y, m, n) && !visited[y][x]) {
                        result.add(matrix[y][x]);
                        visited[y][x] = true;
                        x = x - 1;
                    }
                    x = x + 1;

                    break;

                case "U":
                    y = y - 1;
                    while (isValid(x, y, m, n) && !visited[y][x]) {
                        result.add(matrix[y][x]);
                        visited[y][x] = true;
                        y = y - 1;
                    }
                    y = y + 1;

                    break;
            }

            index++;
        }

        return result;
    }

    private boolean isValid(int x, int y, int m, int n) {
        return m > y && n > x && x >= 0 && y >= 0;
    }
}