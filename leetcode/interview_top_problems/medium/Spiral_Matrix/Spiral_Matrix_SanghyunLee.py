from typing import List


class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        r_max = len(matrix)
        c_max = len(matrix[0])

        visited = [[False for _ in range(c_max)] for _ in range(r_max)]
        visit_cnt = 0

        r = 0
        c = 0
        d = 0

        dr = [0, 1, 0, -1]
        dc = [1, 0, -1, 0]

        answer = []

        while visit_cnt < r_max * c_max:
            answer.append(matrix[r][c])
            visited[r][c] = True
            visit_cnt += 1

            r_next = r + dr[d]
            c_next = c + dc[d]
            if r_next < 0 or r_next >= r_max or c_next < 0 or c_next >= c_max or visited[r_next][c_next]:
                d = (d + 1) % 4
                r_next = r + dr[d]
                c_next = c + dc[d]

            r = r_next
            c = c_next

        return answer


# print(Solution().spiralOrder(matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]))