class Solution {

    List<Integer> wList;
	int max;
	Random generator;

	public Solution(int[] w) {
		wList = new ArrayList<Integer>();

		for(int i = 0; i < w.length; i++) {
			this.max += w[i];
			if(w[i] >= 1){
                for(int j = 0; j < w[i]; j++){
                    wList.add(i);
                }
            }
        }
        
		generator = new Random();
	}

	public int pickIndex() {
        return wList.get(generator.nextInt(max));
	}
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(w);
 * int param_1 = obj.pickIndex();
 */
