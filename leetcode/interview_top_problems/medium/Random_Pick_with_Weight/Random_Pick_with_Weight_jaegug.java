class Solution {
	int[] summedWt;
	int upBound;
	Random generator;

	public Solution(int[] w) {
		summedWt = new int[w.length];
		summedWt[0] = w[0];

		for (int i = 0; i < w.length; i++) {
			this.upBound += w[i];
			if (i >= 1){//binary search를 위해서 weight를 더해나감(1, 4, 5 -> 1, 5(1+4), 10(5+5))
				summedWt[i] = w[i] + summedWt[i - 1];
			} 
		}

		generator = new Random();
	}

	public int pickIndex() {
		// 1과 최대값 사이의 key를 랜덤하게 생성한 후
		int key = generator.nextInt(upBound) + 1;

		/*  summedWt 배열을 binary search하여 해당 key가 속한 범위를 찾음
		 ** arr = [0, 1, 2]
		 ** w = [1, 4, 5]
		 ** summedWt = [1, 5, 10]
		 ** key = 7
		 ** key는 5(summedWt[1])보다 크고 10(summedWt[2])보다 작으므로, summedWt[2]에 속한다.
		 ** 그러므로 해당 배열의 인덱스, 즉, arr[2]의 값인 2를 리턴한다.
		 */ 
		return biSearch(key);
	}

	public int biSearch(int key) {
		int lo = 0;
		int hi = summedWt.length - 1;
		int mid = lo + (hi - lo) / 2;

		while (lo < hi) {
			// If the key belongs to the current bucket
			if (summedWt[mid] == key || (summedWt[mid] > key && mid - 1 >= 0 && summedWt[mid - 1] < key)) 
				break;

			// If the key belongs to the bucket on the right
			else if (summedWt[mid] < key && summedWt[mid + 1] >= key) {
				mid = mid + 1;
				break;
			}

			// Search in the right half by updating the lo index
			else if (summedWt[mid] < key) lo = mid + 1;

			// Search in the left half by updating the hi index
			else hi = mid - 1;

			// Compute the new mid
			mid = lo + (hi - lo) / 2;
		}

		return mid;
	}
}

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(w);
 * int param_1 = obj.pickIndex();
 */
