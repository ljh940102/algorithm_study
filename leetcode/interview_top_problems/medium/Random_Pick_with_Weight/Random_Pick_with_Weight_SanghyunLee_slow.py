from collections import Counter
from random import choices
from typing import List


class Solution:

    def __init__(self, w: List[int]):
        self.weight = w
        self.nums = list(range(len(w)))

    def pickIndex(self) -> int:
        return choices(population=self.nums, weights=self.weight, k=1)[0]


"""
ctr = Counter()
for _ in range(1000):
    num = Solution([1,3]).pickIndex()
    ctr[num] += 1
print(ctr)
"""
