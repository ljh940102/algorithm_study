class Solution {
    private Random random = new Random();
    private int[] array;
    private int sum = 0;

    public Solution(int[] w) {
        array = new int[w.length];
        for (int i = 0; i < w.length; i++) {
            sum += w[i];
            array[i] = sum;
        }
    }

    public int pickIndex() {
        int randomNumber = random.nextInt(sum) + 1;
        int target = Arrays.binarySearch(array, randomNumber);
        return target < 0 ? (target + 1) * -1 : target;
    }
}
