from bisect import bisect_left


class Solution:

    def __init__(self, w: List[int]):
        self.idx_range = list()

        curr_idx = 0
        for weight in w:  # 인덱스의 부분합을 계산해서 저장 -> 인덱스에 해당하는 최대 접근 인덱스 값이 저장됨
            self.idx_range.append(curr_idx + weight - 1)
            curr_idx += weight

    def pickIndex(self) -> int:
        if self.idx_range[-1] == 0:
            return self.idx_range[-1]
        else:
            target_idx = random.randint(0, self.idx_range[-1])

        return bisect_left(self.idx_range, target_idx)