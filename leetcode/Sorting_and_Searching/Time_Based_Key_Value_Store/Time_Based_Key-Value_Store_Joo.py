from bisect import bisect_left

class TimeMap:

    def __init__(self):
        self.timestamp_store = dict()

    def set(self, key: str, value: str, timestamp: int) -> None:
        if key not in self.timestamp_store:
            self.timestamp_store[key] = [[timestamp], [value]]  # 이걸 list말고 OrderDict로 쓰면 더 효율적임
        else:
            timestamp_list, value_list = self.timestamp_store[key]
            timestamp_list.append(timestamp)
            value_list.append(value)

    def get(self, key: str, timestamp: int) -> str:
        if key not in self.timestamp_store:
            return ""

        timestamp_list, value_list = self.timestamp_store[key]

        lower_bound = bisect_left(timestamp_list, timestamp)  # lower bound

        if lower_bound == len(timestamp_list):  # input timestamp가 가장 큰 값인 경우
            return value_list[-1]

        if timestamp_list[lower_bound] == timestamp:  # input timestamp에 해당하는 값이 있는 경우
            return value_list[lower_bound]

        if lower_bound != 0:  # lower bound이므로
            lower_bound -= 1

        if timestamp_list[lower_bound] <= timestamp:
            return value_list[lower_bound]
        else:
            return ""