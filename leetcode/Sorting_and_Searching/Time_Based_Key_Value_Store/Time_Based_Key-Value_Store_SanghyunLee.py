from collections import defaultdict
from bisect import bisect


class TimeMap:

    # dict[key][ts][val] 형태의 dict 1개만 쓰려다가
    # dict_values 객체를 list로 형변환하는 시간 때문에 시간 초과 발생
    # 아래와 같이 list 타입 value를 갖는 2개의 dict 사용하면 해결
    # 이것이 가능한 이유는 문제 조건에 따라 시간 순으로 입력되기 때문에
    # values가 항상 순서대로 정렬되어 있음
    def __init__(self):
        self.key_to_ts = defaultdict(list)
        self.key_to_val = defaultdict(list)

    def set(self, key: str, value: str, timestamp: int) -> None:
        self.key_to_ts[key].append(timestamp)
        self.key_to_val[key].append(value)

    def get(self, key: str, timestamp: int) -> str:
        # bisect_right 사용해야 함 (bisect == bisect_right)
        # bisect_left 사용 시 get하려는 ts가 이미 존재하면 틀림 
        idx = bisect(self.key_to_ts[key], timestamp)
        # no prev ts
        if idx == 0:
            return ""
        else:
            return self.key_to_val[key][idx - 1]


"""
ops = ["set", "get", "get", "set", "get", "get"]
params = [
	["foo", "bar", 1],
	["foo", 1],
	["foo", 3],
	["foo", "bar2", 4],
	["foo", 4],
	["foo", 5]
]

ops = ["set","set","get","get","get","get","get"]
params = [
	["love","high",10],
	["love","low",20],
	["love",5],
	["love",10],
	["love",15],
	["love",20],
	["love",25]
]

time_map = TimeMap()
for op, param in zip(ops, params):
    print(f"{op}, {param}")
    if op == "set":
        key, val, ts = param
        time_map.set(key, val, ts)
    else:
        key, ts = param
        print(time_map.get(key, ts))
"""
