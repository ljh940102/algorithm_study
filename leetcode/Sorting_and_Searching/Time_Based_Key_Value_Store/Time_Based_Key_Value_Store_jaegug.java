class TimeMap {

    HashMap<String, ArrayList<String>> map;

    /** Initialize your data structure here. */
    public TimeMap() {
        map = new HashMap<String, ArrayList<String>>();
    }
    
    public void set(String key, String value, int timestamp) {

        if (!map.containsKey(key)) {

            ArrayList<String> list = new ArrayList<String>();
            list.add(Integer.toString(timestamp));
            list.add(value);
            map.put(key,list);

        } else {
            
            map.get(key).add(Integer.toString(timestamp));
            map.get(key).add(value);
        }
    }
    
    public String get(String key, int timestamp) {

        if (map.containsKey(key)) {

            for (int i=map.get(key).size()-2; i>=0;  i-=2) {

                if (Integer.parseInt(map.get(key).get(i)) <= timestamp){
                    return map.get(key).get(i+1);  
                }
            }
        }

        return "";
    }
}

/**
 * Your TimeMap object will be instantiated and called as such:
 * TimeMap obj = new TimeMap();
 * obj.set(key,value,timestamp);
 * String param_2 = obj.get(key,timestamp);
 */
