"""
이진탐색?..
true면 앞쪽, false면 뒤쪽 탐색
"""
class Solution:
    def firstBadVersion(self, n):
        inspect_v = (1+n)//2
        checked = dict()
        front, back = 1, n
        while checked.get(inspect_v) is None:  # 방문한적이 있는 노트를 방문하면 종료
            if isBadVersion(inspect_v):  # true -> 앞쪽 탐색
                back = inspect_v
                checked[inspect_v] = True
                inspect_v = (front+inspect_v) // 2
            else:  # false -> 뒤쪽 탐색
                front = inspect_v
                checked[inspect_v] = False
                inspect_v = (back + inspect_v) // 2

        if checked[inspect_v]:
            return inspect_v
        else:
            return inspect_v+1
