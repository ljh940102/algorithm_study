/*
 * The isBadVersion API is defined in the parent class VersionControl.
 * boolean isBadVersion(int version);
 */

class VersionControl {
	int bad_start = 10;
	int called = 0;

	boolean isBadVersion(int version) {
		called += 1;
		return version >= bad_start ? true : false;
	}
}

class Solution extends VersionControl {

	public int firstBadVersion(int n) {

		int start = 1;
		int end = n;

		while (start < end) {

			//int mid = (start + end) / 2;			// Overflow 발생
			int mid = start + ((end - start) / 2);	// Overflow 미발생

			if (isBadVersion((int)mid)) {
				end = mid;
			}
			else {
				start = mid + 1;
			}
		}
		return start;
	}
}
