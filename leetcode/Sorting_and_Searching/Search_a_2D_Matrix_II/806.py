class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        for row in matrix:
            length = len(row)

            if target == row[length - 1]:
                return True

            if target < row[length - 1]:
                row_length = len(row)
                for idx in range(row_length - 1, -1, -1):
                    if target == row[idx]:
                        return True

                    if target > row[idx]:
                        continue

        return False