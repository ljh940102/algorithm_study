from collections import deque, Counter
from typing import List
import heapq


from collections import deque
class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        m = len(matrix)
        n = len(matrix[0])
        visited = [[False] * n for _ in range(m)]
        current_queue = deque()
        current_queue.append((matrix[0][0], 0, 0))
        while current_queue:
            check_target, current_m, current_n = current_queue.pop()
            if check_target == target:
                return True
            elif check_target < target:
                if current_m < m - 2:
                    if not visited[current_m+1][current_n]:
                        visited[current_m+1][current_n] = True
                        current_queue.append((matrix[current_m+1][current_n], current_m+1, current_n))
                if current_n < n - 2:
                    if not visited[current_m][current_n+1]:
                        visited[current_m][current_n+1] = True
                        current_queue.append((matrix[current_m][current_n+1], current_m, current_n+1))
        return False


matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]]
target = 20
matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]]
target = 5
c = Solution()
print(c.searchMatrix(matrix, target))
