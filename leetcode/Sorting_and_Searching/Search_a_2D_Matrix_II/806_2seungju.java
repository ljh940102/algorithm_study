/*
행,열 모두 오름차순이기 때문에,
row[first] <= target <= row[last]일때 값이 존재해야함.
 */

class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        for (int[] nums : matrix) {
            int first = nums[0];
            int last = nums[matrix[0].length - 1];

            // row[first] <= target <= row[last]
            if (first <= target && last >= target) {
                if (findTarget(nums, target)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean findTarget(int[] nums, int target) {
        for (int num : nums) {
            if (num == target) {
                return true;
            }
        }

        return false;
    }
}