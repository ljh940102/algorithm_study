class Solution {
    public int search(int[] nums, int target) {
        int answer = binarySearch(nums, target, 0, nums.length - 1);
        return answer;
    }
    
    private int binarySearch(int[] nums, int target, int left, int right){        
        int center = (left + right) / 2;
        if(left > right){
            return -1;
        }
        else if(nums[center] == target){
            return center;
        }
        if(nums[left] <= nums[center]){
            if(nums[left] <= target && nums[center] > target){
                return binarySearch(nums, target, left, center - 1);
            }
            else{
                return binarySearch(nums, target, center+1, right);
            }    
        }else{
            if(nums[center] < target && nums[right] >= target){
                return binarySearch(nums, target, center + 1, right);
            }
            else{
                return binarySearch(nums, target, left, center-1);
            }  
        }
    }
}
