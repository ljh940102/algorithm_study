class Solution {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int num : nums) {
            map.merge(num, 1, Integer::sum);
        }

        List<Integer> list = new ArrayList<>();

        while (k > 0) {
            int max = 0;
            for (int key : map.keySet()) {
                if (map.get(max) == null) {
                    max = key;
                } else if (map.get(key) > map.get(max)) {
                    max = key;
                }
            }

            list.add(max);
            map.put(max, 0);
            k--;
        }

        return list.stream().mapToInt(i -> i).toArray();
    }
}