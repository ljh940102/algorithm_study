import heapq
from collections import Counter

"""
counter에 most common이 있네...
"""


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        counter = Counter(nums)  # O(n)

        targets = []
        for num, count in counter.items():  # O(n)?
            targets.append((-count, num))

        heapq.heapify(targets)  # O(n)

        rtn = []
        for _ in range(k):
            rtn.append(heapq.heappop(targets)[1])  # O(log n)

        return rtn