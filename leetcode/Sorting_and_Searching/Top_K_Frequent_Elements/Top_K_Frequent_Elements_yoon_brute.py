class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        result = sorted(nums, key=lambda x: -x)
        return result[k-1]