class Solution {
    public int[] topKFrequent(int[] nums, int k) {

        Map<Integer, Integer> countMap = new HashMap<>();
        for (int num : nums) {
            countMap.put(num, countMap.getOrDefault(num, 0) + 1);
        }

        Queue<Integer> topK = 
            new PriorityQueue<>((a, b) -> countMap.get(a) - countMap.get(b)); // 큰 쪽이 우선순위 ^

        for (Integer num : countMap.keySet()) {
            topK.add(num);

            if (topK.size() > k) {
                topK.poll();
            }
        }

        // list to array
        int[] answer = new int[k];
        
        for (int i = 0; i < k; i++) {
            answer[i] = topK.poll();
        }

        return answer;
    }
}
