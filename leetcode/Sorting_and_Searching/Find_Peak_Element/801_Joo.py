class Solution:
    def findPeakElement(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return 0

        if len(nums) == 2:
            if nums[0] > nums[1]:
                return 0
            else:
                return 1

        for target_idx in range(1, len(nums) - 1):
            if nums[target_idx - 1] < nums[target_idx] and nums[target_idx + 1] < nums[target_idx]:
                return target_idx
        return nums.index(max(nums))