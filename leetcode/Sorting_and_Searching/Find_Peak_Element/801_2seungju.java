class Solution {
    public int findPeakElement(int[] nums) {
//         int max = 0;
//         for (int i = 0; i < nums.length; i++) {
//             if (nums[i] > nums[max]) {
//                 max = i;
//             }
//         }

//         return max;
        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            int median = (left + right) / 2;
            if (nums[median] > nums[median + 1]){
                right = median;

            } else {
                left = median + 1;
            }
        }

        return left;
    }
}