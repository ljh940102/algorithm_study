class Solution {
    public int findPeakElement(int[] nums) {
        int bfr = Integer.MIN_VALUE;
        for(int i = 0; i < nums.length; i++){
            if(nums[i]<bfr){
                return i-1;
            }
            bfr=nums[i];
        }
        return nums.length-1;
    }
}
