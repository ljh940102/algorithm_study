
"""
1. 일단 직관적인 방법은 앞에서부터 비교하면서 insert하는 방식 -> insert가 상당히 느림
2. 시간복잡도와 공간복잡도를 교환한다면 m크기의 empty 배열을 선언해서 앞부터채우기 -> 어쨌든 추가공간이 필요
3. 뒤에서부터 비교하면서 넣어주기 -> 시간, 공간 모두 최적일듯

"""
class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        update_idx = m + n - 1
        num1_idx, num2_idx = m - 1, n - 1

        while num1_idx >= 0 and num2_idx >= 0:  # 하나의 list가 모두 사용될 때까지 반복
            if nums1[num1_idx] >= nums2[num2_idx]:
                nums1[update_idx] = nums1[num1_idx]
                num1_idx -= 1
            else:
                nums1[update_idx] = nums2[num2_idx]
                num2_idx -= 1
            update_idx -= 1

        if num1_idx < 0:  # nums1의 모든 수가 사용된 경우
            while num2_idx >= 0:
                nums1[update_idx] = nums2[num2_idx]
                num2_idx -= 1
                update_idx -= 1
        else:  # nums2의 모든 수가 사용된 경우
            while num1_idx >= 0:
                nums1[update_idx] = nums1[num1_idx]
                num1_idx -= 1
                update_idx -= 1
