class Solution {
	public void merge(int[] nums1, int m, int[] nums2, int n) {
		int merge_idx = m + n - 1;
		int idx1 = m - 1;
		int idx2 = n - 1;
		
		while (merge_idx >= 0 && idx1 >= 0 && idx2 >= 0) {
			
			int val1 = nums1[idx1];
			int val2 = nums2[idx2];
			
			if (val1 > val2) {
				nums1[merge_idx] = val1;
				merge_idx--;
				idx1--;
			}
			else if (val1 < val2) {
				nums1[merge_idx] = val2;
				merge_idx--;
				idx2--;
			}
			else { // val1 == val2
				nums1[merge_idx] = val1;
				merge_idx--;
				idx1--;
				nums1[merge_idx] = val2;
				merge_idx--;
				idx2--;
			}
		}
		// num1 소진 후 nums2만 남은 경우
		while (idx2 >= 0) {
			nums1[merge_idx] = nums2[idx2];
			merge_idx--;
			idx2--;
		}
		// num2 소진 후 num1만 남았다면 별도로 처리해주지 않아도 됨 (이미 num1에 들어가 있으니까)
	}
}
