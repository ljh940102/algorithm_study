class Solution {
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals, Comparator.comparing((int[] item) -> item[0]));

        List<List<Integer>> mergeList = new ArrayList<>();
        List<Integer> tmp;
        
        for(int i = 0; i<intervals.length; i++){
            if(mergeList.size() > 0 && mergeList.get(mergeList.size()-1).get(1) >= intervals[i][0]){
                tmp = mergeList.get(mergeList.size()-1);
                tmp.set(1, intervals[i][1] > tmp.get(1) ? intervals[i][1] : tmp.get(1));
                mergeList.set(mergeList.size()-1, tmp);
            }else{
                tmp = new ArrayList<>();
                tmp.add(intervals[i][0]);
                tmp.add(intervals[i][1]);
                mergeList.add(tmp);
            }
        }
        
        int[][] answer = new int[mergeList.size()][2];
        for(int i = 0; i<mergeList.size(); i++){
            answer[i][0] = mergeList.get(i).get(0);            
            answer[i][1] = mergeList.get(i).get(1);
        }
        
        return answer;
    }
}
