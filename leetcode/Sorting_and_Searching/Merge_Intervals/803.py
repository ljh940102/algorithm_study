class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        rtn = []
        intervals.sort(key=lambda x: x[0])

        start, end = intervals[0]
        for interval in intervals[1:]:
            if end >= interval[0]:  # 겹치는 케이스
                if interval[1] > end:
                    end = interval[1]
            else:
                rtn.append([start, end])
                start, end = interval
        rtn.append([start, end])

        return rtn