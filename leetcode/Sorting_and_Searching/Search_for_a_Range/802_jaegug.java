class Solution {
    public int[] searchRange(int[] nums, int target) {
        
        int first = findFirst(nums, target);
        
        int last = (first == -1)?-1:findLast(nums, target);
        
        return new int[]{first, last};
    }
    
    private int findFirst(int[] nums, int target) {
        int begin = 0;
        int end = nums.length-1;
        
        while (begin <= end) {
            int mid = (begin + end) / 2;
            
            if (nums[mid] == target) {
                if (mid == begin || nums[mid - 1] != target) {
                    return mid;
                }
                end = mid - 1;
            } else if (nums[mid] > target) {
                end = mid - 1;
            } else if (nums[mid] < target) {
                begin = mid + 1;
            }
        }
        return -1;
    }
    
    private int findLast(int[] nums, int target) {
        int begin = 0;
        int end = nums.length-1;
        
        while (begin <= end) {
            int mid = (begin + end) / 2;
            
            if (nums[mid] == target) {
                if (mid == end || nums[mid + 1] != target) {
                    return mid;
                }
                begin = mid + 1;
            } else if (nums[mid] > target) {
                end = mid - 1;
            } else if (nums[mid] < target) {
                begin = mid + 1;
            }
        }
        return -1;
    }
}
