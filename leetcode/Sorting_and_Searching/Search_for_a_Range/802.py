class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        rtn = []
        found = False

        if not nums:
            return [-1, -1]

        for idx, num in enumerate(nums):
            if not found and num == target:
                found = True
                rtn.append(idx)

            if found and num != target:
                rtn.append(idx - 1)
                break

            if found and idx == len(nums) - 1:
                rtn.append(idx)

        if len(rtn) == 1:
            rtn.append(rtn[0])

        if len(rtn) != 2:
            rtn = [-1, -1]

        return rtn
