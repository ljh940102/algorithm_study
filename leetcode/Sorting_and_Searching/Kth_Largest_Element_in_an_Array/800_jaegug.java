class Solution {
    public int findKthLargest(int[] nums, int k) {
        
        Queue<Integer> kst = 
            new PriorityQueue<>((a, b) -> a - b); // 큰 쪽이 우선순위 ^

        for (Integer num : nums) {
            kst.add(num);

            if (kst.size() > k) {
                kst.poll();
            }
        }
        
        return kst.poll();
    }
}
