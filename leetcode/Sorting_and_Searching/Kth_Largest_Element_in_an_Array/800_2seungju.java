class Solution {
    public int findKthLargest(int[] nums, int k) {
//         Arrays.sort(nums);
        quickSort(nums, 0, nums.length - 1);

        return nums[nums.length - k];
    }

    public void quickSort(int[] nums, int left, int right) {
        if (left > right) {
            return;
        }

        int median = (left + right) / 2;
        swap(nums, left, median);

        int i = left;
        int j = right;

        while (i < j) {
            while (nums[left] < nums[j]) {
                j--;
            }

            while (i < j && nums[left] >= nums[i]) {
                i++;
            }
            swap(nums, i, j);
        }

        swap(nums, left, i);

        quickSort(nums, left, i - 1);
        quickSort(nums, i + 1, right);
    }

    private void swap(int[] nums, int left, int right) {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }
}