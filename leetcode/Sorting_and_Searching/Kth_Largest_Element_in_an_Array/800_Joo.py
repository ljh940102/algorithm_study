from collections import Counter


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        counter = Counter(nums)

        curr_k = k
        counter_list = list(counter.items())
        counter_list.sort(key=lambda x: x[0], reverse=True)

        for num, count in counter_list:
            if count < curr_k:
                curr_k -= count
            else:
                return num