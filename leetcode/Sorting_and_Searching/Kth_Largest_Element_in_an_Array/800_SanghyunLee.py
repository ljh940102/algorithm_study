from typing import List
from heapq import heappush, heappushpop


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        min_heap = []
        # 길이 k 짜리 최소 힙 생성
        for i in range(k):
            heappush(min_heap, nums[i])
        # 길이 k로 유지하면서 최솟값 pop 반복
        # 결국 상위 k개의 원소가 남고 root에는 그중 최솟값인 k번째 값이 존재
        for i in range(k, len(nums)):
            heappushpop(min_heap, nums[i])
        return min_heap[0]


"""
nums = [3, 2, 3, 1, 2, 4, 5, 5, 6]
k = 4
print(Solution().findKthLargest(nums, k))
"""
