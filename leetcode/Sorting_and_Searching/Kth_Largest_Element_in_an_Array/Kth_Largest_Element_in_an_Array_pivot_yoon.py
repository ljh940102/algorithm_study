class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        pivot = -1
        group_start = 0
        group_end = len(nums)
        started = False
        left = 0
        right = len(nums) - 1
        while left <= right:
            if pivot == -1:
                pivot = 0
            if pivot == k - 1:
                return pivot
            for num_index in range(left, right + 1):
                num = nums[num_index]
                if num <= nums[pivot]:
                    nums[num_index], nums[left] = nums[left], nums[num_index]
                    left += 1
                else:
                    nums[num_index], nums[right] = nums[right], nums[num_index]
                    right -= 1
            pivot = left + 1
            
        return pivot
    
