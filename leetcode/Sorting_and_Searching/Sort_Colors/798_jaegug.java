class Solution {
    public void sortColors(int[] nums) {
        if(nums==null||nums.length<2){
            return;
        }
 
        int red = 0;
        int blue = 0;
        int i;
        int max = nums.length;
        
        for(i=0; i<max; i++){
            if(nums[i]==0){
                red++;
            }else if(nums[i]==1){
                blue++;
            }
        }
        
        for(i=0; i<max; i++){
            if(i < red){
                nums[i] = 0;    
            }else if(i < red + blue){
                nums[i] = 1;
            }else{
                nums[i] = 2;
            }
        }
    }
}
