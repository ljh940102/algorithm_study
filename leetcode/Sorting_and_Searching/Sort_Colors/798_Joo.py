class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        constant space 1이면 selection이나 insertion?
        -> selection 구현 연습 겸~
        """
        for target_idx in range(len(nums)):
            min_idx = target_idx
            for start_idx in range(target_idx + 1, len(nums)):
                if nums[min_idx] > nums[start_idx]:
                    min_idx = start_idx

            nums[target_idx], nums[min_idx] = nums[min_idx], nums[target_idx]