class Solution:
    def isPowerOfThree(self, n: int) -> bool:
        if n == 1:
            return True

        if n <= 0:
            return False

        rtn = True
        val, left = n, n
        while left > 0:
            val, mod = divmod(val, 3)

            if mod != 0:
                rtn = False
                break
            if val <= 1:
                break
        return rtn