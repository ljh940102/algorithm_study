class Solution:
    def my_divide(self, dividend, divisor):
        if dividend == divisor:
            return 1

        quotient = 0
        while dividend >= divisor:
            shift = 0
            while dividend >= divisor << shift + 1:
                shift += 1

            dividend -= divisor << shift
            quotient += 1 << shift

        return quotient

    def divide(self, dividend: int, divisor: int) -> int:
        if dividend == 0:
            return 0

        MAX, MIN = 2147483647, -2147483648
        # Check for overflow
        if dividend == MAX:
            if divisor == 1:
                return MAX
            if divisor == -1:
                return MIN

        if dividend == MIN:
            if divisor == 1:
                return MIN
            if divisor == -1:
                return MAX

        is_minus_dividend, is_minus_divisor = False, False
        if dividend < 0:
            is_minus_dividend = True
            dividend = abs(dividend)

        if divisor < 0:
            is_minus_divisor = True
            divisor = abs(divisor)

        is_minus = is_minus_dividend ^ is_minus_divisor

        val = self.my_divide(dividend, divisor)

        rtn = val
        if is_minus:
            rtn -= (val + val)

        return rtn

