class Solution {
    public int divide(int dividend, int divisor) {
        // Check for overflow
        if (divisor == 0 || (dividend == Integer.MIN_VALUE && divisor == -1)) {
            return Integer.MAX_VALUE;
        }
        // 부호 구하기
        int sign = (dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0) ? -1 : 1;

        int answer = 0;
        
        long absDividend = (dividend > 0) ? (long) dividend : (long) dividend * (-1);
        long absDivisor = (divisor > 0) ? (long) divisor : (long) divisor * (-1);
        
        while (absDividend >= absDivisor) {
            
            int shift = 0;
            // divisor * 2^shift 가 dividend 보다 작은 조건으로 loop
            while (absDividend >= (absDivisor << shift + 1)) { 
                shift++;
            }
            
            // answer += 2^shift
            answer += 1 << shift;
            
            // divisor * 2^shift
            absDividend -= absDivisor << shift;
        }
        return sign * answer;
    }
}
