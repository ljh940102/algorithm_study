from typing import List


class Solution:
	def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
		points = sorted(points, key=lambda point:(point[0] ** 2) + (point[1] ** 2))
		return points[:k]


# print(Solution().kClosest(points=[[1,3],[-2,2]], k=1))
# print(Solution().kClosest(points=[[3,3],[5,-1],[-2,4]], k=2))
