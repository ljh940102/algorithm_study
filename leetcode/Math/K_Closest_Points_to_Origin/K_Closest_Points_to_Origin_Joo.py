import heapq

"""
heap의 복잡도는 삽입, 삭제 모두 logN
그래서 힙정렬의 복잡도는 NlogN
"""
class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        rtn = []

        priority_queue = []
        for x, y in points:
            priority = (x * x) + (y * y)

            heapq.heappush(priority_queue, (priority, (x, y)))

        for _ in range(k):
            _, elem = heapq.heappop(priority_queue)
            rtn.append(elem)

        return rtn