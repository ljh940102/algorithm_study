import math
import heapq
class Solution:
    def calc_dist(self, point):
        return math.sqrt(point[0] ** 2 + point[1] ** 2)
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        result = []
        temp = []
        for point in points:
            dist = self.calc_dist(point)
            heapq.heappush(temp,(-dist, point[0], point[1]))
            if len(temp) > k:
                heapq.heappop(temp)
        for dist, x, y in temp:
            result.append((x,y))
        return result