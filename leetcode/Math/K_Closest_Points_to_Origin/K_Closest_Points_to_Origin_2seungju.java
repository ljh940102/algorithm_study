class Solution {
    public int[][] kClosest(int[][] points, int k) {
        Map<Integer, List<int[]>> map = new HashMap<>();

        for (int[] point : points) {
            int key = (int) (Math.pow(point[0], 2) + Math.pow(point[1], 2));
            if (map.get(key) == null) {
                map.put(key, new ArrayList<>(List.of(point)));
            } else {
                List<int[]> value = map.get(key);
                value.add(point);

                map.put(key, value);
            }
        }

        List<Integer> list = new ArrayList<>(map.keySet());
        Collections.sort(list);

        int[][] result = new int[k][2];
        int index = 0;
        for (Integer key : list) {
            if (index >= k) {
                break;
            }
            for(int[] point : map.get(key)) {
                if (index >= k) {
                    break;
                }
                result[index++] = point;
            }
        }

        return result;
    }
}