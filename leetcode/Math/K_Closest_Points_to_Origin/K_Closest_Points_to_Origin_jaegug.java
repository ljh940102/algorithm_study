import java.util.*;

class Solution {
    public int[][] kClosest(int[][] points, int K) {
        PriorityQueue<Point> queue = new PriorityQueue<>();
        
        for(int i=0; i<points.length; i++)
            queue.add(new Point(points[i][0],points[i][1]));
        
        int[][] answer = new int[K][2];
        
        for(int i=0; i<K; i++){
            Point p = queue.poll();
            answer[i][0] = p.x;
            answer[i][1] = p.y;
        }
        
        return answer;
    }
}

class Point implements Comparable<Point>{
    public int x;
    public int y;
    public int distance;
    
    public Point(int x,int y){
        this.x = x;
        this.y = y;
        this.distance = x*x + y*y;
    }
    
    @Override
    public int compareTo(Point other){
        return Integer.compare(distance,other.distance);
    }
}
