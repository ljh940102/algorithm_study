class Solution:
    def fizzBuzz(self, n: int) -> List[str]:
        rtn = []
        for num in range(1, n+1):
            if num % 3 == 0:
                if num % 5 == 0:
                    rtn.append("FizzBuzz")
                else:
                    rtn.append("Fizz")
            elif num% 5 == 0:
                rtn.append("Buzz")
            else:
                rtn.append(str(num))

        return rtn
