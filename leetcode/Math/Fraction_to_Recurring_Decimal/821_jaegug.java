class Solution {
    public String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) {
            return "0";
        }
        StringBuilder fraction = new StringBuilder();
        
        if (numerator < 0 ^ denominator < 0) { // 부호
            fraction.append("-");
        }

        // Convert to Long or else abs(-2147483648) overflows
        long dividend = numerator > 0 ? (long) numerator : (long) numerator * (-1);
        long divisor = denominator > 0 ? (long) denominator : (long) denominator * (-1);

        fraction.append(String.valueOf(dividend / divisor));

        long remainder = dividend % divisor;

        if (remainder == 0) {
            return fraction.toString();
        }

        fraction.append(".");

        //소수점 아래는 나머지를 10으로 곱한 다음 divisor로 나누는 작업을 반복
        Map<Long, Integer> map = new HashMap<>();
        while (remainder != 0) {
            if (map.containsKey(remainder)) { //똑같은 나머지가 나오면 이후는 반복됨
                fraction.insert(map.get(remainder), "(");
                fraction.append(")");
                break;
            }
            map.put(remainder, fraction.length());
            remainder *= 10;
            fraction.append(String.valueOf(remainder / divisor));
            remainder %= divisor;
        }
        
        return fraction.toString();
    }
}
