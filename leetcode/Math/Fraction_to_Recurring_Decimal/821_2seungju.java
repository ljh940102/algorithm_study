class Solution {
    public String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) {
            return "0";
        }

        StringBuilder decimal = new StringBuilder();
        Map<Long, Integer> dictionary = new HashMap<>();

        boolean isInfinite = false;
        int value = 0;
        // overflow
        long key = numerator % denominator * 10;
        while (key != 0) {
            if (dictionary.get(key) == null) {
                dictionary.put(key, value++);
            } else {
                isInfinite = true;
                break;
            }

            decimal.append(Math.abs(key / denominator));

            key = key % denominator * 10;
        }

        int numeratorSign = numerator < 0 ? -1 : 1;
        int denominatorSign = denominator < 0 ? -1 : 1;
        String sign = (numeratorSign * denominatorSign) > 0 ? "" : "-";

        String result =  sign + Math.abs((long) numerator / denominator) + (decimal.length() > 0 ? "." : "");
        return result + (isInfinite ? decimal.insert(dictionary.get(key), "(").append(")") : decimal);
    }
}