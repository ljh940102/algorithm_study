class Solution:
    def fractionToDecimal(self, numerator: int, denominator: int) -> str:
        n, remainder = divmod(abs(numerator), abs(denominator))
        if numerator * denominator < 0:
            sign = "-"
        else:
            sign = ''  # 부호 결정

        result = [sign + str(n), '.']

        remainders = {}

        while remainder > 0 and remainder not in remainders:
            remainders[remainder] = len(result)
            n, remainder = divmod(remainder * 10, abs(denominator))  # 반복되지는 확인
            result.append(str(n))

        if remainder in remainders:
            idx = remainders[remainder]  # 반복이 된다면 () 추가
            result.insert(idx, '(')
            result.append(')')

        return ''.join(result).rstrip(".")