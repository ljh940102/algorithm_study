class Solution:
    def fractionToDecimal(self, numerator: int, denominator: int) -> str:
        if numerator == 0:
            return '0'

        result = []
        if numerator < 0 and denominator > 0 or numerator >= 0 and denominator < 0:
            result.append('-')

        numerator, denominator = abs(numerator), abs(denominator)

        result.append(str(numerator // denominator))

        remainder = numerator % denominator

        if remainder == 0: return ''.join(result)
        result.append('.')

        # 334 / 333을 예제로 생각
        d = {}
        while remainder != 0:
            if remainder in d:  # 단순히 숫자의 반복 체크가 아니라, 같은 값을 나눈적이 있는지 체크
                result.insert(d[remainder], '(')
                result.append(')')
                return ''.join(result)

            d[remainder] = len(result)  # 나눈 값에 대한 나머지가 result의 어디에 있는지 인덱스 기록

            remainder *= 10
            result += str(remainder // denominator)  # 몫 붙여주기
            remainder = remainder % denominator  # 나머지 갱신

        return ''.join(result)