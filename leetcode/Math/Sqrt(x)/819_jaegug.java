class Solution {
    public int mySqrt(int x) {
        if (x == 0) return 0;

        int left = 1, right = x;

        while (left <= right) {
            int mid = left + (right - left)/2;

            if (mid <= x / mid && (mid + 1) > x / (mid + 1)) {
                return mid;
            } else if (mid <= x / mid) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return left;
        
        /*
        int i = 0;
        while(true){
            if(i*i == x){
                return i;
            }else if(i*i > x){
                return i-1;
            }
            
            i++;
        }
        */
    }
}
