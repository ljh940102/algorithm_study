class Solution:
    def mySqrt(self, x: int) -> int:
        if x == 0:
            return 0
        if x == 1 or x == 2:
            return 1

        start, end = 0, x // 2
        while start < end:
            mid = (start + end) // 2
            if int(mid ** 2) == x:
                return mid
            elif int(mid ** 2) < x:
                start = mid + 1
            elif int(mid ** 2) >= x:
                end = mid

        if end ** 2 <= x:
            return end
        else:
            return end - 1
