class Solution {
    public int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        int left = 1;
        int right = x / 2;

        int result = 1;
        while (left <= right) {
            int median = left + (right - left) / 2;

            if (median <= x / median) {
                left = median + 1;
                result = median;

            } else {
                right = median - 1;
            }
        }
        return result;
        // return (int) Math.floor(Math.sqrt(x));
    }
}