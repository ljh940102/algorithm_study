class Solution {
    public int titleToNumber(String columnTitle) {
        
        int num = 0;
        int length = columnTitle.length()-1; 
        
        for(int i = length ; i>=0; i--){
        
            int tmp = columnTitle.charAt(i) - 64;
            
            for(int j = 0; j < length-i; j++){
            
                tmp *= 26;    
            }
            
            num += tmp;
        }
        
        return num;
    }
}
