class Solution:
    def titleToNumber(self, columnTitle: str) -> int:
        """26진법이라고 생각"""
        rtn = 0
        length = len(columnTitle)
        for idx, char in enumerate(columnTitle):
            degree = length - idx - 1
            num = ord(char) - 64
            rtn += (num * (26 ** degree))

        return rtn