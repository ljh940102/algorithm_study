class Solution {
    public int titleToNumber(String columnTitle) {
        char[] chars = columnTitle.toCharArray();
        int sum = 0;
        for (int i = 0; i < chars.length; i++) {
            sum += Math.pow(26, chars.length - (i + 1)) * (chars[i] - 64);
        }

        return sum;
    }
}