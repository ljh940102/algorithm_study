import string

class Solution:
    def titleToNumber(self, columnTitle: str) -> int:
        result = 0
        alpha_dict = self.num_dict()
        for i,letter in enumerate(columnTitle):
            result += (26 ** (len(columnTitle) - i - 1)) * alpha_dict[letter] 
        return result
    def num_dict(self):
        result_dict = {}
        letters = list(string.ascii_uppercase)
        for i,letter in enumerate(letters):
            result_dict[letter] = i + 1
        return result_dict