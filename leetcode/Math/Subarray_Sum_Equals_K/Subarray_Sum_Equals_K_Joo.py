from collections import defaultdict


class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        """
        subsum 이용
        sum(i, j) = sum(0,j) - sum(0, i) = k 찾기
        sum(0, j) - k = sum(0, i)인 대상 찾기 로 변환 가능
        단, i는 여러개 일 수 있음에 유의
        """
        rtn = 0

        sub_sum_dict = defaultdict(int)
        sub_sum_dict[0] += 1  # 시작점

        curr_sum = 0
        for num in nums:
            curr_sum += num

            # 무조건 현재 idx보다 뒤에 있는 원소들과 subsum을 체크하므로, 즉시 확인
            # subsum을 만들고 curr_sum - k를 확인하면 반례 존재 -> k=3, nums=[1 2 1 -4 1], subsum=[1 3 4 0 1]
            if (curr_sum - k) in sub_sum_dict:
                rtn += sub_sum_dict[(curr_sum - k)]

            sub_sum_dict[curr_sum] += 1

        return rtn

