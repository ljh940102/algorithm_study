from collections import defaultdict
class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        count, cur_sum = 0, 0
        sum_dict = defaultdict(int)
        sum_dict[0] = 1
        for i in range(len(nums)):
            cur_sum += nums[i]
            if cur_sum - k in sum_dict:
                count += sum_dict[cur_sum - k]
            sum_dict[cur_sum] = sum_dict[cur_sum] + 1
        return count
