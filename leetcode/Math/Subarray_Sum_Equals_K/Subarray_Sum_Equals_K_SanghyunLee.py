from collections import defaultdict
from typing import List


class Solution:
	def subarraySum(self, nums: List[int], k: int) -> int:
		answer = 0

		# 누적값 출현 횟수 기록
		cumul_ctr = defaultdict(int)
		# 길이 1짜리 subarray 찾기 위해 누적값 0 추가
		cumul_ctr[0] = 1
		
		# 누적값
		cumul = 0
		
		for num in nums:
			# 누적값 구하기
			cumul += num
			
			# 만약 이전에 cumul값 중 (cumul-k)와 같은 값이 존재 했다면,
			# 해당 위치부터 현재 위치까지가 합 k짜리 subarray
			answer += cumul_ctr[cumul-k]
			
			cumul_ctr[cumul] += 1

		return answer


Solution().subarraySum(nums = [1,1,1], k = 2)
Solution().subarraySum(nums = [1,2,3], k = 3)