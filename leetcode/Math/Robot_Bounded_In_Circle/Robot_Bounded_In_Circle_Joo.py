class Solution:
    def isRobotBounded(self, instructions: str) -> bool:
        """
        0,0 북쪽을 바라보고 있음

        1. 명령어에 해당하는 행동 구현
        2. 좌표, 방향에 따른 중복여부 체크
        """
        move_r, move_c = [-1, 0, 1, 0], [0, 1, 0, -1]  # 상 우 하 좌
        curr_direc, curr_loc = 0, [0, 0]
        visited = set()

        cnt, limit = 0, len(instructions) * 100
        rtn = False
        while not rtn:
            if cnt > limit:
                break
            for instruction in instructions:
                if instruction == "G":
                    curr_loc[0], curr_loc[1] = curr_loc[0] + move_r[curr_direc], curr_loc[1] + move_c[curr_direc]
                    visit_info = (curr_loc[0], curr_loc[1], curr_direc)

                    if visit_info in visited:
                        rtn = True
                        break
                    else:
                        visited.add(visit_info)

                elif instruction == "L":
                    curr_direc = (curr_direc - 1) % 4
                elif instruction == "R":
                    curr_direc = (curr_direc + 1) % 4

        return rtn
