class Solution {
	enum Direction {
		Up(0,1,0),
		Right(1,0,1),
		Down(0,-1,2),
		Left(-1,0,3),
		EMPTY(0,0,-1);
		
		private int x;
		private int y;
		private int order;
		
		Direction(int x, int y, int order){
			this.x=x;
			this.y=y;
			this.order=order;
		}
		
		private int getOrder() {
			return order;
		}
		
		private static Direction findNext(char instruction, Direction dir){
			int nextOrder = findNextOrder(instruction, dir.getOrder());

			return Arrays.stream(Direction.values())
						.filter(target -> target.order == nextOrder)
						.findAny().orElse(EMPTY);
		}
	}

	public static int findNextOrder(char instruction, int order) {
		if(instruction == 'L') {			
			order = (order == 0)? 3 : order-1;
		}else if(instruction == 'R') {
			order = (order == 3)? 0 : order+1;
		}
		return order;
	}
    
	public boolean isRobotBounded(String instructions) {
		int x = 0;
		int y = 0;
		Direction dir = Direction.Right;

		for(int i = 0; i < 4; i++){
			for(int j = 0; j < instructions.length(); j++) {
				char instruction = instructions.charAt(j);
				if(instruction == 'G') {
					x+=dir.x;
					y+=dir.y;
				}else {
					dir = dir.findNext(instruction, dir);
				}
			}
			if(x==0 && y==0)
				return true;
		}
		return false;
	}
}
