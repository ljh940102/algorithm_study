class Solution:
    def isRobotBounded(self, instructions: str) -> bool:
        directions = [(0,1),(1,0),(0,-1),(-1,0)]
        dir_idx = 0
        conse = 0
        ans_vec = []
        current_loc = [0,0]
        for instruction in instructions:
            if instruction == "G":
                current_loc[0] += directions[dir_idx][0]
                current_loc[1] += directions[dir_idx][1]
            elif instruction == "L":
                if dir_idx == 0:
                    dir_idx = 3
                else:
                    dir_idx -= 1
            elif instruction == "R":
                if dir_idx == 3:
                    dir_idx = 0
                else:
                    dir_idx += 1        
        ans_vec = [current_loc, directions[dir_idx]]
        if current_loc == [0,0]:
            return True
        elif ans_vec[1][0] == 0 and ans_vec[1][1] == 1:
            return False  
        return True