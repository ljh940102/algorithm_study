import java.util.Arrays;

class Solution {
	public int countPrimes(int n) {

		if (n <= 2) {
			return 0;
		}

		// 초기에는 2 ~ n-1 범위의 모든 숫자(n-2개)가 Prime이라고 가정
		// 이후에 Non-Prime을 발견할 때마다 Prime 개수 1씩 차감
		// 2를 제외한 짝수는 무조건 소수가 아니라는 가정 추가하면 속도 향상 (but 헷갈림)
		int numOfPrime = n - 2;
		boolean[] isPrime = new boolean[n];
		Arrays.fill(isPrime, true);

		// 루트 n 미만의 소수로 나누어 떨어지지 않는 n 미만의 숫자는 소수이다 (법칙)
		// n 미만의 소수를 찾으려면 루트 n 미만의 모든 소수로 나누어 떨어지지 않는지 검사하면 됨
		for (int prime = 2; prime * prime < n; prime++) {
			if (!isPrime[prime]) {
				continue;
			}

			// prime은 루트 n 미만의 소수
			// prime의 배수들에 대해 Non-Prime 처리
			// prime에 대해 prime보다 작은 숫자(a)를 곱한 결과는 이미 a의 턴에서 수행했음
			// 그래서 prime의 제곱부터 검사 시작 (int multiple = prime * prime;)
			// 헷갈리면 multiple = prime * 2로 시작해도 됨 (조금 느릴 뿐)
			for (int multiple = prime * prime; multiple < n; multiple += prime) {
				// 이전까지 보지 못했던 새로운 Non-Prime 발견
				if (isPrime[multiple]) {
					isPrime[multiple] = false;
					numOfPrime--;
				}
			}
		}
		return numOfPrime;
	}
}
