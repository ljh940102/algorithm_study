class Solution {
    public int countPrimes(int n) {
        //에라토스테네스의 체
        boolean[] check = new boolean[n+1];
        int answer = 0;
        
        for (int i = 2; i < n; i++){
            if (check[i] == false){
                answer++;
            }   
            for (int j = i + i; j < n; j += i) {
                check[j] = true;
            }
        }
        return answer;
    }
}