import math

class Solution:

    dp = {}

    def countPrimes(self, n: int) -> int:
        rtn = 0
        for target in range(2, n):
            if self.dp.get(target) == True:
                rtn += 1
                continue
            elif self.dp.get(target) == False:
                continue

            if self.isPrime(target):
                self.dp[target] = True
                rtn += 1
            else:
                self.dp[target] = False

        return rtn

    def isPrime(self, n):
        if n == 2:
            return True

        criteria = int(math.sqrt(n))
        for divide in range(2, criteria+1):
            if n % divide == 0:
                return False

        return True
