class Solution {
    public int trailingZeroes(int n) {
        int count = 0;
        for (int i = 1; i <= n; i++) {
            if (i % 5 == 0) {
                count += findNumber(i);
            }
        }

        return count;
    }

    public int findNumber(int num) {
        int count = 0;
        while (num % 5 == 0) {
            num = num / 5;
            count++;
        }

        return count;
    }
}