class Solution {
    public int trailingZeroes(int n) {
        if(n==0) return 0;
        return n/5 + trailingZeroes(n/5);

        /* java 는 곧잘 integer.MAX_VALUE 초과 나서 안 돌아감
        int prod = 1;
        for(int i = 1; i <= n; i++){
            prod *= i;
            System.out.println(prod);
        }
        int count = 0;
        while(prod % 10 == 0){
            count++;
            prod /= 10;
        }
        return count;
        */
    }
}
