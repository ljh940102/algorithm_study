class Solution:
    def trailingZeroes(self, n: int) -> int:
        """
        뒤에 0의 개수는 결국 10이 몇번 곱해졌냐가 관건이다.
        그럼 팩토리얼에서 10이 몇개인지 세면되는데,
        5에다가 짝수를 곱해주면 결국 10이 생긴다. 그리고 짝수는 무조건 5의 배수보다 많다.
        그러므로 5의 배수의 개수가 10의 개수와 같다.
        그러므로 5의 배수의 개수가 0의 개수가 된다.
        """
        rtn = 0
        while n != 0:
            n = n // 5
            rtn += n
        return rtn
