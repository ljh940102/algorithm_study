class Solution {
    public boolean isHappy(int n) {
        String[] split = String.valueOf(n).split("");
        Set<Integer> resultSet = new HashSet<>();

        boolean isHappy = false;
        while (true) {
            int sum = 0;
            for (String s : split) {
                int temp = Integer.parseInt(s);
                sum += Math.pow(temp, 2);
            }

            if (sum == 1) {
                isHappy = true;
                break;
            }

            if (resultSet.contains(sum)) {
                break;
            }

            resultSet.add(sum);

            split = String.valueOf(sum).split("");
        }

        return isHappy;
    }
}