class Solution:
    def isHappy(self, n: int) -> bool:
        record = set()
        while n not in record:
            if n == 1:
                return True
            record.add(n)
            num = 0
            for each in str(n):
                num += int(each) ** 2
            n = num
        return False
                
