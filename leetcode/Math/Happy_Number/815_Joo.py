class Solution:
    def isHappy(self, n: int) -> bool:
        check_dup = set()

        target = n
        while target not in check_dup:
            check_dup.add(target)

            square_sum = 0
            digits = []
            while target != 0:
                quotient, left = divmod(target, 10)

                target = quotient
                square_sum += left * left

            if square_sum == 1:
                return True

            else:
                target = square_sum

        return False