class Solution {
    public boolean isHappy(int n) {
        Set<Integer> dupChk = new HashSet<>();
        while(dupChk.add(n)){
            int sum = 0;
            while(n > 0){
                int tmp = n%10;
                sum += tmp * tmp;
                n = n/10;
            }
            if(sum == 1){
                return true;
            }else{
                n = sum;
            }
        }
        return false;
    }
}
