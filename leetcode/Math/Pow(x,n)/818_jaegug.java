class Solution {
    public double myPow(double x, int n) {     
        if (n == 0) return 1;
        double half = myPow(x, n / 2);
        if (n % 2 == 0) return half * half;
        else if (n > 0) return half * half * x;
        else return half * half / x;
        
        /*
        double answer = 1;
        if(x==1){
            return 1;
        }else if(n>0){
            for(int i=0; i<n; i++){
                answer *= x; 
            }
        }else{
            for(int i=0; i>n; i--){
                answer /= x;
            }
        }
        return answer;
        */
    }
}
