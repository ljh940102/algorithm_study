class Solution {
    public double myPow(double x, int n) {
//        return Math.pow(x, n);

        if (n == 0) {
            return 1;
        }

        return n < 0 ? getNegative(x, n) : getPositive(x, n);
    }

    private double getPositive(double x, int n) {
        double result = x;
        while (n > 1) {
            result = result * x;

            n--;
        }

        return result;
    }

    private double getNegative(double x, int n) {
        double result = 1 / x;

        n *= -1;
        while (n > 1) {
            result = result / x;

            n--;
        }

        return result;
    }
}