class Solution:
    def calc_pow(self, num, repeat, dp):
        if repeat in dp:
            return dp[repeat]

        if repeat % 2 == 1:  # 홀수배인 경우
            val = self.calc_pow(num, repeat - 1, dp) * num

        else:  # 짝수배인 경우
            val = self.calc_pow(num, repeat // 2, dp) * self.calc_pow(num, repeat // 2, dp)

        dp[repeat] = val
        return val

    def myPow(self, x: float, n: int) -> float:

        if n == 0:
            return 1

        is_inverse = False
        rtn = 1
        if n < 0:
            repeat = abs(n)
            is_inverse = True
        else:
            repeat = n

        dp = {1: x}
        rtn = self.calc_pow(x, repeat, dp)

        if is_inverse:
            return 1 / rtn
        else:
            return rtn
