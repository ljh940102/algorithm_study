class Solution:
    def lengthOfLIS(self, nums: List[int]) -> int:
        dp = [0 for _ in range(len(nums))]

        rtn = 0
        for idx, num in enumerate(nums):
            dp[idx] = 1
            for reverse_idx in range(idx - 1, -1, -1):  # 현재 idx에서 -1씩하면서 increasing인지 검사
                if nums[reverse_idx] < num:
                    dp[idx] = max(dp[idx], dp[reverse_idx] + 1)

            rtn = max(rtn, dp[idx])

        return rtn