class Solution {
    public int lengthOfLIS(int[] nums) {
        int[] ascCnt = new int[nums.length];
        int maxNum = Integer.MIN_VALUE;
        
        for(int i = 0; i < nums.length; i++){
            ascCnt[i] = 1;
            for(int j = 0; j < i; j++){
                if(nums[i] > nums[j]){
                    ascCnt[i] = Math.max(ascCnt[j] + 1, ascCnt[i]);
                }
            }
            maxNum = Math.max(ascCnt[i], maxNum);
        }
        return maxNum;
    }
}
