class Solution {
    public int lengthOfLIS(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int[] dp = new int[nums.length];
        dp[0] = 1;

        int result = 1;
        for (int i = 1; i < dp.length; i++) {
            int count = 0;
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    count = Math.max(count, dp[j]);
                }
            }
            dp[i] = count + 1;
            result = Math.max(result, dp[i]);
        }
        return result;
    }

//     public int lengthOfLIS(int[] nums) {
//         return func(nums, Integer.MIN_VALUE, 0);
//     }

//     private int func(int[] nums, int num, int index) {
//         if (index >= nums.length) {
//             return 0;
//         }

//         int count = 0;
//         if (nums[index] > num) {
//             count = 1 + func(nums, nums[index], index + 1);
//         }

//         int nextCount = func(nums, num, index + 1);

//         return Math.max(count, nextCount);
//     }
}