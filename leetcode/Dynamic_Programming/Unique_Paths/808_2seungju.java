class Solution {

    /*
    a(m)이라는 수열이 있을때 a(m)의 n번째 요소는 a(m)의 n-1번째 요소 + a(m-1)의 n번째 요소이다.
     */

    public int uniquePaths(int m, int n) {
        // a(m-1) 수열
        int[] array = new int[n];
        for (int i = 0; i < m; i++) {
            // a(m) 수열
            int[] buffer = new int[n];
            for (int j = 0; j < n; j++) {
                if (j == 0) {
                    buffer[j] = 1;
                    continue;
                }

                buffer[j] = buffer[j - 1] + array[j];
            }

            // swap
            array = buffer;
        }

        return array[n - 1];
    }

    /*
    Time-out 재귀 방식
     */

    public int count;

    public int uniquePaths(int m, int n) {
        boolean[][] visited = new boolean[m][n];
        func(visited, 0, 0);

        return count;
    }

    private void func(boolean[][] visited, int a, int b) {
        if (a >= visited.length || b >= visited[0].length || visited[a][b]) {
            return;
        }

        if (a == visited.length -1 && b == visited[0].length - 1) {
            count++;
            return;
        }

        visited[a][b] = false;
        // 아래쪽
        func(visited, a + 1, b);
        // 오른쪽
        func(visited, a, b + 1);
    }
}