class Solution:
    def is_safe(self, r, c, m, n):
        if r < 0 or r >= m:
            return False

        if c < 0 or c >= n:
            return False

        return True

    def uniquePaths(self, m: int, n: int) -> int:
        path_map = [[0 for col in range(n)] for row in range(m)]
        path_map[0][0] = 1

        move_r, move_c = [-1, 0], [0, -1]  # 상 좌

        for idx_r in range(m):
            for idx_c in range(n):
                # 현재 idx의 왼쪽, 위쪽 값 가져다가 더해주기
                for idx in range(2):
                    target_r = idx_r + move_r[idx]
                    target_c = idx_c + move_c[idx]
                    if self.is_safe(target_r, target_c, m, n):
                        path_map[idx_r][idx_c] += path_map[target_r][target_c]

        return path_map[m - 1][n - 1]