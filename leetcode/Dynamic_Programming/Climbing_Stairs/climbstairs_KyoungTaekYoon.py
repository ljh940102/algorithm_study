from collections import defaultdict
class Solution:
    def climbStairs(self, n: int) -> int:
        stair=defaultdict(int)
        stair[0] = 0
        stair[1] = 1
        stair[2] = 2
        for way in range(3,n +1):
            stair[way] +=stair[way-2] + stair[way - 1]
        return stair[n]
