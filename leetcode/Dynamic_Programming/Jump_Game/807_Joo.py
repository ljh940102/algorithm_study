class Solution:
    def canJump(self, nums: List[int]) -> bool:
        max_idx = 0  # 현재 접근 가능한 최대 인덱스 저장

        for idx, num in enumerate(nums):

            if max_idx < idx:  # 최대인덱스를 넘어간 경우 -> 도달불가
                return False

            # 최대 인덱스 갱신
            max_idx = max(max_idx, idx + num)

            if max_idx >= len(nums) - 1:  # 도달가능
                return True

        return False