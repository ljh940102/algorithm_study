class Solution {
    public boolean canJump(int[] nums) {
        int max = 0;
        for(int i = 0; i < nums.length; i++) {
            // 점프시 현재 i가 점프거리보다 크다면 false
            if (i > max) {
                return false;
            }

            // 갈 수 있는 가장 먼 거리
            max = Math.max(max, nums[i] + i);
        }
        return true;
    }
}