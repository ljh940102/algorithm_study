from collections import deque
from typing import List


class Solution:
    def canJump(self, nums: List[int]) -> bool:
        visited = [False] * len(nums)
        queue = deque()
        queue.append(0)
        max = 1
        while queue:
            current_loc = queue.popleft()
            if nums[current_loc] + current_loc >= len(nums) - 1:
                return True
            for i in range(max, nums[current_loc] + current_loc + 1):
                if not visited[i]:
                    queue.append(i)
                    visited[i] = True
                    max = i
        return False
nums = [1,2,3]
nums = [1,1,1,0]
c = Solution()
print(c.canJump(nums))