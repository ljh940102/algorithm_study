class Solution {
    public int maxSubArray(int[] nums) {
        int max = nums[0];
        int sum = 0;
        for(int i=0; i<nums.length; i++){
            if(0 < sum+nums[i] && nums[i] <= sum+nums[i]){
                sum += nums[i];
                if(max<sum)
                    max = sum;
            }else{
                sum = nums[i];
                if(max<sum)
                    max = sum;
            }
        }
        return max;
    }
}