class Solution:
    #못 품. kadene의 알고리즘 참조. 즉 element 하나가 음수인거는 상관이 없고 그룹이 음수인지 확인
    def maxSubArray(self, nums: List[int]) -> int:
        for i in range(1, len(nums)):
            if nums[i-1] > 0:
                nums[i] += nums[i-1]
        return max(nums)