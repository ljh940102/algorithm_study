"""
부분합으로 해결한다.
단, 부분합이 0보다 작거나 같아진다면 0으로 초기화한다. -> 부분합이 음수라는 것은 그 부분까지 합하는건 마이너스라는 소리
마이너스가 되는 부분은 없는 셈 친다는 개념으로 0 만들어주기
"""
import sys
class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        rtn = -sys.maxsize
        subset = 0

        for target in nums:
            subset += target
            rtn = max(rtn,subset)

            if subset <= 0:
                subset = 0

        return rtn
