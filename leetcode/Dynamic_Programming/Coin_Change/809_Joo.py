import sys


class Solution:
    def coinChange(self, coins: List[int], amount: int) -> int:
        dp = [sys.maxsize for _ in range(amount + 1)]

        if amount == 0:
            return 0

        for coin in coins:
            if coin < len(dp):
                dp[coin] = 1

        for idx in range(amount + 1):
            for coin in coins:  # 현재 인덱스에서 coin만큼 전진한 값을 갱신한다.
                if idx + coin < len(dp):
                    dp[idx + coin] = min(dp[idx + coin], dp[idx] + 1)

        if dp[amount] == sys.maxsize:
            return -1

        return dp[amount]

