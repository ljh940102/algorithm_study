class Solution {
    public int coinChange(int[] coins, int amount) {
        int[] caseNum = new int[amount+1];
        Arrays.fill(caseNum, Integer.MAX_VALUE);

        caseNum[0] = 0;
        
        for(int i = 0; i <= amount; i++){
            for(int coin : coins){
                if(i-coin >= 0){
                    if(caseNum[i-coin] == Integer.MAX_VALUE) continue; // i-coin 에 기록된 값이 없는 경우
                    caseNum[i] = Math.min(caseNum[i-coin]+1, caseNum[i]); // i-coin 의 case 숫자 + 1을 기록
                }
            }
        }
        
        return caseNum[amount] == Integer.MAX_VALUE ? -1 : caseNum[amount];
    }
}
