from collections import defaultdict
class Solution:
    def rob(self, nums: List[int]) -> int:
        if not nums:
            return 0
        if len(nums) == 1:
            return nums[0]
        RobProfitTable = [0] * len(nums)
        RobProfitTable[0] = nums[0]
        RobProfitTable[1] = max(nums[0], nums[1])
        for i in range(2, len(nums)):
            RobProfitTable[i] = max(nums[i] + RobProfitTable[i-2], RobProfitTable[i-1])
        return RobProfitTable[-1]
