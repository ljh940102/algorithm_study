"""
1 2 3 4 5 6 7 8
i번째에 i번째까지의 최대값을 기록한다. 이때, i-2와 i-3의 최대값을 선택하면 된다. i-4번째는 고려할 필요가 없다. i-4는 i-2보다 무조건 작기 때문
이런식의 배열에서.. 6을 선택할 때 최대 값은, 3선택후 6이거나 4선택후 6이거나 둘중 하나이다...
"""

class Solution:
    def rob(self, nums: List[int]) -> int:

        len_nums = len(nums)
        if not nums:
            return 0
        elif len_nums < 3:
            return max(nums)
        elif len_nums == 3:
            return max(nums[0]+nums[2], nums[1])

        dp = [0 for _ in range(len_nums)]
        dp[0], dp[1], dp[2] = nums[0], nums[1], nums[0]+nums[2]

        for idx in range(3, len_nums):
            dp[idx] = nums[idx] + max(dp[idx-2], dp[idx-3])

        return max(dp)
