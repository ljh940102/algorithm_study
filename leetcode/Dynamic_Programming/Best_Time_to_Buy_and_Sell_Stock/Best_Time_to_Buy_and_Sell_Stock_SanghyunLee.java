class Solution {
	public int maxProfit(int[] prices) {
		if (prices.length == 0) {
			return 0;
		}
		// 지금까지 나왔던 최저 가격
		int min_price = prices[0];
		// 지금까지 발생 가능했던 최대 수익
		int max_profit = 0;

		for (int i = 1; i < prices.length; i++) {
			max_profit = Math.max(max_profit, prices[i] - min_price);
			min_price = Math.min(min_price, prices[i]);
		}
		return max_profit;
	}
}
