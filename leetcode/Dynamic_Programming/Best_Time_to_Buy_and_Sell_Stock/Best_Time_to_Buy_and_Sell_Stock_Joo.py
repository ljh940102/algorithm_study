"""
배열 순회하면서 min(그 시점에서 살 수 있었던 최저가)를 기록하고
앞으로 가면서 min값과 해당 시점의 가격의 차를 구함(수익)
이 수익의 최대값을 구하면 될 듯.
"""
import sys

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        min_price = sys.maxsize
        max_profit = 0

        for target in prices:
            min_price = min(min_price, target)
            max_profit = max(max_profit, target-min_price)

        return max_profit