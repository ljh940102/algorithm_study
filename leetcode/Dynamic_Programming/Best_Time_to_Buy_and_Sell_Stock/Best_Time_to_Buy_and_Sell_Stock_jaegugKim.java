class Solution {
    public int maxProfit(int[] prices) {
        if(prices.length == 0)
            return 0;
        int profit = 0;
        int max = prices[0];
        int min = prices[0];
        for(int i=1; i<prices.length; i++){
            if(prices[i]<min){
                min = prices[i];
                max = prices[i];
            }
            if(prices[i]>max){
                max = prices[i];
                if(profit < max - min)
                    profit = max - min;
            }
        }
        if(profit<0)
            return 0;
        return profit;
    }
}