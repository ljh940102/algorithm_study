class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        if not prices:
            return 0
        buy = prices[0]
        maxprofit = 0
        for index in range(1,len(prices)):
            maxprofit = max(maxprofit,prices[index] - buy)
            if buy > prices[index]:
                buy = prices[index]
        return maxprofit
