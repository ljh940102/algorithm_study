from collections import defaultdict
"""
inorder하되, 레벨을 물고 재귀로 타고 들어가서 list에 넣고
짝수번쨰 list reverse하먄 될 듯.
"""
class Solution:
    def inorder(self, node, depth):
        if not node:
            return

        if node.left:
            self.inorder(node.left, depth + 1)

        self.node_dict[depth].append(node.val)

        if node.right:
            self.inorder(node.right, depth + 1)

    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        if not root:
            return []

        self.node_dict = defaultdict(list)
        self.inorder(root, 0)

        length = len(self.node_dict.keys())

        rtn = []
        for d in range(length):
            if d % 2 == 1:
                self.node_dict[d].reverse()

            rtn.append(self.node_dict[d])

        return rtn

