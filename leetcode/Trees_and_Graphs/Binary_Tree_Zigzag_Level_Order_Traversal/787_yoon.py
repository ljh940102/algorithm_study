# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from collections import deque

class Solution:
    def zigzagLevelOrder(self, root: TreeNode) -> List[List[int]]:
        level = 0
        node_queue = deque()
        if root is not None:
            node_queue.appendleft((root,level))
        else:
            return list()
        result = list()
        result.append(deque())
        zigzag = False
        while len(node_queue) is not 0:
            current_node, depth = node_queue.popleft()
            if current_node.left:
                node_queue.appendleft((current_node.left, depth + 1)) 
            if current_node.right:
                node_queue.appendleft((current_node.right, depth + 1))
            if len(result) == depth:
                result.append(deque())
            if depth % 2 == 0:
                result[depth].appendleft(current_node.val)
            else:
                result[depth].append(current_node.val)
            
        return result
        
