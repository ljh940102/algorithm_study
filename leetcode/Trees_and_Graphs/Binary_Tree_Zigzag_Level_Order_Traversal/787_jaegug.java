/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> answer = new ArrayList<>();
        levelTr(root, answer, 0);
        return answer;        
    }
    
    private static void levelTr(TreeNode node, List<List<Integer>> answer, int level) {
        
        if (node == null) return;
        
        if (answer.size() == level){
            answer.add(level, new ArrayList<>()); //LinkedList일 때 메모리 효율이 더 떨어짐. 왜?       
        } 
        
        if (level % 2 == 0){
            answer.get(level).add(node.val);
        }else{
            answer.get(level).add(0, node.val); //idx 0 에 추가 > 역순 정렬
        }
            
        levelTr(node.left, answer, level + 1);
        levelTr(node.right, answer, level + 1);
    }
}
