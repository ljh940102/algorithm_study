class Solution:
    def connect(self, root: 'Node') -> 'Node':
        if not root:
            return None

        queue = deque()
        queue.appendleft((root, 0))

        curr_node, curr_depth = None, -1
        while queue:
            next_node, next_depth = queue.pop()
            if curr_depth == next_depth:  # 같은 깊이 -> right 연결
                next_node.next = curr_node

            if next_node.right:
                queue.appendleft((next_node.right, next_depth + 1))

            if next_node.left:
                queue.appendleft((next_node.left, next_depth + 1))

            curr_node, curr_depth = next_node, next_depth

        return root

