class Solution {
    public Node connect(Node root) {
        func(root);

        return root;
    }

    private void func(Node node) {
        if (node == null) {
            return;
        }

        if (node.left != null) {
            node.left.next = node.right;
        }

        if (node.right != null) {
            // 같은 depth 다른 트리 값 추가
            if (node.next != null) {
                node.right.next = node.next.left;
            }
        }

        func(node.right);
        func(node.left);
    }
}