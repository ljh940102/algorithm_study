# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

"""
처음엔 left_node와 right_node의 자식까지 검사했는데,
굳이 그럴 필요없음.
"""
class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        if root is None:
            return True
        return self.checkSymm(root.left, root.right)

    def checkSymm(self, left_node, right_node):
        # None 체크
        if left_node is None and right_node is None:
            return True
        if left_node is None or right_node is None:
            return False

        if left_node.val == right_node.val:
            return self.checkSymm(left_node.left, right_node.right) and self.checkSymm(left_node.right, right_node.left)
        else:
            return False
