// 나중에 시간되면 iterative 방식으로 풀어보기
class Solution {
	public boolean isSymmetric(TreeNode root) {
		if (root == null) {
			return true;
		}
		return isSymmetricRecursive(root.left, root.right);
	}

	boolean isSymmetricRecursive(TreeNode left, TreeNode right) {
		// 좌우 sub-tree 모두 null이면 true
		if (left == null && right == null) {
			return true;
		}
		// 좌우 sub-tree 중 하나만 null이면 false
		if (left == null || right == null) {
			return false;
		}
		// 좌우 sub-tree의 root 값 불일치 시 false
		if (left.val != right.val) {
			return false;
		}
		// 좌측 sub-tree의 좌측 자식(좌좌)과 우측 sub-tree의 우측 자식(우우)에 대해 재귀 호출
		if (!isSymmetricRecursive(left.left, right.right)) {
			return false;
		}
		// 좌측 sub-tree의 우측 자식(좌우)과 우측 sub-tree의 좌측 자식(우좌)에 대해 재귀 호출
		if (!isSymmetricRecursive(left.right, right.left)) {
			return false;
		}
		return true;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/

