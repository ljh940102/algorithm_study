/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public boolean isSymmetric(TreeNode root) {
        if(root==null)
            return true;
        boolean answer = true;
        answer = dfs(root.left, root.right);
        return answer;
    }
    public boolean dfs(TreeNode left, TreeNode right){
        //null 대칭인지 확인
        if(left == null && right == null){
            return true;
        }else if(left == null || right == null)
            return false;

        //자식노드 대칭인지 확인
        if(left.val != right.val)
            return false;

        //자식노드의 자식노드 대칭인지 확인
        if(!dfs(left.left, right.right))
            return false;
        if(!dfs(left.right, right.left))
            return false;
        return true;
    }
}