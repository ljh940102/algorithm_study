# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if root is None:
            return 0

        return Solution.search(root, 1)

    def search(target, current_dept):
        rtn_depth = 0
        # 해당 방향 자식이 있는 경우만 재귀탐색
        if target.left is not None:
            rtn_depth = max(rtn_depth, Solution.search(target.left, current_dept + 1))
        if target.right is not None:
            rtn_depth = max(rtn_depth, Solution.search(target.right, current_dept + 1))
        if target.left is None and target.right is None:  # leaf인 경우 현재 dept 리턴
            return current_dept

        return rtn_depth
