/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public int maxDepth(TreeNode root) {
        //더 이상 탐색할 하위노드가 없으면 0을 리턴
        if(root == null)
            return 0;
        //왼쪽으로 탐색하고
        int left = maxDepth(root.left);
        //오른쪽으로 탐색해서
        int right = maxDepth(root.right);
        //더 큰 값을 하위노드에서 상위노드로 전달
        return 1+Math.max(left,right);
    }
}