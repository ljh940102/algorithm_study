/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {    
    public int maxDepth(TreeNode root) {
        //input 이 null 일 때 예외처리
        if(root == null)
            return 0;
        int depth = 1;
        depth = dfs(root, depth);
        return depth;
    }
    
    public int dfs(TreeNode root, int depth){
        int left = 0;
        int right = 0;
        //더 이상 탐색할 하위노드가 없으면 depth를 리턴한다.
        if(root.left == null && root.right == null){
            return depth;
        }else{
            if(root.left != null){
                //왼쪽으로 탐색하면서 depth에 1씩 더해준다.
                left = dfs(root.left, depth+1);
            }
            if(root.right != null){
                //오른쪽으로 탐색하면서 depth에 1씩 더해준다.
                right = dfs(root.right, depth+1);
            }
        }
        //큰 값을 리턴
        return Math.max(left, right);
    }
}