class Solution:
    K = 0
    answer = 0
    is_finished = False

    def search(self, node):
        if node.left:
            self.search(node.left)

        self.K -= 1

        if self.K == 0:
            self.is_finished = True
            self.answer = node.val

        if node.right:
            self.search(node.right)

    def kthSmallest(self, root: TreeNode, k: int) -> int:
        self.K = k
        self.search(root)
        return self.answer