# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        global result 

        result = 0
        cur_max = -float('inf')
        if root:
            self.is_good(root, cur_max)
        return result
    def is_good(self, node, current_max):
        global result
        if node.val >= current_max:
            current_max = node.val
            result += 1
        if node.left:
            self.is_good(node.left, current_max)
        if node.right:
            self.is_good(node.right, current_max)
 
        
