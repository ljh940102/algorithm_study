# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        answer = 0

        # (node, curr_max)
        stack = [(root, root.val)]

        while stack:
            node, curr_max = stack.pop()
            if not node:
                continue

            if curr_max <= node.val:
                answer += 1
            curr_max = max(curr_max, node.val)

            stack.append((node.left, curr_max))
            stack.append((node.right, curr_max))

        return answer


"""
root = list_to_tree([10,9,8,7,"null",6,5])
print(Solution().goodNodes(root))
"""
