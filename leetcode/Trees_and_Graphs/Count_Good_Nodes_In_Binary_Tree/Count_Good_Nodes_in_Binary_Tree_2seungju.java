/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    private int count = 0;

    public int goodNodes(TreeNode root) {
        countGoodNode(root, Integer.MIN_VALUE);

        return count;
    }

    public void countGoodNode(TreeNode node, int previousMax) {
        if (node == null) {
            return;
        }

        int nodeVal = node.val;
        int max = Math.max(nodeVal, previousMax);

        if (max <= nodeVal) {
            count++;
        }

        countGoodNode(node.left, max);
        countGoodNode(node.right, max);
    }
}