class Solution:
    def get_num_of_good_nodes(self, target_node: TreeNode, path_values):

        good_cnt, is_good = 0, True
        for path_value in path_values:
            if target_node.val < path_value:
                is_good = False
                break

        if is_good:
            good_cnt += 1

        path_values.append(target_node.val)

        if target_node.left:
            good_cnt += self.get_num_of_good_nodes(target_node.left, path_values)

        if target_node.right:
            good_cnt += self.get_num_of_good_nodes(target_node.right, path_values)

        path_values.pop()

        return good_cnt

    def goodNodes(self, root: TreeNode) -> int:
        """
         inx 0을 제외하고는 홀수가 왼쪽 자식, 짝수가 오른쪽 자식이다.
         -> 홀수: value //2 , 짝수: (value-1) // 2 하면 루트로 노트이다.
        """
        return self.get_num_of_good_nodes(root, [])
