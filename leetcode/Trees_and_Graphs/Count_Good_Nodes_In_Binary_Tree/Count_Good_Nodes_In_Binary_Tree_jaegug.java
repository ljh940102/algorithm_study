/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    
    int answer = 0;
    
    public int goodNodes(TreeNode root) {
        
        if (root != null) {
            dfs(root, root.val);
        }
        return answer;
    
    }
    
    private void dfs(TreeNode node, int max) {

        if (node.val >= max) {
            answer++;
        }
        max = Math.max(max, node.val);

        if (node.right != null) {
            dfs(node.right, max);
        }

        if (node.left != null) {
            dfs(node.left, max);
        }
    }
}
