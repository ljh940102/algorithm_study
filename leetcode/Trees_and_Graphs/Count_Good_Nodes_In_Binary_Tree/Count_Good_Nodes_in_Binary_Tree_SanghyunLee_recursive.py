# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        def dfs(node, curr_max):
            if not node:
                return 0
            curr_cnt = int(curr_max <= node.val)
            curr_max = max(curr_max, node.val)
            left_cnt = dfs(node.left, curr_max)
            right_cnt = dfs(node.right, curr_max)
            return curr_cnt + left_cnt + right_cnt
        return dfs(root, root.val)


"""
root = list_to_tree([10,9,8,7,"null",6,5])
print(Solution().goodNodes(root))
"""
