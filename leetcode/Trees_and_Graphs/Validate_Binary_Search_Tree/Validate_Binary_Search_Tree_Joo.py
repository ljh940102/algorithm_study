# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

"""
해결개념은 BST인지 체크하는 함수를 만들고 
해당 함수의 인수로 트리의 값들이 반드시 들어와 있어야하는 범위를 전달하므로써 유효여부를 체크한다.
(BST의 각 노드는 가지는 val값에 제한이 있음을 이용)
"""
import sys

class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        return self.checkBST(root, sys.maxsize, -sys.maxsize)

    def checkBST(self, target, max_criteria, min_criteria):
        if target is None:
            return True
        if target.val <= min_criteria or target.val >= max_criteria:  # val이 주어진 범위를 넘어간다면 성질 위반
            return False

        return self.checkBST(target.left, target.val, min_criteria) and self.checkBST(target.right, max_criteria, target.val)