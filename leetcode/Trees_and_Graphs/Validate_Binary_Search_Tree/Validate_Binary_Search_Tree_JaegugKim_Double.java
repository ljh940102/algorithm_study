/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
 //부모노드 기준으로 왼쪽은 무조건 부모노드보다 작고, 오른쪽은 무조건 커야한다는 전제를 모르고 코딩을 한 후 max, min 값을 추가한 코드로 매우 지저분함
class Solution {
    public boolean isValidBST(TreeNode root) {
        //입력값이 null이면 true 리턴
        if(root==null)
            return true;
        boolean answer = true;
        //왼쪽으로 탐색할 때는 root.val을 max로, 오른쪽으로 탐색할 때는 root.val을 min으로 셋팅.
        //val 의 범위가 Integer 전체이기 때문에 max, min 을 double로 선언하고 초기값은 Integer.MAX_VALUE+1, Integer.MIN_VALUE-1 로 셋팅해주었다.
        //논리연산자 & 를 사용하여 왼쪽으로 비비고 오른쪽으로 비비고 둘다 true 를 리턴 받으면 true 를, 둘 중 하나라도 false 를 리턴 받으면 false 리턴
        answer = dfs(root.left, root.val, (double)Integer.MIN_VALUE-1) & dfs(root.right, (double)Integer.MAX_VALUE+1, root.val);
        return answer;
    }
    public boolean dfs(TreeNode root, double max, double min){
        boolean answer = true;
        if(root==null){
            return true;
        }else{
            //root.val이 max 보다 같거나 크거나, min 보다 같거나 작으면 false 를 리턴해준다.
            if((double)root.val >= max || (double)root.val <= min){
                return false;
            }
                
        }
        //left가 존재하고 left.val 이 root.val 보다 같거나 크면 false 리턴
        if(root.left != null && root.val <= root.left.val)
            return false;
        //right가 존재하고 left.val 이 root.val 보다 같거나 작으면 false 리턴
        if(root.right != null && root.val >= root.right.val)
            return false;
        //왼쪽으로 비비고 오른쪽으로 비벼주는데,
        //왼쪽으로 비빌 때는 max 값을 셋팅해야하므로 만약 max 값이 초기 셋팅한 값(Integer.MAX_VALUE+1)과 똑같으면 root.val을 max 값으로 셋팅해준다.
        //오른쪽으로 비빌 때는 min 값을 셋팅해야하므로 만약 min 값이 초기 셋팅한 값(Integer.MIN_VALUE-1)과 똑같으면 root.val을 min 값으로 셋팅해준다.
        answer = dfs(root.left, max==(double)Integer.MAX_VALUE+1?(double)root.val:max, min) & dfs(root.right, max, min==(double)Integer.MIN_VALUE-1?(double)root.val:min);
        return answer;
    }
}