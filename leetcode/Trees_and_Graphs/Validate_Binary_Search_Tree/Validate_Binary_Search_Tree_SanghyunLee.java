import java.util.*;

class Solution {
	
	public boolean isValidBST(TreeNode root) {
		// 중위 순회 내역을 기록하기 위한 LinkedList
		LinkedList<TreeNode> inorderList = new LinkedList<>();
		return isValidBstRecursive(inorderList, root);
	}
	
	boolean isValidBstRecursive(LinkedList<TreeNode> inorderList, TreeNode root){
		if (root == null) {
			return true;
		}
		
		// 좌측 sub-tree에 대해 중위 순회하며 유효성 검증
		if (!isValidBstRecursive(inorderList, root.left)) {
			return false;
		}

		// BST를 중위순회하면 오름차순으로 정렬되므로
		// 가장 마지막에 들어간 노드는 현재 노드보다 작아야 함
		TreeNode prevNode = inorderList.size() > 0 ? inorderList.getLast() : null;
		if (prevNode != null && prevNode.val >= root.val) {
			return false;
		}
		
		// 현재 노드를 리스트에 추가
		inorderList.add(root);

		// 우측 sub-tree에 대해 중위 순회하며 유효성 검증
		if (!isValidBstRecursive(inorderList, root.right)) {
			return false;
		}

		return true;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/