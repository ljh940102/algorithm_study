# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        if root is None:
            return True

        rtn = True
        if root.left is not None:
            rtn = rtn and self.isValidLeft(root.left,root.val)
        if root.right is not None:
            rtn = rtn and self.isValidRight(root.right,root.val)
        if not rtn:
            return False
        return self.isValidBST(root.left) and self.isValidBST(root.right)

    def isValidLeft(self, target, criteria):
        if target.val >= criteria:
            return False

        rtn = True
        if target.left is not None:
            rtn = rtn and self.isValidLeft(target.left, criteria)
        if target.right is not None:
            rtn = rtn and self.isValidLeft(target.right, criteria)

        return rtn

    def isValidRight(self, target, criteria):
        if target.val <= criteria:
            return False

        rtn = True
        if target.left is not None:
            rtn = rtn and self.isValidRight(target.left, criteria)
        if target.right is not None:
            rtn = rtn and self.isValidRight(target.right, criteria)

        return rtn