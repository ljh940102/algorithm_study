class Solution:
    in_loc_dict = dict()

    def find_leftmost_pre(self, preorder_check):
        for idx, check_used in enumerate(preorder_check):
            if not check_used:
                preorder_check[idx] = True
                return idx

        return None

    def find_tree(self, preorder, preorder_check, start, end):
        target_node = TreeNode()
        pre_idx = self.find_leftmost_pre(preorder_check)

        if pre_idx == None:
            return None

        pre_val = preorder[pre_idx]
        inorder_idx = self.in_loc_dict[pre_val]
        target_node.val = pre_val

        if inorder_idx > start + 1:  # left가 존재하는 경우
            left_start, left_end = start, inorder_idx - 1
            target_node.left = self.find_tree(preorder, preorder_check, left_start, left_end)
        else:
            target_node.left = None

        if end > inorder_idx:  # right가 존재하는 경우
            right_start, right_end = inorder_idx + 1, end
            target_node.right = self.find_tree(preorder, preorder_check, right_start, right_end)
        else:
            target_node.right = None

        return target_node

    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        """
        전위가 중위를 정확하게 반으로 나눈다.
        전위 값과 중위를 받아 트리를 만들 수 있다.
        --------------------
                1
           2         3
         4    5    6
                 7   8
        --------------------

        preorder: 1 2 4 5 3 6 7 8
        inorder: 4 2 5 1 7 6 8 3
        1, (4 2 5 1 7 6 8 3) -> (4 2 5) , (7 6 8 3)
            2, (4 2 5) -> (4) , (5)
                4, (4) -> null , null -> leaf
                5, (5) -> null , null -> leaf
            3, (7 6 8 3) -> (7 6 8) , null
                6 , (7 6 8) -> (7) , (8)
                    7 , (7) -> null , null -> leaf
                    8 , (8) -> null , null -> leaf
        """
        pre_len, in_len = len(preorder), len(inorder)
        preorder_check = [False] * pre_len

        self.in_loc_dict = {in_val: idx for idx, in_val in enumerate(inorder)}

        rtn = self.find_tree(preorder, preorder_check, 0, in_len)

        return rtn