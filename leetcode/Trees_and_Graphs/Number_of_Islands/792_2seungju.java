class Solution {
    private int[] x = {0, 1, 0, -1};
    private int[] y = {1, 0, -1, 0};

    public int numIslands(char[][] grid) {
        int count = 0;
        int m = grid.length;
        int n = grid[0].length;

        boolean[][] check = new boolean[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1' && !check[i][j]) {
                    func(grid, check, i, j);
                    count++;
                }
            }
        }

        return count;
    }

    private void func(char[][] grid, boolean[][] check, int m, int n) {
        check[m][n] = true;

        // 직선방향 루프
        for (int i = 0; i < 4; i++) {
            int nextM = m + x[i];
            int nextN = n + y[i];

            if (checkIndexOut(grid, nextN, nextM)) {
                continue;
            }

            if (grid[nextM][nextN] == '1' && !check[nextM][nextN]) {
                func(grid, check, nextM, nextN);
            }
        }
    }

    private boolean checkIndexOut(char[][] grid, int x, int y) {
        return x < 0 || x >= grid[0].length || y < 0 || y >= grid.length;
    }
}