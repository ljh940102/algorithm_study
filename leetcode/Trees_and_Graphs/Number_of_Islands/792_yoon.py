from collections import deque
from typing import List


class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        def bfs(row, col, grid):
            queue = deque()
            queue.append((row, col))
            while queue:
                row, col = queue.popleft()
                if 0 <= row < len(grid) and 0 <= col < len(grid[0]) and grid[row][col] == '1':
                    grid[row][col] = '0'
                    for direction in directions:
                        next_row, next_col = row + direction[0], col + direction[1]
                        queue.append((next_row, next_col))

        result = 0
        for row in range(len(grid)):
            for col in range(len(grid[0])):
                if grid[row][col] == '1':
                    bfs(row, col, grid)
                    result += 1
        return result