class Solution:
    move_r = [-1, 1, 0, 0]
    move_c = [0, 0, -1, 1]

    def dfs(self, grid, r, c):
        if r < 0 or c < 0 or r >= len(grid) or c >= len(grid[0]) or grid[r][c] == "0":
            return

        grid[r][c] = "0"

        for i in range(4):
            self.dfs(grid, r + self.move_r[i], c + self.move_c[i])

    def numIslands(self, grid: List[List[str]]) -> int:
        cnt = 0
        if len(grid) <= 0:
            return 0

        for r in range(len(grid)):
            for c in range(len(grid[0])):
                if grid[r][c] == "1":
                    cnt += 1
                    self.dfs(grid, r, c)
        return cnt