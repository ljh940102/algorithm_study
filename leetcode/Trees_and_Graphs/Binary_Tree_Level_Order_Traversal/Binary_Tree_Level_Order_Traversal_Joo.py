# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        if root is None:
            return []
        level_order = {}
        self.putLevel(root,0,level_order)

        return list(level_order.values())

    def putLevel(self,target,depth,level_order):
        if target is None:
            return
        if level_order.get(depth) is None:  # 해당 depth가 처음인 경우 list 생성 및 추가
            level_order[depth] = [target.val]
        else:
            level_order[depth].append(target.val)

        self.putLevel(target.left, depth+1,level_order)  # left tree
        self.putLevel(target.right, depth+1,level_order)  # right tree
