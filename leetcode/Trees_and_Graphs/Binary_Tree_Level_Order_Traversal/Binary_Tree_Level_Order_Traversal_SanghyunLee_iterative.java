import java.util.*;

class Solution {

	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> all_levels = new LinkedList<>();
		
		// root가 null인 경우 [[]]를 반환하는게 아니라 []를 반환해야하는 것에 유의
		if (root == null) {
			return all_levels;
		}

		// tree를 level 별로 순회하기 위해 node를 저장하는 리스트
		LinkedList<TreeNode> node_queue = new LinkedList<>();
		node_queue.add(root);

		// 모든 node를 한번씩 순회
		while (!node_queue.isEmpty()) {
			// 해당 level의 결과 리스트 초기화
			LinkedList<Integer> one_level = new LinkedList<>();
			
			// 동일 level의 노드들에 대해 반복
			int q_size = node_queue.size();
			for (int qs = 0; qs < q_size; qs++) {
				
				// node리스트에서 빼고 결과 리스트에 추가
				TreeNode node = node_queue.pop();
				one_level.add(node.val);
				
				// 다음 level의 노드를 노드 리스트에 추가
				if (node.left != null) {
					node_queue.add(node.left);
				}
				if (node.right != null) {
					node_queue.add(node.right);
				}
			}
			// 해당 level이 끝났으니 최종 결과에 추가
			all_levels.add(one_level);
		}
		return all_levels;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/

