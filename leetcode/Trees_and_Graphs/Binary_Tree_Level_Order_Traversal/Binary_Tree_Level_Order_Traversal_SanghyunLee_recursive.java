import java.util.*;

class Solution {

	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> all_levels = new LinkedList<>();
		levelOrderRecursive(all_levels, 0, root);
		return all_levels;
	}

	void levelOrderRecursive(List<List<Integer>> all_levels, int level, TreeNode node) {
		if (node == null) {
			return;
		}
		if (all_levels.size() <= level) {
			all_levels.add(new LinkedList<Integer>());
		}

		levelOrderRecursive(all_levels, level + 1, node.left);

		all_levels.get(level).add(node.val);

		levelOrderRecursive(all_levels, level + 1, node.right);
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/

