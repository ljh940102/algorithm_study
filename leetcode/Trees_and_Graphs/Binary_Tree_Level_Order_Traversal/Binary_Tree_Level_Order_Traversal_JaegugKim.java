/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public static List<List<Integer>> answer;
    
    public List<List<Integer>> levelOrder(TreeNode root) {
        answer = new ArrayList<>();
        if(root==null)
            return answer;
        int depth = 0;
        depth = dfs(root, depth, answer);
        return answer;
    }

    //이전에 사용했던 depth 구하는 함수를 재활용, depth를 index로 하여 List에 같은 depth의 root.val을 하나씩 추가한다.
    public int dfs(TreeNode root, int depth, List<List<Integer>> answer){
        //answer에 해당 depth의 List<Integer> 가 없으면 새로 만들어준다.
        if(answer.size()<=depth){
            List<Integer> tmp = new ArrayList<>();
            tmp.add(root.val);
            answer.add(depth, tmp);
        //answer에 해당 depth의 List<Integer> 가 담겨있으면 호출해서 변경(List에 root.val 추가)
        }else{
            List<Integer> tmp = answer.get(depth);
            tmp.add(root.val);
            answer.set(depth, tmp);
        }
        
        int left = 0;
        int right = 0;
        //더 이상 탐색할 하위노드가 없으면 depth를 리턴한다.
        if(root.left == null && root.right == null){
            return depth;
        }else{
            if(root.left != null){
                //왼쪽으로 탐색하면서 depth에 1씩 더해준다.
                left = dfs(root.left, depth+1, answer);
            }
            if(root.right != null){
                //오른쪽으로 탐색하면서 depth에 1씩 더해준다.
                right = dfs(root.right, depth+1, answer);
            }
        }
        //큰 값을 리턴
        return Math.max(left, right);
    }
}