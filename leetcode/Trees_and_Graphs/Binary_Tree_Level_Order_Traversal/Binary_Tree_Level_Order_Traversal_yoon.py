# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from collections import deque
class Solution(object):
    def levelOrder(self, root):
        queue, result = deque(), []
        if root:
            queue.append(root)
        while len(queue):
            level = []
            for _ in range(len(queue)):
                x = queue.popleft()
                level.append(x.val)
                if x.left:
                    queue.append(x.left)
                if x.right:
                    queue.append(x.right)
            result.append(level)
        return result