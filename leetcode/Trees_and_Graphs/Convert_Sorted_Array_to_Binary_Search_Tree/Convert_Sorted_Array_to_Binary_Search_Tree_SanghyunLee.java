import java.io.*;
import java.util.*;

class Solution {
	public TreeNode sortedArrayToBST(int[] nums) {
		return recursiveSortedArrayToBST(nums, 0, nums.length - 1);
	}
	TreeNode recursiveSortedArrayToBST(int[] nums, int start, int end) {
		// 이 구문으로 길이 0짜리 입력과 빈 좌우 서브트리 처리 가능
		if (start > end) {
			return null;
		}
		int mid = (start + end) / 2;
		TreeNode root = new TreeNode(nums[mid]);
		root.left = recursiveSortedArrayToBST(nums, start, mid - 1);
		root.right = recursiveSortedArrayToBST(nums, mid + 1, end);
		return root;
	}
}

/*
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	TreeNode() {}
	TreeNode(int val) {
		this.val = val;
	}
	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}
*/

