# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
        center = len(nums) // 2

        rtn = right_node = left_node = TreeNode(nums[center])
        for move in range(1,center+1):

            if center + move < len(nums):
                left_node.left = TreeNode(nums[center + move])
                left_node = left_node.left

            if center - move >= 0:
                right_node.right = TreeNode(nums[center - move])
                right_node = right_node.right

        return rtn