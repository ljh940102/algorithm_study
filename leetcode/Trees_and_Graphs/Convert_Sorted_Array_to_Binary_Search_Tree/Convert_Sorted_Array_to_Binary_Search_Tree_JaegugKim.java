/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public TreeNode sortedArrayToBST(int[] nums) {
        //nums가 빈 배열이면 null 을 리턴
        if(nums.length == 0){
            return null;    
        }     
        //nums의 길이가 1이면 배열의 값을 val로 가진 Tree 생성하여 리턴
        if(nums.length == 1){
            TreeNode root = new TreeNode(nums[0]);    
            return root;
        //배열의 길이가 2이상인 경우
        }else{  
            //반으로 갈라서 가운데 값을 val로 가진 Tree 생성
            TreeNode root = new TreeNode(nums[nums.length/2]);
            //왼쪽 하위노드 = sortedArrayToBST 재귀호출, 인자는 nums를 0부터 가운데까지 복사한 배열
            root.left = sortedArrayToBST(Arrays.copyOfRange(nums,0,nums.length/2));
            //가운데가 nums의 마지막이 아니라면
            if(nums.length/2<nums.length-1)
                //오른쪽 하위노드 = sortedArrayToBST 재귀호출, 인자는 nums를 가운데부터 끝까지 복사한 배열
                root.right = sortedArrayToBST(Arrays.copyOfRange(nums,nums.length/2+1,nums.length));
            return root;
        }
    }
}