/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> answer = new ArrayList<Integer>();
        inorderTraverse(root, answer);
        return answer;

    }
    
    private void inorderTraverse(TreeNode self, List<Integer> answer) {
        if(self == null) return;
        inorderTraverse(self.left, answer);
        answer.add(self.val);
        inorderTraverse(self.right, answer);
    }

}
