# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        Store = list()
        if root is None:
            return root
        self.inorderTraversalRecur(root,Store)
        return Store
    
    def inorderTraversalRecur(self,root,Store):
        if root.left is not None:
            self.inorderTraversalRecur(root.left,Store)
        Store.append(root.val)
        if root.right is not None:
            self.inorderTraversalRecur(root.right,Store)
        return Store
