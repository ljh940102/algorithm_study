class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:

        if not root:
            return []

        inorder = []

        if root.left:
            inorder.extend(self.inorderTraversal(root.left))

        inorder.append(root.val)

        if root.right:
            inorder.extend(self.inorderTraversal(root.right))

        return inorder
