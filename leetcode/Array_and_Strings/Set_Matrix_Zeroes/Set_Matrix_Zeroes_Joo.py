class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        M, N = len(matrix), len(matrix[0])
        zero_target = []
        for row in range(M):
            for col in range(N):
                if matrix[row][col] == 0:
                    zero_target.append((row, col))

        for row, col in zero_target:
            matrix[row] = [0] * N  # 행처리
            for target in range(M):  # 열처리
                matrix[target][col] = 0