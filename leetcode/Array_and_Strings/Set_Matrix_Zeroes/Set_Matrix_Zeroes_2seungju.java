import java.util.*;

class Solution {
    public void setZeroes(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;

        Set<Integer> xSet = new HashSet<>();
        Set<Integer> ySet = new HashSet<>();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    xSet.add(i);
                    ySet.add(j);
                }
            }
        }

        for (Integer x : xSet) {
            for (int i = 0; i < n; i++) {
                matrix[x][i] = 0;
            }
        }

        for (Integer y : ySet) {
            for (int i = 0; i < m; i++) {
                matrix[i][y] = 0;
            }
        }
    }
}