class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        for i in range(0,len(matrix)):
            for j in range(0,len(matrix[0])):
                if matrix[i][j] == 0:
                    matrix[i][j] = None
        
        for i in range(0,len(matrix)):
            for j in range(0,len(matrix[0])):
                if matrix[i][j] == None:
                    for k in range(0,len(matrix[0])):
                        if matrix[i][k] != None:
                            matrix[i][k] = 0
                    for l in range(0,len(matrix)):
                        if matrix[l][j] != None:
                            matrix[l][j] = 0
        
        for i in range(0,len(matrix)):
            for j in range(0,len(matrix[0])):
                if matrix[i][j] == None:
                    matrix[i][j] = 0
