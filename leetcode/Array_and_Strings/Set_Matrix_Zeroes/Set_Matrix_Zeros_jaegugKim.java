class Solution {
    public void setZeroes(int[][] matrix) {
        boolean firstCol = false; //첫째 열을 0으로 변환할지 여부 
        boolean firstRow = false; //첫째 행을 0으로 변환할지 여부
        
        if(matrix[0][0]==0){
            firstCol = true;
            firstRow = true;
        }
            
        for(int row=0; row<matrix.length; row++){
            for(int col=0; col<matrix[row].length; col++){
                if(matrix[row][col]==0){ //0을 찾으면
                    matrix[row][0]=0; //첫번째 열의 해당 행
                    matrix[0][col]=0; //첫번째 행의 해당 열에 0 표기
                    if(row==0) firstRow=true; //첫번째 행에 0이 있으면 첫번째 행을 0으로 변환
                    if(col==0) firstCol=true; //첫번째 열에 0이 있으면 첫번째 열을 0으로 변환
                }
            }
        }
        
        if(matrix.length > 1){ //행이 2개 이상일 때
            for(int col=1; col<matrix[0].length; col++){
                if(matrix[0][col]==0){ //첫번째 행의 해당 열에 0이 표기되어있으면
                    for(int row=1; row<matrix.length; row++){
                        matrix[row][col]=0; //해당 열을 모두 0으로 변환
                    }
                }
            }   
            
        }
        
        if(matrix[0].length > 1){ //열이 2개 이상일 때
            for(int row=1; row<matrix.length; row++){
               if(matrix[row][0]==0){ //첫번째 열의 해당 행에 0이 표기되어있으면
                    for(int col=1; col<matrix[row].length; col++){
                        matrix[row][col]=0; //해당 행을 모두 0으로 변환
                    }
                }
            }       
        }
        
        if(firstCol){ //첫째 열을 0으로 변환
            for(int row=0; row<matrix.length; row++){
                matrix[row][0]=0;
            }
        }
        
        if(firstRow){ //첫째 행을 0으로 변환
            for(int col=0; col<matrix[0].length; col++){
                matrix[0][col]=0;
            }
        }
    }
}
