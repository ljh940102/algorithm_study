import java.util.*;

class Solution {

	public boolean isValidSudoku(char[][] board) {
		for (int i = 0; i < 9; i++) {
			if (validateRow(board, i) && validateColumn(board, i) && validateBox(board, i)) {
				continue;
			} else {
				return false;
			}
		}
		return true;
	}
	private boolean validateRow(char[][] board, int row) {
		boolean[] visited = new boolean[10];
		visited[0] = true;
		for (int i = 0; i < 9; i++) {
			char c = board[row][i];
			if (c == '.') {
				continue;
			}
			if (visited[c - '0']) {
				return false;
			}
			visited[c - '0'] = true;
		}
		return true;
	}

	private boolean validateColumn(char[][] board, int column) {
		boolean[] visited = new boolean[10];
		visited[0] = true;
		for (int i = 0; i < 9; i++) {
			char c = board[i][column];
			if (c == '.') {
				continue;
			}
			if (visited[c - '0']) {
				return false;
			}
			visited[c - '0'] = true;
		}
		return true;
	}

	private boolean validateBox(char[][] board, int index) {
		boolean[] visited = new boolean[10];
		visited[0] = true;
		int row = index / 3;
		int column = index % 3;
		for (int i = 0; i < 9; i++) {
			int newRow = 3 * row + i / 3;
			int newColumn = 3 * column + i % 3;
			char c = board[newRow][newColumn];
			if (c == '.') {
				continue;
			}
			if (visited[c - '0']) {
				return false;
			}
			visited[c - '0'] = true;
		}
		return true;
	}
    /*
    public boolean isValidSudoku(char[][] board) {  // 3 ms
    	List<StringBuilder> rows = new ArrayList<>();
    	List<StringBuilder> cols = new ArrayList<>();
    	List<StringBuilder> cells = new ArrayList<>();
        for(int i=0; i<9; i++) {
        	rows.add(new StringBuilder());
        	cols.add(new StringBuilder());
        	cells.add(new StringBuilder());
        }
        for(int i=0; i<9; i++) {
        	for(int j=0; j<9; j++) {
        		char c = board[i][j];
            	if(c == '.') {
            		continue;
            	}
            	rows.get(i).append(c);
            	cols.get(j).append(c);
            	cells.get((i/3)*3 + j/3).append(c);
            }
        }
        for(int i=0; i<9; i++) {
        	if(hasDuplicate(rows.get(i).toString())
        			|| hasDuplicate(cols.get(i).toString())
        			|| hasDuplicate(cells.get(i).toString())) {
        		return false;
        	}
        }
        return true;
    }
    boolean hasDuplicate(String input) {
    	int len = input.length();
    	Set<Character> set = new HashSet<>();
    	for(int i=0; i<len; i++) {
    		char c = input.charAt(i);
    		set.add(c);
    	}
    	return set.size() != len;
    }
    public boolean isValidSudoku(char[][] board) {  // 17 ms
    	String[] rows = new String[9];
    	String[] cols = new String[9];
    	String[] cells = new String[9];
    	for(int i=0; i<9; i++) {
        	rows[i] = "";
        	cols[i] = "";
        	cells[i] = "";
        }
        for(int i=0; i<9; i++) {
        	for(int j=0; j<9; j++) {
        		char c = board[i][j];
            	if(c == '.') {
            		continue;
            	}
            	rows[i] += c;
            	cols[j] += c;
            	cells[(i/3)*3 + j/3] += c;
            }
        }
        for(int i=0; i<9; i++) {
        	if(hasDuplicate(rows[i])) {
        		return false;
        	}
        	else if(hasDuplicate(cols[i])) {
        		return false;
        	}
        	else if(hasDuplicate(cells[i])) {
        		return false;
        	}
        }
        return true;
    }
    */
}
