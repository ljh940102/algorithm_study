import java.util.*;

/*
HashSet 
빠른 접근속도

LinkedHashSet 
입력된 순서대로 정렬

TreeSet 
키값 오름차순으로 정렬
 */

class Solution {
	/*
	public boolean containsDuplicate(int[] nums) {

        Set<Integer> set = new HashSet<>();
        for(int num : nums) {
        	set.add(num);
        }
        return set.size() != nums.length;
    }
     */
	public boolean containsDuplicate(int[] nums) {
		Arrays.sort(nums);
		for (int i = 0; i < nums.length - 1; i++) {
			if (nums[i] == nums[i + 1])
				return true;
		}
		return false;
	}
}
