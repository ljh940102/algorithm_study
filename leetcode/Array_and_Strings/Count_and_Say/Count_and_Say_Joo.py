"""
이전 단계를 count and say 하면서 넘어가는 문제
인풋으로 n(1<= n <= 30)이 주어지면 n번째에 해당하는 count and say 리턴
무식하게 세면 맞긴해도 시간 초과일듯?
recursive 하게 호출할 수 있다는 문구를 보니.. 이전에 가지고 있는 값이 딱 나오면 그전꺼 끌어다 쓰면될듯?
일종의 DP네, 대신 앞뒤로 연속된 수가 없어야겠군..
set이용해서 중복체크하고, 연속된 수가 끝날떄마다 있는지 체크? 이것도 아닌데..
"""


class Solution:
    def countAndSay(self, n: int) -> str:
        rtn = "1"
        rtn_list = []

        for stage in range(n - 1):
            # 첫 char read 처리
            now_char, count = rtn[0], 1
            for idx in range(1, len(rtn)):  # 두번째 글자부터 read
                if now_char == rtn[idx]:
                    count += 1
                else:  # 현재까지의 read 값 append 처리 -> 다음글자 read 처리
                    rtn_list.append(str(count))
                    rtn_list.append(now_char)
                    now_ch, count = rtn[idx], 1

            # 마지막 read append
            rtn_list.append(str(count))
            rtn_list.append(now_char)
            rtn = ''.join([ch for ch in rtn_list])
            rtn_list.clear()

        return rtn

def countAndSay(n: int) -> str:
    rtn = "1"
    for stage in range(n - 1):
        rtn_list = []

        # 첫 char read 처리
        now_ch, count = rtn[0], 1
        for idx in range(1, len(rtn)):  # 두번째 글자부터 read
            if now_ch == rtn[idx]:
                count += 1
            else:  # 현재까지의 read 값 append 처리 -> 다음글자 read 처리
                rtn_list.append(str(count))
                rtn_list.append(now_ch)

                now_ch, count = rtn[idx], 1

        # 마지막 read append
        rtn_list.append(str(count))
        rtn_list.append(now_ch)
        rtn = ''.join([ch for ch in rtn_list])

    return rtn

print(countAndSay(1))
print(countAndSay(2))
print(countAndSay(3))

print(countAndSay(10))
