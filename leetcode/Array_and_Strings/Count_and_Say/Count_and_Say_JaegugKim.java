class Solution {
    public String countAndSay(int n) {
        String answer = "1";
        for(int i = 1; i<n; i++){
            answer = readoff(answer);    
        }
        return answer;
    }

    public String readoff(String s){
        int count = 1;
        char tmp = s.charAt(0);
        String result = "";
        for(int i=1; i<s.length(); i++){
            if(s.charAt(i) == tmp){
                count++;
            }else{
                result += Integer.toString(count);
                result += tmp;
                tmp = s.charAt(i);
                count = 1;
            }
                
        }
        result += Integer.toString(count);
        result += tmp;
        return result;
    }
}
