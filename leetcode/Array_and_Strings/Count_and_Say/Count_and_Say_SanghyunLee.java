class Solution {
    public String countAndSay(int n) {
        if(n==1) {
        	return "1";
        }
        String before = countAndSay(n-1);
        int before_length = before.length();

        StringBuilder sb = new StringBuilder();
        
        int start_idx = 0;
        while(start_idx < before_length) {
        	char c = before.charAt(start_idx);
        	int count = 1;
        	int end_idx = start_idx + 1;
        	while((end_idx < before_length) && (c == before.charAt(end_idx))) {
        		count++;
        		end_idx++;
        	}
        	sb.append(Integer.toString(count));
        	sb.append(c);
        	start_idx = end_idx;
        }
        return sb.toString();
    }
}
