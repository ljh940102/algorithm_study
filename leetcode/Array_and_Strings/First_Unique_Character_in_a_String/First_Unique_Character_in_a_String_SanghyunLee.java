class Solution {
	public int firstUniqChar(String s) {
		// +1 빼먹어서 틀렸었음, 배열 크기는 인덱스 + 1인 것 항상 유의
		int[] char_to_cnt = new int['z' - 'a' + 1];
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			char_to_cnt[c - 'a']++;
		}
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (char_to_cnt[c - 'a'] == 1) {
				return i;
			}
		}
		return -1;
	}
	/*	
	// 더 빠른 정답
	public int firstUniqChar(String s) {
		int min = Integer.MAX_VALUE;
		for (char i = 'a'; i <= 'z'; i++) {
			int index = s.indexOf(i);
			if (index != -1 && s.lastIndexOf(i) == index) {
				min = Math.min(index, min);
			}
		}
		return min == Integer.MAX_VALUE ? -1 : min;
	}
	*/
}
