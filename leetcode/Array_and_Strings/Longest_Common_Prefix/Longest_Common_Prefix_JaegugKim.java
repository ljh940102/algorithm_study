class Solution {
    public String longestCommonPrefix(String[] strs) {
        String answer = "";
        boolean flag = true;
        if(strs.length == 0)
            return "";
        
        
        for(int i = 0; i<strs[0].length(); i++){
            for(int j = 1; j<strs.length; j++){
                if(strs[j].length() < i+1){
                    flag = false;
                    break;
                }    
                if(strs[j].charAt(i) == strs[j-1].charAt(i)){
                    continue;
                }else{
                    flag = false;
                    break;
                }          
            }
            if(flag){
                answer += strs[0].charAt(i);
            }else{
                break;
            }
        }
        
        return answer;
    }
}
