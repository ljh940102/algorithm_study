class Solution {
    public String longestCommonPrefix(String[] strs) {
        int n = strs.length;
        if(n==0) {
        	return "";
        }

        int min_length = strs[0].length();
        for(int i=1; i<n; i++) {
        	if(strs[i].length() < min_length) {
        		min_length = strs[i].length();
        	}
        }
        if(min_length==0) {
        	return "";
        }
        
        int char_idx = 0;
        while(char_idx < min_length) {
        	char c = strs[0].charAt(char_idx);
        	int str_idx = 1;
	        for(; str_idx<n; str_idx++) {
	        	if(strs[str_idx].charAt(char_idx) != c) {
	        		break;
	        	}
	        }
	        if(str_idx==n) {
	        	char_idx++;
	        }
	        else {
	        	break;
	        }
        }
        return strs[0].substring(0, char_idx);
    }
}
