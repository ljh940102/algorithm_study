class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        rtn = ""

        if not strs:
            return rtn

        strs = sorted(strs)  # 정렬 후 가장 앞 str과 뒤 str 비교
        first = strs[0]
        last = strs[len(strs)-1]

        for cmp_idx in range(len(first)):
            if first[cmp_idx] == last[cmp_idx]:
                rtn += first[cmp_idx]
            else:
                break

        return rtn
