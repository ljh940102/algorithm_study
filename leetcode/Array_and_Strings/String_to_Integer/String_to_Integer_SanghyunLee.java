class Solution {

	public int myAtoi(String s) {
		int idx = 0;
		int len = s.length();
		// 앞에 공백 제거
		while (idx < len && s.charAt(idx) == ' ') {
			idx++;
		}
		// 공백밖에 없음
		if (idx == len) {
			return 0;
		}

		// 부호 처리
		int sign = 1;
		if (s.charAt(idx) == '+') {
			idx++;
		}
		else if (s.charAt(idx) == '-') {
			sign = -1;
			idx++;
		}
		
		// 부호 뒤에 아무것도 없거나 숫자가 아님
		// 사실 이거 없어도 아래에서 어차피 0을 출력하므로 동일 결과 도출
		//if (idx == len || !Character.isDigit(s.charAt(idx))) {
		//	return 0;
		//}

		long ret = 0;
		while(idx < len && Character.isDigit(s.charAt(idx))) {
			ret = ret * 10 + (s.charAt(idx) - '0');
			if (sign * ret > Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			}
			else if (sign * ret < Integer.MIN_VALUE) {
				return Integer.MIN_VALUE;
			}
			idx++;
		}
		return (int) (sign * ret);
	}
}
