class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        
        List<Integer> maxList = new ArrayList<>();
        
        PriorityQueue<List<Integer>> priorityQueue = 
                new PriorityQueue<>((a,b) -> b.get(0) - a.get(0));
        
        for(int i = 0; i < nums.length; i++){
            
            List<Integer> positionSet = new ArrayList<>();
            
            positionSet.add(nums[i]);
            positionSet.add(i);
            
            priorityQueue.add(positionSet);
            
            if(i < k-1) continue;
            
            while(!priorityQueue.isEmpty() && priorityQueue.peek().get(1) <= i-k) 
                priorityQueue.poll();
            
            maxList.add(priorityQueue.peek().get(0));
        }

        int[] answer = new int[maxList.size()];
        
        for(int i = 0; i < maxList.size(); i++){
            
            answer[i] = maxList.get(i);
        }
        
        return answer;
    }
}
