class Solution {
	public void rotate(int[] nums, int k) {
		k = k % nums.length;
		int count = 0;
		for (int start_idx = 0; count < nums.length; start_idx++) {
			int current_idx = start_idx;
			int prev_val = nums[start_idx];
			do {
				int next_idx = (current_idx + k) % nums.length;
				int temp = nums[next_idx];
				nums[next_idx] = prev_val;
				prev_val = temp;
				current_idx = next_idx;
				count++;
			} while (start_idx != current_idx);
		}
	}
}
