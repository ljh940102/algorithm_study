def lengthOfLongestSubstring(s):
    rtn = 0
    for idx in range(len(s)):
        curr_length, memo = 0, set()

        for check_idx in range(idx, len(s)):
            if s[check_idx] in memo:  # 중복
                break
            else:
                memo.add(s[check_idx])
                curr_length += 1
        rtn = max(rtn, curr_length)
    return rtn

print(lengthOfLongestSubstring("pwwkew"))
print(lengthOfLongestSubstring("abcabcbb"))
print(lengthOfLongestSubstring(" "))

t = set()
t.add(" ")
print(len(" "))


aabba