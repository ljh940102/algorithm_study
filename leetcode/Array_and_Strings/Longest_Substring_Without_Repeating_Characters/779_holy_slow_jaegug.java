class Solution {
    public int lengthOfLongestSubstring(String s) {
        int maxLen = (s.length()==0)?0:1;
        for(int i=0; i<s.length()-maxLen; i++){
            for(int j=i; j<s.length(); j++){
                if(s.substring(i,j).contains(s.substring(j,j+1))==true){
                    if(maxLen < j-i){
                        maxLen = j-i;
                    }
                    break;
                }else if(j==s.length()-1){
                    if(maxLen < j-i+1){
                        maxLen = j-i+1;
                    }
                }
            }
        }
        return maxLen;
    }
}
