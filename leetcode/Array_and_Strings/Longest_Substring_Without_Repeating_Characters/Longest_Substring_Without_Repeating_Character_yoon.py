class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        longest_substr = ""
        ans = 0
        for char in s:
            for i in range(len(longest_substr)):
                if char == longest_substr[i]:
                    if i != len(longest_substr) - 1:
                        longest_substr = longest_substr[i+1:]
                    else:
                        longest_substr = ""
                    break
            longest_substr = "".join([longest_substr, char])
            ans = max(ans, len(longest_substr))
        return ans
                
