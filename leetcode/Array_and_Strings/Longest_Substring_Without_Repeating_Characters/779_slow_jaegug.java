class Solution {
    public int lengthOfLongestSubstring(String s) {
        boolean[] visited = new boolean[256];
        int maxLen = (s.length()==0)?0:1;
        for(int i=0; i<s.length(); i++){
            Arrays.fill(visited, false);
            for(int j=i; j<s.length(); j++){
                int flag = (int) s.charAt(j) - ' ';
                if(visited[flag]==true){
                    if(maxLen < j-i){
                        maxLen = j-i;
                    }
                    break;
                }else if(j==s.length()-1){
                    if(maxLen < j-i+1){
                        maxLen = j-i+1;
                    }
                }else{
                    visited[flag]=true;
                }
            }
        }
        return maxLen;
    }
}
