import java.util.*;

class Solution {

	// 매우 빠르고 간결한 정답
	public boolean isAnagram(String s, String t) {
		if (s.length() != t.length()) {
			return false;
		}

		// String을 char 배열로 바꿔주는 toCharArray함수는 기억해두면 유용할 듯
		char[] arr1 = s.toCharArray();
		char[] arr2 = t.toCharArray();

		// Arrays.sort : 단순 배열 정렬
		// Collections.sort : Collections 소속 객체 (ArrayList, LinkedList 등) 정렬
		Arrays.sort(arr1);
		Arrays.sort(arr2);

		// arr1.equals(arr2) 이렇게 하면 틀림, 실제로 동일 객체인지(주소값이 같은지) Reference 비교하는 것임
		// 이상하게도 Collections 객체의 내용(element) 비교는 c1.equals(c2)와 같이 수행함
		// Collections.equals(c1, c2)는 존재하지 않는 함수
		return Arrays.equals(arr1, arr2);
	}

	/*
	// 쓸데없이 Map 써서 어렵게 풀었음, sort 후 비교하는게 정답
	public boolean isAnagram(String s, String t) {
		int slen = s.length();
		int tlen = t.length();
		if (slen != tlen) {
			return false;
		}
		Map<Character, Integer> schar_to_cnt = new HashMap<>();
		Map<Character, Integer> tchar_to_cnt = new HashMap<>();
		for(int i=0; i<slen; i++) {
			char schar = s.charAt(i);
			if(!schar_to_cnt.containsKey(schar)) {
				schar_to_cnt.put(schar, 0);
			}
			else {
				int curr_cnt = schar_to_cnt.get(schar);
				schar_to_cnt.put(s.charAt(i), curr_cnt + 1);
			}

			char tchar = t.charAt(i);
			if(!tchar_to_cnt.containsKey(tchar)) {
				tchar_to_cnt.put(tchar, 0);
			}
			else {
				int curr_cnt = tchar_to_cnt.get(tchar);
				tchar_to_cnt.put(t.charAt(i), curr_cnt + 1);
			}
		}
		// Collections 객체의 내용(element) 비교는 아래와 같이 수행
		// 단순 리스트(Array)에 대해 아래와 같이 하면 객체 Reference 비교가 수행됨
		// 단순 리스트(Array)의 내용 비교는 Arrays.equals(arr1, arr2)와 같이 수행
		return schar_to_cnt.equals(tchar_to_cnt);
	}
	*/
}
