import java.io.*;

class Solution {
    public void moveZeroes(int[] nums) {
        int read_idx = 0;
        int write_idx = 0;
        for(; read_idx<nums.length; read_idx++) {
        	if(nums[read_idx] != 0) {
        		nums[write_idx++] = nums[read_idx];
        	}
        }
        for(; write_idx<nums.length; write_idx++) {
        	nums[write_idx] = 0;
        }
    }
}
