from collections import defaultdict
from typing import List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        sorted_strs = []

        for str in strs:
            sorted_string = "".join(sorted(str))
            sorted_strs.append(sorted_string)
        sorted_dict = defaultdict(list)
        for i, sorted_str in enumerate(sorted_strs):
            sorted_dict[tuple(sorted_str)].append(i)
        result = []

        for sorted_index_list in sorted_dict.values():
            temp = []
            for sorted_index in sorted_index_list:
                temp.append(strs[sorted_index])
            result.append(temp)

        return result