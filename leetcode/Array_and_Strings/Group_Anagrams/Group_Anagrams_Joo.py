from collections import defaultdict

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        check = defaultdict(list)
        for _str in strs:
            check["".join(sorted(_str))].append(_str)

        return list(check.values())
