from collections import Counter

def groupAnagrams(strs):
    answer, targets = list(), list()
    for _str in strs:
        targets.append((_str, Counter(_str)))

    for idx1 in range(len(targets)):
        if not targets[idx1]:
            continue
        answer_list = [targets[idx1][0]]
        for idx2 in range(idx1+1, len(targets)):
            if not targets[idx2]:
                continue

            if not targets[idx1][1] - targets[idx2][1] and not targets[idx2][1] - targets[idx1][1]:  # 같은 anagram
                answer_list.append(targets[idx2][0])
                targets[idx2] = None
        answer.append(answer_list)

    return answer

#groupAnagrams(["eat","tea","tan","ate","nat","bat"])
print(groupAnagrams(["","b"]))