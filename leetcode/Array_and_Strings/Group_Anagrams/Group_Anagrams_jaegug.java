class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> answer = new ArrayList<List<String>>();
        HashMap<String, List<String>> sameLettersMap = new HashMap<>();
        for(int i=0; i<strs.length; i++){
            char[] toCharArr = strs[i].toCharArray(); //정렬을 위해 Char 배열로 변환
            Arrays.sort(toCharArr);
            String sortedStr = new StringBuilder(new String(toCharArr)).toString();
             //정렬한 String을 key로 HashMap의 List에 동일한 문자를 가진 String 추가
            List<String> sameLettersList = 
                sameLettersMap.get(sortedStr)==null?new ArrayList<>():sameLettersMap.get(sortedStr);
            sameLettersList.add(strs[i]);
            sameLettersMap.put(sortedStr, sameLettersList);
        }
        for(String key : sameLettersMap.keySet()){
            answer.add(sameLettersMap.get(key));
        }
        return answer;
    }
}
