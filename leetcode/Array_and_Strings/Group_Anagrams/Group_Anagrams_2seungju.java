import java.util.*;

class Solution {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> result = new HashMap<>();
        for (String str : strs) {
            int[] check = new int[26];

            for (char c : str.toCharArray()) {
                check[c - 'a']++;
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int value : check) {
                stringBuilder.append(value).append(",");
            }

            String key = stringBuilder.toString();
            if (!result.containsKey(key)) {
                result.put(key, new LinkedList<>());
            }

            result.get(key).add(str);
        }

        return new LinkedList<>(result.values());
    }
}