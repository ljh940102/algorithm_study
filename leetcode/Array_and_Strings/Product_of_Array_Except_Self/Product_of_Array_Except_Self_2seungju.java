class Solution {
    public int[] productExceptSelf(int[] nums) {
        int[] right = new int[nums.length];
        for(int i = 0; i < nums.length; i++) {
            if (i == 0) {
                right[i] = 1;
                continue;
            }

            right[i] = right[i - 1] * nums[i - 1];
        }

        int[] left = new int[nums.length];
        for(int i = nums.length - 1; i >= 0; i--) {
            if (i == nums.length - 1) {
                left[i] = 1;
                continue;
            }

            left[i] = left[i + 1] * nums[i + 1];
        }

        int[] result = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            result[i] = left[i] * right[i];
        }

        return result;
    }
}