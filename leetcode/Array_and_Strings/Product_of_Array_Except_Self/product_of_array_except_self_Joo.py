class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        rtn = [1] * len(nums)
        for idx in range(1, len(nums)):  # left to right
            rtn[idx] = nums[idx - 1] * rtn[idx - 1]

        for idx in range(len(nums) - 2, -1, -1):
            rtn[idx] *= nums[idx + 1]
            nums[idx] *= nums[idx + 1]

        return rtn