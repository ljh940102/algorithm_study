from typing import List


class Solution:
	def productExceptSelf(self, nums: List[int]) -> List[int]:
		length = len(nums)

		answer = [1] * length
		for idx, num in enumerate(nums[:-1]):
			answer[idx + 1] = answer[idx] * num

		backward = nums[-1]
		for idx in range(length - 2, -1, -1):
			answer[idx] *= backward
			backward *= nums[idx]


# print(Solution().productExceptSelf([4, 2, 3, 5]))
