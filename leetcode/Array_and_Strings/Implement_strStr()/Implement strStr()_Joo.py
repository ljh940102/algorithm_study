"""
0 <= haystack.length, needle.length <= 5 * 104
두개변수 모두 "" 일수 있음을 유의
haystack and needle consist of only lower-case English characters.
"""
""" Try 1. 
-> "hlllello" "llo" 구별 못함

char 단위로 비교하면서 틀리면 첫시작위치+1로 돌아가서 다시탐색 -> 무조건 시간오버로 보고 버림 

search_idx = 0
start_idx = 0

for target in haystack:
    if needle[search_idx] == target:
        search_idx += 1
        continue

    if search_idx != 0:
        return -1
"""
""" Best
-> 이런 함수 쓰기 있기?!?!

return haystack.find(needle)
또는
try:
    return haystack.index(needle)
except:
    return -1
"""

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:

        # 특이케이스 처리 rule 처리
        if needle == "":
            return 0

        if len(needle) > len(haystack):
            return -1

        if len(needle) == len(haystack):
            if needle == haystack:
                return 0
            else:
                return -1

        win_size = len(needle)
        criteria = len(haystack) - win_size

        for start_idx in range(criteria + 1):  # 기준점까지만 비교
            if haystack[start_idx:start_idx + win_size] == needle:
                return start_idx
        return -1

"""
print(strStr("0123456789","3456"))
print(strStr("0123456789","2456"))
print(strStr("hello","ll"))
print(strStr("aaaaa","bba"))
print(strStr("","1"))
print(strStr("1",""))
"""