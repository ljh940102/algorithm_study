class Solution {
    public int strStr(String haystack, String needle) {
    	int needle_length = needle.length();
    	if(needle_length == 0) {
    		return 0;
    	}
    	int haystack_length = haystack.length();
    	
        for(int i = 0; i<=haystack_length - needle_length; i++) {
        	int j = 0;
        	for(; j<needle_length; j++) {
        		char h = haystack.charAt(i+j);
        		char n = needle.charAt(j);
        		if(h!=n) {
        			break;
        		}
        	}
        	if(j==needle_length) {
        		return i;
        	}
        }
        // 여기까지 넘어온 이상 무조건 -1 리턴
        return -1;
    }
}
