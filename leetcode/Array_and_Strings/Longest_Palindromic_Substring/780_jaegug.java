class Solution {
    
    int start, maxLen;
    
    public String longestPalindrome(String s) {
        if(s.length()<2){
            return s;
        }
        
        for(int i=0; i < s.length()-1; i++){
            searchPalindrome(s,i,i); // 홀수 회문
            searchPalindrome(s,i,i+1); // 짝수 회문
        }
        return s.substring(start,start+maxLen);
    }
    
    public void searchPalindrome(String s, int i, int j){
        while(i >= 0 && j < s.length() && s.charAt(i) == s.charAt(j)){
            i--;
            j++;
        }
        if(maxLen < j-i-1){
            start = i+1;
            maxLen = j-i-1;
        }
    }
}
