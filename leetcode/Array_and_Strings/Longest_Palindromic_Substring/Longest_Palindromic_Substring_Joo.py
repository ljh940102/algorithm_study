class Solution:
    def longestPalindrome(self, s: str) -> str:
        ans = ""
        curr_max = 0
        n = len(s)
        for idx in range(len(s)):
            # 홀수 회문 조사
            left, right = idx, idx
            while True:
                if (left >= 0 and right < n) and s[left] == s[right]:
                    if len(s[left:right + 1]) > curr_max:  # 현재 ans보다 긴 경우
                        ans = s[left:right + 1]
                        curr_max = len(ans)
                    left -= 1
                    right += 1
                else:
                    break

            # 짝수 회문 조사
            left, right = idx, idx+1
            while True:
                if (left >= 0 and right < n) and s[left] == s[right]:
                    if len(s[left:right + 1]) > curr_max:  # 현재 ans보다 긴 경우
                        ans = s[left:right + 1]
                        curr_max = len(ans)
                    left -= 1
                    right += 1
                else:
                    break

        return ans

print(Solution.longestPalindrome(Solution, "babad"))
# print(Solution.longestPalindrome("aacabdkacaa"))