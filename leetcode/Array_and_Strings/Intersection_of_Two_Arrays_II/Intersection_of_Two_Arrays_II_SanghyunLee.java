import java.util.*;


class Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
    	Arrays.sort(nums1);
    	Arrays.sort(nums2);
    	
        int ptr1 = 0;
        int ptr2 = 0;
        int len1 = nums1.length;
        int len2 = nums2.length;
        ArrayList<Integer> intersection_array = new ArrayList<>();
        while(ptr1 < len1 && ptr2 < len2) {
        	if(nums1[ptr1] < nums2[ptr2]) {
        		ptr1++;
        	}
        	else if(nums1[ptr1] > nums2[ptr2]) {
        		ptr2++;
        	}
        	else {  // nums1[ptr1] == nums2[ptr2]
        		intersection_array.add(nums1[ptr1]);
        		ptr1++;
        		ptr2++;
        	}
        }
        Collections.sort(intersection_array);
        int intersection_list[] = new int[intersection_array.size()];
        int idx = 0;
        for(int num : intersection_array) {
        	intersection_list[idx++] = num;
        }
        return intersection_list;
    }
}
