class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> answer = new LinkedList<List<Integer>>();
        
        Arrays.sort(nums);
        
        for (int i = 0; i < nums.length - 2; i++) {
            if (i > 0 && nums[i] == nums[i-1]){
                continue; //중복값은 계산 생략
            } 
            
            int left = i + 1; //왼쪽부터 탐색
            int right = nums.length - 1; //오른쪽부터 탐색
            
            while (left < right) { //두 탐색점이 만날 때까지
                int sum = nums[i] + nums[left] + nums[right];
                
                if (sum == 0) {
                    answer.add(Arrays.asList(nums[i], nums[left], nums[right])); //합이 0이면 answer에 추가
                    left++;
                    right--;
                    while (nums[left] == nums[left - 1] && left < right){//중복값은 계산 생략
                        left++;
                    } 
                    while (nums[right] == nums[right + 1] && left < right){//중복값은 계산 생략
                        right--;
                    } 
                } else if (sum > 0) { //합이 양수이면 큰값인 right를 감소
                    right--;
                } else if (sum < 0) { //합이 음수이면 작은값인 left를 증가
                    left++;
                }
            }
        }
        return answer;
    }
}
