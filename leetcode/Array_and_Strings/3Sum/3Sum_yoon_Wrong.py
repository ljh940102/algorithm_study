from collections import defaultdict
from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        answer = []
        check_duplicate = set()
        for i in range(len(nums)):
            num_dict = defaultdict(int)
            for j in range(len(nums)):
                if i != j:
                    num_dict[nums[j]] += 1
            for j in range(len(nums)):
                if i != j:
                    to_find = -(nums[j] + nums[i])
                    if to_find in num_dict:
                        if num_dict[to_find] != 0:
                            ans_tuple = (nums[i], nums[j], -(nums[j] + nums[i]))
                            if ans_tuple not in check_duplicate:
                                answer.append(ans_tuple)
                                check_duplicate.add(ans_tuple)
                                num_dict[to_find] -=1
        return answer

a = Solution()
nums = [-1,0,1,2,-1,-4]
print(a.threeSum(nums))