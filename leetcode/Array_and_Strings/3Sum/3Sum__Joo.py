class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        answer = set()

        if len(nums) < 3:
            return []

        nums.sort()
        for idx, target in enumerate(nums[:-2]):
            left, right = idx+1, len(nums)-1
            while left < right:
                if nums[idx] + nums[left] + nums[right] == 0:  # hit
                    answer.add((nums[idx], nums[left], nums[right]))
                    left += 1
                elif nums[idx] + nums[left] + nums[right] < 0:  # 0보다 작은 경우 -> left 이동
                    left += 1
                else:  # 0보다 큰 경우 -> rigth 이동
                    right -= 1

        return list(answer)