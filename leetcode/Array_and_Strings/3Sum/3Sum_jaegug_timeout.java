class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        boolean[] visited = new boolean[nums.length];
        List<List<Integer>> answer = new ArrayList<List<Integer>>();
        combination(nums, visited, 0, nums.length, 3, answer);    
        return answer;
    }
    
    //모든 조합 구하기
    static void combination(int[] arr, boolean[] visited, int start, int n, int r, List<List<Integer>> answer) {
        if(r == 0) {
            int sum = 0;
            List<Integer> comb = new ArrayList<Integer>();
            for (int i = 0; i < n; i++) {
                if (visited[i]) {
                    sum += arr[i];
                    comb.add(arr[i]);
                }
            }
            if(sum==0 && !answer.contains(comb)){
                answer.add(comb);
            }
            return;
        } 
        for(int i=start; i<n; i++) {
            visited[i] = true;
            combination(arr, visited, i + 1, n, r - 1, answer);
            visited[i] = false;
        }
    }
}
