import java.util.*;

class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums.length < 3) {
            return new ArrayList<>();
        }

        List<List<Integer>> result = new ArrayList<>();

        Arrays.sort(nums);
        for (List<Integer> combination : getCombination(nums.length)) {
            int sum = 0;
            for (int value : combination) {
                sum += nums[value];
            }

            if (sum == 0) {
                List<Integer> combinationList = new ArrayList<>();

                for (int value : combination) {
                    combinationList.add(nums[value]);
                }

                if (!result.contains(combinationList)) {
                    result.add(combinationList);
                }
            }
        }

        return result;
    }

    private List<List<Integer>> getCombination(int length) {
        List<List<Integer>> list = new ArrayList<>();
        func(0, 0, length - 1, Arrays.asList(0, 0, 0), list);

        return list;
    }

    private void func(int index, int start, int end, List<Integer> data, List<List<Integer>> list) {
        if (index == 3) {
            List<Integer> combination = new ArrayList<>(data);
            list.add(combination);

        } else if (start <= end) {
            data.set(index, start);

            func(index + 1, start + 1, end, data, list);
            func(index, start + 1, end, data, list);
        }
    }
}