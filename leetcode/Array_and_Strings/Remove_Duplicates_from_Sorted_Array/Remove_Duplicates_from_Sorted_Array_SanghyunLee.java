class Solution {
    public int removeDuplicates(int[] nums) {
        int len = nums.length;
        int read_idx = 0;
        int write_idx = 0;
        while(read_idx + 1 < len) {
        	if(nums[read_idx] != nums[read_idx + 1]) {
        		write_idx++;
        		nums[write_idx] = nums[read_idx + 1];
        	}
        	read_idx++;
        }
        return write_idx + 1;
    }
}
