import sys

def increasingTriplet(nums):
    length = len(nums)
    first, second = sys.maxsize, sys.maxsize

    for idx in range(length):
        curr = nums[idx]
        
        # curr가 저장된 값보다 작으면 교체 -> incresing이 아니면 True 도달 불가
        if first >= curr:
            first = curr
        elif second >= curr:
            second = curr
        else:
            return True
    return False
