class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
      
        minimum = float('inf')
        mid = float('inf')
        for num in nums:
            if num > minimum and num > mid:
                return True
            if num < minimum:
                minimum = num
            elif num < mid and num != minimum:
                mid = num
        return False
            