class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        intervals = sorted(intervals)
        result_list = [[-1,-1]]
        for start, end in intervals:
            cur_start, cur_end = result_list[-1]
            if start == cur_start:
                end = max(end, cur_end)
                result_list[-1] = [start,end]
            elif start > cur_start:
                if cur_end >= start:
                    end = max(end, cur_end)
                    result_list[-1] = [cur_start,end]
                else:
                    result_list.append([start, end])
        result_list = result_list[1:len(result_list)]
        return result_list
                
