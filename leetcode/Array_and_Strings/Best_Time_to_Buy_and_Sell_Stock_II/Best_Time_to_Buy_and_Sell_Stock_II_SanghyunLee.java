class Solution {
    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1])
                maxProfit += prices[i] - prices[i - 1];
        }
        
        return maxProfit;
    }
}
/*
 * 매우 느림 위의 빠른 정답 참조할 것
class Solution {
	public int maxProfit(int[] prices) {
        int day = 0;
        int len = prices.length;
        boolean hold = false;
        int buy_price = -1;
        int profit = 0;

        while(day + 1 < len) {
        	int today = prices[day];
        	int tomorrow = prices[day + 1];
        	if(today > tomorrow && hold) {
        		hold = false;
        		profit += (today - buy_price);
        	}
        	else if(today < tomorrow && !hold) {
        		hold = true;
        		buy_price = today;
        	}
        	System.out.println(hold);
        	day++;
        }
        if(hold) {
        	profit += (prices[len - 1] - buy_price);
        }
        return profit;
    }
}
*/
