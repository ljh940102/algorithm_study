import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    public static int sherlockAndAnagrams(String s) {

        ArrayList<Integer> list = new ArrayList<>();
                
        for(int i = 0; i < s.length(); i++) {//Character -> Integer
            list.add(s.charAt(i) - 'a' + 1);
        }
        
        HashMap<HashMap<Integer, Integer>, Integer> setMap = new HashMap<>();
        
        for(int i = 1; i < s.length()+1; i++) {
            for(int j = 0; j + i -1 < s.length(); j++) {
                
                HashMap<Integer, Integer> set = new HashMap<>();
                
                for(int k = j; k < j+i; k++) {//HashMap<subString의 letter, letter 개수>
                    set.put(list.get(k), set.getOrDefault(list.get(k), 0)+1);
                }
                setMap.put(set, setMap.getOrDefault(set, 0)+1);
            }
        }
        int answer
            = setMap.entrySet().stream()
                    .filter(entry -> entry.getValue() > 1)
                    .collect(Collectors.summingInt(entry -> numOfCombination(entry.getValue()))); //nC2
        
        return answer;
    }
    
    private static int numOfCombination(int num) {
        return num*(num-1)/2;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, q).forEach(qItr -> {
            try {
                String s = bufferedReader.readLine();

                int result = Result.sherlockAndAnagrams(s);

                bufferedWriter.write(String.valueOf(result));
                bufferedWriter.newLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
        bufferedWriter.close();
    }
}
