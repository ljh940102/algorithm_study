import math
import os
import random
import re
import sys
from collections import defaultdict

def sherlockAndAnagrams(s):
    substrings_cnt = defaultdict(int)

    for idx in range(1, len(s)):
        for idx_start in range(len(s) - idx + 1):
            sorted_str = "".join(sorted(s[idx_start:idx_start+idx]))
            substrings_cnt[sorted_str] += 1

    ans = 0
    for _, count in substrings_cnt.items():
        ans += count*(count-1) // 2

    return ans