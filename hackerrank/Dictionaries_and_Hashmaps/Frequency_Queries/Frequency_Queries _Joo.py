#!/bin/python3

import math
import os
import random
import re
import sys
from collections import defaultdict

def freqQuery(queries):
    """
    dict 2개로 숫자 존재여부, 특정 카운터의 존재여부 관리
    num2count => 등장한 num과 해당 num의 count
    count2frequency => 특정 num count의 개수 (Ex. count가 3인 num이 5개 존재한다면, count2frequency[3] = 5)
    """
    rtn = []
    num2count, count2frequency = defaultdict(int), defaultdict(int)
    for query in queries:
        command, num = query

        if command == 1:  # insert
            if count2frequency.get(num2count[num], 0) != 0:
                count2frequency[num2count[num]] -= 1

            num2count[num] += 1

            count2frequency[num2count[num]] += 1

        elif command == 2:  # delete
            if num2count.get(num, 0) == 0:  # 존재하지 않으면 skip
                continue

            count2frequency[num2count[num]] -= 1

            num2count[num] -= 1

            count2frequency[num2count[num]] += 1

        elif command == 3:  # check
            if count2frequency.get(num, 0) != 0:
                rtn.append(1)
            else:
                rtn.append(0)

    return rtn


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    fptr.write('\n'.join(map(str, ans)))
    fptr.write('\n')

    fptr.close()
