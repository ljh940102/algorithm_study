import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    // Complete the freqQuery function below.
    static List<Integer> freqQuery(List<List<Integer>> queries) {
        List<Integer> answer = new ArrayList<>();
        HashMap<Integer, Integer> frequencies = new HashMap<>();
        
        queries.stream().forEach(query -> {
            int queryId = query.get(0);
            int item = query.get(1);
            
            if(queryId == 1){
                frequencies.put(item, frequencies.getOrDefault(item, 0)+1);
            }else if(queryId == 2) {
                int cnt = frequencies.getOrDefault(item, 0) - 1;
                if(cnt == -1){
                    cnt = 0;
                }
                frequencies.put(item, cnt);
            }else if(queryId == 3){
                boolean check = frequencies.entrySet().stream()
                                    .anyMatch(frequency -> frequency.getValue() == item);
                answer.add(check ? 1 : 0);
            }
        });
        
        return answer;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> queries = new ArrayList<>();

        for(int i = 0 ; i < q ; i++) {
            String[] query = bufferedReader.readLine().split(" ");
            List<Integer> tmpQ = new ArrayList<>();
            tmpQ.add(Integer.parseInt(query[0]));
            tmpQ.add(Integer.parseInt(query[1]));
            queries.add(tmpQ);
        }

        List<Integer> ans = freqQuery(queries);

        bufferedWriter.write(
            ans.stream()
                .map(Object::toString)
                .collect(joining("\n")) + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
