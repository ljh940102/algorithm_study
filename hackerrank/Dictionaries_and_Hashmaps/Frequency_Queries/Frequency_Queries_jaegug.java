import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class Solution {

    static List<Integer> freqQuery(List<int[]> queries) {
        Map<Integer, Integer> frequencies = new HashMap<>();
        int[] reverseArr = new int[queries.size() + 1];
        List<Integer> answer = new ArrayList<>();
        int frequency, queryId, item;

        for (int[] query : queries) {
            queryId = query[0];
            item = query[1];
            if(queryId == 1){
                frequency = frequencies.getOrDefault(item, 0);
                frequencies.put(item, frequency+1);
                reverseArr[frequency]--;
                reverseArr[frequency+1]++;
            }else if(queryId == 2){
                frequency = frequencies.getOrDefault(item, 0);
                if(frequency > 0){
                    frequencies.put(item, frequency-1);
                    reverseArr[frequency]--;
                    reverseArr[frequency-1]++;
                }
            }else if(queryId == 3){
                if(item > reverseArr.length) answer.add(0);
                else answer.add(reverseArr[item] > 0 ? 1 : 0);
            }
        }
        return answer;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = Integer.parseInt(bufferedReader.readLine().trim());
        List<int[]> queries = new ArrayList<>();

        for(int i=0; i<q; i++){
            String[] row = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
            int queryId = Integer.parseInt(row[0]);
            int item = Integer.parseInt(row[1]);
            int[] query = new int[2];
            query[0] = queryId;
            query[1] = item;
            queries.add(query);
        }

        List<Integer> ans = freqQuery(queries);

        bufferedWriter.write(
                ans.stream()
                        .map(Object::toString)
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
