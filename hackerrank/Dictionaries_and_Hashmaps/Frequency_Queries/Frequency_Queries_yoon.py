#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the freqQuery function below.
from collections import defaultdict
def freqQuery(queries):
    result = []
    num_dict = defaultdict(int)
    frequency_dict = defaultdict(dict)
    for oper, num in queries:
        if oper == 1:
            frequency_dict[num_dict[num]].pop(num,None)
            num_dict[num] += 1
            frequency_dict[num_dict[num]][num] = 1
        elif oper == 2:
            if num_dict[num] != 0:
                frequency_dict[num_dict[num]].pop(num,None)
                num_dict[num] -= 1
                frequency_dict[num_dict[num]][num] = 1
        elif oper == 3:
            if len(frequency_dict[num]) > 0:
                result.append(1)
            else:
                result.append(0)
    return result

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)

    fptr.write('\n'.join(map(str, ans)))
    fptr.write('\n')

    fptr.close()
