import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    static List<Integer> freqQuery(List<int[]> queries) {
        List<Integer> result = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        int[] counts = new int[queries.size() + 1];
        int count;

        for (int[] query : queries) {
            int action = query[0];
            int element = query[1];

            if (action == 1) {
                count = map.getOrDefault(element, 0) + 1;
                map.put(element, count);
                counts[count]++;
                counts[count - 1]--;

            } else if (action == 2) {
                count = map.getOrDefault(element, 0) - 1;
                if (count > -1) {
                    map.put(element, count);
                    counts[count]++;
                    counts[count + 1]--;

                }

            } else if (action == 3) {
                if (element > counts.length) {
                    result.add(0);
                    continue;
                }

                if (counts[element] > 0) {
                    result.add(1);
                } else {
                    result.add(0);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int q = Integer.parseInt(bufferedReader.readLine().trim());
        List<int[]> queries = new ArrayList<>();

        for(int i=0; i<q; i++){
            String[] row = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
            int cmd = Integer.parseInt(row[0]);
            int v = Integer.parseInt(row[1]);
            int[] query = new int[2];
            query[0] = cmd;
            query[1] = v;
            queries.add(query);
        }

        List<Integer> ans = freqQuery(queries);

        bufferedWriter.write(
                ans.stream()
                        .map(Object::toString)
                        .collect(joining("\n"))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
