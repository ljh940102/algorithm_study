import math
import os
import random
import re
import sys
from collections import defaultdict

def countTriplets(arr, r):
    rtn = 0
    num2, num3 = defaultdict(int), defaultdict(int)

    for num in arr:
        if num in num3:
            rtn += num3[num]

        if num in num2:
            num3[num * r] += num2[num]

        num2[num * r] += 1

    return rtn