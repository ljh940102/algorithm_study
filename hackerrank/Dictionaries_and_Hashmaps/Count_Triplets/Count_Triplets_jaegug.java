import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    static long countTriplets(List<Long> arr, long r) {//사실은 arr를 한번 sort 해야되지 않을까? 일단 테스트케이스는 모두 sort 된 데이터
        long ans = 0;
        
        Map<Long, Long> mapForStart = new HashMap<>();//first indice Cnt (start, x, x)
        Map<Long, Long> mapForMiddle = new HashMap<>();//second indice Cnt (x, middle, x)

        for(long num : arr) {
            if(num % r == 0) { //num이 r로 나누어질 때 (prev가 존재할 수 있을 때)
                
                long prev = num / r;
                
                ans += mapForMiddle.getOrDefault(prev, 0L); //이전 값이 mapForMiddle에 있으면 현재 num이 마지막 indice

                mapForMiddle.put(num, mapForMiddle.getOrDefault(num, 0L) + mapForStart.getOrDefault(prev, 0L)); // MiddleCnt = StartCnt + MiddleCnt
            }
            
            mapForStart.put(num, mapForStart.getOrDefault(num, 0L) + 1); //모든 num은 첫번째 인자가 될 수 있는 가능성을 지님
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(nr[0]);

        long r = Long.parseLong(nr[1]);

        List<Long> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Long::parseLong)
            .collect(toList());

        long ans = countTriplets(arr, r);

        bufferedWriter.write(String.valueOf(ans));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
