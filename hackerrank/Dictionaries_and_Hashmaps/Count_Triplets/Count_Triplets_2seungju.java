import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    // Complete the countTriplets function below.
    private static long countTriplets(List<Long> arr, long r) {
        long ans = 0;

        Map<Long, Long> startMap = new HashMap<>();
        Map<Long, Long> medianMap = new HashMap<>();
        for(long num : arr) {
            if(num % r == 0) {
                long prev = num / r;
                Long cntForEnd = medianMap.get(prev);
                if(cntForEnd != null) {
                    ans += cntForEnd;
                }

                Long cntForMiddle = startMap.get(prev);
                if(cntForMiddle != null) {
                    medianMap.put(num, medianMap.getOrDefault(num, 0L) + cntForMiddle);
                }
            }

            startMap.put(num, startMap.getOrDefault(num, 0L) + 1);
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(nr[0]);

        long r = Long.parseLong(nr[1]);

        List<Long> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Long::parseLong)
                .collect(toList());

        long ans = countTriplets(arr, r);

        bufferedWriter.write(String.valueOf(ans));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
