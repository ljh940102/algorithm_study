import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Solution {

    // hidden 다 틀림
    static long countTriplets(List<Long> arr, long r) {
        HashMap<Long, Long> tmp = new HashMap<>();
        
        arr.stream().forEach(item -> tmp.put(item, tmp.getOrDefault(item, 0L)+1));
        
        return tmp.entrySet().stream().mapToLong(item -> mapping(r, item, tmp)).sum();
    }

    private static long mapping(long r, Entry item, HashMap<Long,Long> tmp){
        long key = item.getKey();
        if(tmp.get(key * r) != null && tmp.get(key * r * r) != null){
            return item.getValue() * tmp.get(key * r) * tmp.get(key * r * r);   
        }else{
            return 0L;
        }
    )

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(nr[0]);

        long r = Long.parseLong(nr[1]);

        List<Long> arr = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Long::parseLong)
            .collect(toList());

        long ans = countTriplets(arr, r);

        bufferedWriter.write(String.valueOf(ans));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
