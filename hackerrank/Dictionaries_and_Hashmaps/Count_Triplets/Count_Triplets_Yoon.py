from collections import defaultdict
# Complete the countTriplets function below.
def countTriplets(arr, r):
        count = 0
        dict = defaultdict(int)
        dictPairs = defaultdict(int)

        for i in reversed(arr):
            target = i*r
            if target in dictPairs:
                count += dictPairs[target]
            if target in dict:
                dictPairs[i] += dict[target] 
            dict[i] += 1

        return count