import java.util.*;

class Player {
	String name;
	int score;

	Player(String name, int score) {
		this.name = name;
		this.score = score;
	}
}

class Checker implements Comparator<Player> {
  	// complete this method
	public int compare(Player a, Player b) {
        if(a.score == b.score){
            int idx = 0;
            String nameA = a.name;
            String nameB = b.name;
            while(idx < Math.min(nameA.length(), nameB.length())){
                char charA = nameA.charAt(idx);
                char charB = nameB.charAt(idx);
                if(charA == charB){
                    idx++;
                }else {
                    return nameA.charAt(idx) - nameB.charAt(idx);
                }
            } 
            return nameA.length() - nameB.length();
        }else{
            return b.score - a.score;    
        }
    }
}

