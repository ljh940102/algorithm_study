import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {
    public static int activityNotifications(int[] expenditure, int d) {
        int result = 0;
        int[] counts = new int[201];

        for (int i = 0; i < d; i++) {
            counts[expenditure[i]]++;
        }

        for (int i = d; i < expenditure.length; i++) {
            if (expenditure[i] >= getMedian(counts, d) * 2) {
                result++;
            }

            counts[expenditure[i - d]]--;
            counts[expenditure[i]]++;
        }

        return result;
    }

    private static double getMedian(int[] counts, int d) {
        double median = 0;
        int index = 0;

        if (d % 2 != 0) {
            for (int i = 0; i < counts.length; i++) {
                index += counts[i];
                if (index > d / 2) {
                    median = i;
                    break;
                }
            }
        } else {
            int first = 0;
            int second = 0;

            for (int i = 0; i < counts.length; i++) {
                index += counts[i];
                if (first == 0 && index >= d / 2) {
                    first = i;
                }

                if (index >= d / 2 + 1) {
                    second = i;
                    break;
                }
            }

            median = (first + second) / 2.0;
        }

        return median;
    }

}

public class Solution {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] expenditure = new int[n];

        String[] expenditureItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int expenditureItem = Integer.parseInt(expenditureItems[i]);
            expenditure[i] = expenditureItem;
        }

        int result = Result.activityNotifications(expenditure, d);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
