#!/bin/python3

import math
import os
import random
import re
import sys
from collections import deque
"""
처음에는 heap 2개를 이용해 median을 빨치 찾는 방법으로 시도 -> d이상의 수가 들어온 경우 oldest를 버리는게 불가능
계수정렬로 해결 
"""
def find_former_idx(counting_sort, idx):
    """idx보다 작은 dix에서 counting이 0이 아닌 idx 중 가장 큰 인덱스 리턴"""
    rtn = 0
    for target_idx in range(idx - 1, -1, -1):
        if counting_sort[target_idx]:
            rtn = target_idx
            break
    return rtn


def find_median(counting_sort, boundry, is_odd):
    check_sum = 0
    idx = 0
    while check_sum <= boundry:
        check_sum += counting_sort[idx]
        idx += 1

    if is_odd:
        return idx - 1
    else:  # 짝수인 경우
        if counting_sort[idx - 1] == 1:  # boundry에 해당하는 counting이 1이면 이전 idx와의 평균 리턴
            another_median = find_former_idx(counting_sort, idx - 1)
        else:
            if boundry == check_sum - counting_sort[idx - 1]:  # boundry가 평균값중 큰 값인 경우
                another_median = find_former_idx(counting_sort, idx - 1)
            else:
                another_median = idx - 1

        return (another_median + idx - 1) / 2


def activityNotifications(expenditure, d):
    rtn = 0
    counting_sort = [0] * 201
    trailing_days = deque()

    boundry = d // 2  # 중앙값의 index

    if d % 2 == 1:
        is_odd = True
    else:
        is_odd = False

    for idx in range(d):
        counting_sort[expenditure[idx]] += 1
        trailing_days.append(expenditure[idx])

    for idx in range(d, len(expenditure)):

        median = find_median(counting_sort, boundry, is_odd)
        """
        print(trailing_days)
        print(counting_sort)
        print(median)
        """

        if expenditure[idx] >= median * 2:
            rtn += 1

        counting_sort[expenditure[idx]] += 1
        trailing_days.append(expenditure[idx])

        pop_num = trailing_days.popleft()
        counting_sort[pop_num] -= 1

    return rtn


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    d = int(first_multiple_input[1])

    expenditure = list(map(int, input().rstrip().split()))

    result = activityNotifications(expenditure, d)

    fptr.write(str(result) + '\n')

    fptr.close()
