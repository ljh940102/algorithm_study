from bisect import bisect_left, insort


def median(sorted_list, len_):
	mid, remainder = divmod(len_, 2)
	if remainder:
		median = sorted_list[mid]
	else:
		median = (sorted_list[mid - 1] + sorted_list[mid]) / 2
	return median


def activityNotifications(expenditure, days):
	answer = 0
	del_idx = 0
	sorted_list = sorted(expenditure[:days])

	for num in expenditure[days:]:

		# compare with median
		if num >= 2 * median(sorted_list, days):
			answer += 1

		# pop from head
		del sorted_list[bisect_left(sorted_list, expenditure[del_idx])]
		del_idx += 1

		# push to tail (insort: insert into sorted list)
		insort(sorted_list, num)


	return answer


# print(activityNotifications(expenditure=[10, 20, 30, 40, 50], d=3))
# print(activityNotifications(expenditure=[2, 3, 4, 2, 3, 6, 8, 4, 5], d=5))
