import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'activityNotifications' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY expenditure
     *  2. INTEGER d
     */

    public static int activityNotifications(List<Integer> expenditure, int d) {
    // Write your code here
        int count = 0;
        int[] tmpArr = new int[201];
        
        for(int i = 0 ; i < d ; i++) {
            tmpArr[expenditure.get(i)]++;
        }

        for(int i = d ; i < expenditure.size() ; i++) {
            double median = CalMedian(tmpArr, d); 
            
            if(median*2 <= expenditure.get(i)) {
                count++;
            }
            
            tmpArr[expenditure.get(i-d)]--;
            tmpArr[expenditure.get(i)]++;
        }
        return count;
    }
    
    static double CalMedian(int[] tmpArr, int d) {
        
        int count = 0;
        double result = 0;
        
        if(d%2 != 0) {
            
            for(int i = 0 ; i < tmpArr.length ; i++) {
                count += tmpArr[i];
                
                if(count > d/2) {
                    result = i;
                    break;
                }
            }
        } else {
            
            int tmpCnt1 = 0;
            int tmpCnt2 = 0;
            
            for(int i = 0 ; i < tmpArr.length ; i++) {
                count += tmpArr[i];
                
                if(tmpCnt1 == 0 && count >= d/2) {
                    tmpCnt1 = i;
                }
                if(tmpCnt2 == 0 && count >= d/2+1) {
                    tmpCnt2 = i;
                    break;
                }
            }
            result = (tmpCnt1+tmpCnt2) /2.0;
        }
        return result;
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int d = Integer.parseInt(firstMultipleInput[1]);

        List<Integer> expenditure = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        int result = Result.activityNotifications(expenditure, d);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
