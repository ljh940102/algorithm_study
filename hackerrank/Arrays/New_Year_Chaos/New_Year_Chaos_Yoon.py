def minimumBribes(q):
    # Write your code here
    result = 0
    for i,num in enumerate(q):
        bribe_num = num - i - 1
        if bribe_num > 2:
            print("Too chaotic")
            return
        for j in range(max(num-2,0),i):
            if q[j] > num - 1:
                result += 1
    print(result)
    return