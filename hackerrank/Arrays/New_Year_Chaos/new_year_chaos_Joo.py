#!/bin/python3

import math
import os
import random
import re
import sys


def minimumBribes(q):
    rtn = 0

    q_info = dict()
    for idx, num in enumerate(q):  # 숫자의 현재 인덱스 저장
        q_info[num] = idx

    for target_num in range(len(q), 0, -1):  # 가장 큰 수부터 역으로 접근(중요)
        target_idx = q_info[target_num]

        if target_idx == target_num - 1:  # 원래 위치인 경우 skip
            continue

        gap = abs(target_idx - (target_num - 1))  # 원래 위치와의 gap

        if gap > 2:
            return "Too chaotic"

        for _ in range(gap):
            rtn += 1

            back_num = q[target_idx + 1]

            q[target_idx], q[target_idx + 1] = q[target_idx + 1], q[target_idx]
            q_info[target_num], q_info[back_num] = target_idx + 1, target_idx

            target_idx += 1

    return rtn