import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import java.util.concurrent.atomic.AtomicInteger; 
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'minimumBribes' function below.
     *
     * The function accepts INTEGER_ARRAY q as parameter.
     */

    public static void minimumBribes(List<Integer> q) {
    // Write your code here              
        AtomicInteger idx = new AtomicInteger(); // stream은 index를 사용할 수 없기 때문에 외부에서 선언
        
        boolean chaos = q.stream()
                .filter(person -> idx.getAndIncrement() < person)
                .map(person -> person - idx.get()) 
                .anyMatch(bribe -> bribe >= 3); //뇌물이 3 이상인 값이 있으면 chaotic
        
        if(chaos){
            System.out.println("Too chaotic");
        }else {
            
            PriorityQueue<Integer> priorityQueue 
                = new PriorityQueue<>(Collections.reverseOrder()); // 큰 값이 우선순위가 높은 우선순위 큐
            
            priorityQueue.add(Integer.MAX_VALUE); // 우선순위 큐 초기화
            
            int bribeSum = q.stream()
                .sorted((a,b) -> -1) // stream 뒤집기!
                .mapToInt(person -> { // 뇌물 갯수로 stream 인자를 변환해준다
                    if(priorityQueue.peek() < person){ //큐에 남은 2개의 값보다 person 값이 크면 뇌물 2번
                        return 2;
                    }else{
                        priorityQueue.add(person);
                        if(priorityQueue.size() > 2){ //3개 이상은 chaotic 이기 때문에 2개만 저장해준다.
                            priorityQueue.remove();
                            if(priorityQueue.peek() == person){ // 큐에 남은 최대값이 person과 같다면 뇌물 1번
                                return 1;
                            }
                        }
                    }
                    return 0;})
                .sum(); // 뇌물 갯수 집계
                
            System.out.println(bribeSum);
        }

        //Stream은 매칭, 집계 등의 메서드가 실행되면 closed 되어서 재활용할 수 없다.
        /*
        Stream<Integer> bribeStream 
            = q.stream()
                .filter(person -> idx.getAndIncrement() < person)
                .map(person -> person - idx.get());
               
        boolean chaos = bribeStream.anyMatch(bribe -> bribe >= 3);
         
        Optional<Integer> bribeSum = bribeStream.reduce((x,y) -> x + y);
        */  
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                int n = Integer.parseInt(bufferedReader.readLine().trim());

                List<Integer> q = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());

                Result.minimumBribes(q);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
    }
}
