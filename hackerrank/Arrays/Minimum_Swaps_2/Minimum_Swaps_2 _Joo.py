#!/bin/python3

import math
import os
import random
import re
import sys

def minimumSwaps(arr):
    rtn = 0

    target_idx = 0
    while target_idx < len(arr):
        if arr[target_idx] == target_idx + 1:
            target_idx += 1
        else:
            # 값이 어떤 인덱스로 가야만하는지를 알고 있다는게 포인트
            num = arr[target_idx]
            arr[num - 1], arr[target_idx] = arr[target_idx], arr[num - 1]
            rtn += 1

    return rtn