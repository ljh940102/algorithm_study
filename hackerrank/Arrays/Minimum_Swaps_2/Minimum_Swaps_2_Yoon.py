# Complete the minimumSwaps function below.
def minimumSwaps(arr):
    ref_arr = [i for i in range(min(arr), max(arr) + 1)]
    index_dict = {v: i for i, v in enumerate(arr)}
    swaps = 0

    for i, v in enumerate(arr):
        correct_val = ref_arr[i]
        if v != correct_val:
            loc = index_dict[correct_val]
            arr[i], arr[loc] = arr[loc], arr[i]
            index_dict[v], index_dict[correct_val] = loc, i
            swaps += 1
    return swaps