#!/bin/python3

import math
import os
import random
import re
import sys


#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#

def countingValleys(steps, path):
    # Write your code here
    curr_loc, rtn = 0, 0

    is_in_velly = False
    for direct in path:
        if direct == "U":
            curr_loc += 1
        else:
            curr_loc -= 1

        if curr_loc < 0 and not is_in_velly:
            is_in_velly = True
            rtn += 1
        elif curr_loc >= 0 and is_in_velly:
            is_in_velly = False

    return rtn


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    steps = int(input().strip())

    path = input()

    result = countingValleys(steps, path)

    fptr.write(str(result) + '\n')

    fptr.close()
