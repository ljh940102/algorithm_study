#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'jumpingOnClouds' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY c as parameter.
#

def jumpingOnClouds(c):
    result, cur_loc = 0, 0
    while cur_loc < len(c) - 1:
        if cur_loc < len(c) - 2:
            if c[cur_loc + 2] == 1:
                cur_loc += 1
                result += 1                
            else:
                cur_loc += 2
                result += 1
        else:
            cur_loc += 1
            result += 1        
    return result
    
    # Write your code here

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + '\n')

    fptr.close()
