from collections import Counter, defaultdict

def isValid(s):
    counter = Counter(s)

    count2letter = defaultdict(list)
    check_count = set()
    for letter, count in counter.items():  # count의 개수 확인 및 letter - count swap
        check_count.add(count)
        count2letter[count].append(letter)

    if len(check_count) == 1:  # count 개수가 1이면 무조건 valid
        return "YES"
    elif len(check_count) == 2:  # count 개수가 2이면 판단 필요
        # count 개수가 2일 때, valid 조건은 2개
        #    1. 두 개 중 하나의 count에 해당하는 letter가 1개뿐이면서, count간 차이가 1인 경우.
        #    2. 두 개 중 하나의 count에 해당하는 letter가 1개뿐이면서, count 값이 1이라서 remove되는 경우
        has_one_length, is_count_1 = False, False

        for count, letter_list in count2letter.items():
            if len(letter_list) == 1:  
                has_one_length = True
                if count == 1:
                    is_count_1 = True

        key_list = list(count2letter.keys())
        is_one_diff = (abs(key_list[0] - key_list[1]) == 1)  # count 차이가 1인 경우

        if has_one_length and is_one_diff:  # 1번 케이스
            return "YES"
        elif has_one_length and is_count_1:  # 2번 케이스
            return "YES"
        else:
            return "NO"
    else:  # count 개수가 1과 2 모두 아니면 무조건 invalid
        return "NO"
