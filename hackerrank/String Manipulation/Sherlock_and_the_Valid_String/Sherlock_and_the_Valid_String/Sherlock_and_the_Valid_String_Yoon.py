from collections import Counter

def isValid(s):

    char_dict = Counter(s)
    min_count = float('inf')
    max_count = 0
    # count how many times a count occured
    count_dict = {}
    for char, value in char_dict.items():
        if value in count_dict:
            count_dict[value] += 1
        else:
            count_dict[value] = 1
        #also update max and min count
        if value < min_count:
            min_count = value
        if value > max_count:
            max_count = value
    
    if len(count_dict) == 1:
        return 'YES'
    elif len(count_dict) == 2:
        if count_dict[max_count] == 1 and max_count - min_count == 1:
            return 'YES'
        elif count_dict[min_count] == 1 and min_count == 1:
            return 'YES'
    return 'NO'
