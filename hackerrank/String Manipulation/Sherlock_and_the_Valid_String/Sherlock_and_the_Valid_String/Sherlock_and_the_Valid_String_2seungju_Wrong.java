import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'isValid' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String isValid(String s) {
        int[] stringCounts = new int[26];
        for (int i = 0; i < s.length(); i++) {
            stringCounts[s.charAt(i) - 97]++;
        }

        int[] counts = new int[100001];
        for (int stringCount : stringCounts) {
            if (stringCount == 0) {
                continue;
            }

            counts[stringCount]++;
        }

        int limit = 2;
        List<Integer> keyList = new ArrayList<>();
        List<Integer> valueList = new ArrayList<>();
        for (int i = 0; i < counts.length; i++) {
            if (limit < 0) {
                break;
            }

            if (counts[i] != 0) {
                keyList.add(i);
                valueList.add(counts[i]);
                limit--;
            }
        }

        String result;
        if (limit > 0) {
            result = "YES";
        } else if (limit < 0) {
            result = "NO";
        } else {
            if (Math.abs(keyList.get(0) - keyList.get(1)) != 1) {
                result = "NO";
            } else {
                result = valueList.get(0) == 1 || valueList.get(1) == 1 ? "YES" : "NO";
            }
        }

        return  result;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = bufferedReader.readLine();

        String result = Result.isValid(s);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
