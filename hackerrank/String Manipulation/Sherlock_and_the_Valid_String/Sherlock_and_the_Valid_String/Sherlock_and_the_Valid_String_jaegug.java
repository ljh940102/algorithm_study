import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'isValid' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts STRING s as parameter.
     */

    public static String isValid(String s) {
    // Write your code here
        Map<Character, Integer> strCntMap = new HashMap<>();

        for(char c : s.toCharArray()) {
            strCntMap.putIfAbsent(c, 0);
            strCntMap.computeIfPresent(c, (key, value) -> value+1);
        }

        List<Integer> strCntList = new ArrayList<>();
        for(int i : strCntMap.values()){
            strCntList.add(i);    
        } 

        Map<Integer, Integer> cntCntMap = new HashMap<>();

        for(int c : strCntList) {
            cntCntMap.putIfAbsent(c, 0);
            cntCntMap.computeIfPresent(c, (key, value) -> value+1);
        }

        List<Integer> keys = new ArrayList<>();
        List<Integer> values = new ArrayList<>();

        for(int i : cntCntMap.keySet())
            keys.add(i);
            
        for(int i : cntCntMap.values())
            values.add(i);

        if(values.size() == 1) 
            return "YES";
        
        if(values.size() == 2) {
        
            int one_value_key = 0;
        
            for(int i=0; i < values.size(); i++){
                if(values.get(i) == 1)
                    one_value_key = keys.get(i);
            }
            if(values.contains(1) 
                && ( Math.abs(keys.get(0) - keys.get(1)) == 1 || one_value_key == 1))
                    return "YES";
        }
        return "NO";
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = bufferedReader.readLine();

        String result = Result.isValid(s);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
